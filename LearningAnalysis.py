'''
@Author: CY Peng
@Date: 2019-11-01
@LastEditTime: 2019-11-01

DataVer  - Author - Note
2019/11/01    CY    Initialization
2019/11/18    CY    Numerical/ Categorical Information

Ref.
https://www.itread01.com/content/1547582056.html, PCA Solution
https://matplotlib.org/3.1.1/gallery/widgets/slider_demo.html, slider plot
https://rpubs.com/skydome20/R-Note7-PCA, PCA example
https://stats.stackexchange.com/questions/215404/is-there-factor-analysis-or-pca-for-ordinal-or-binary-data, PCA for Non-numerical Data Type
http://vxy10.github.io/2016/06/10/intro-MCA/, MCA example
https://github.com/MaxHalford/prince, Component Analysis
'''
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import pickle

#import prince
from sklearn.decomposition import PCA
from matplotlib.widgets import Slider

# Unsupervised Learning: Principle Component Analysis
def PrincipleComponentAnalysis(df_data, outputFolder, filename, n_components = None):
    # PCA by Scikit-learn
    # pca.fit_transform(np_array_data) transformation for the array data
    #if n_components == None:
    #    n_components = min(np_array_data.shape) - 1
    pca = PCA(n_components= min(df_data.shape[0], df_data.shape[1]))  # n_components can be integer or float in (0,1)
    pca.fit(df_data)

    n_pcs = pca.components_.shape[0]
    most_important = [np.abs(pca.components_[i]).argmax() for i in range(n_pcs)]
    #print(most_important)
    initial_feature_names = df_data.columns
    most_important_names = [initial_feature_names[most_important[i]] for i in range(n_pcs)]
    features_importance_dic = {'PC{}'.format(i): most_important_names[i] for i in range(n_pcs)}
    features_importance = pd.DataFrame(sorted(features_importance_dic.items()))
    #print(features_importance)

    pd.DataFrame(initial_feature_names).to_csv(os.path.join(outputFolder, filename + '_pca_ini_features_column.csv'))
    pd.DataFrame(features_importance).to_csv(os.path.join(outputFolder, filename + '_pca_features_column.csv'))
    pd.DataFrame(pca.explained_variance_ratio_).to_csv(os.path.join(outputFolder, filename + '_pca_eigval_ratio.csv'))
    pd.DataFrame(pca.components_, columns=df_data.columns).to_csv(os.path.join(outputFolder, filename + '_pca_eigvec.csv'))
    pd.DataFrame(pca.explained_variance_).to_csv(os.path.join(outputFolder, filename + '_pca_eigval.csv'))

    fig = plt.figure()
    #ax=fig.add_subplot(1,1,1)
    #ticks=ax.set_xticks([x for x in range(0, len(df_data.columns))])
    plt.plot(np.cumsum(pca.explained_variance_ratio_[:n_components]))
    plt.grid()
    plt.title("Percentage of Variance Explained")
    #ax.set_xticklabels(features_importance.loc[:,1],rotation=90,fontsize='small')
    plt.xlabel("No of the Eigenvalues")
    plt.ylabel("Percentage Importance")
    plt.savefig(os.path.join(outputFolder, filename + '_percentage_explained_variance.svg'))
    pickle.dump(pca, open(os.path.join(outputFolder, filename + '_pca.sav'), 'wb'))
    return pca # fit the model

def VisualizingN2MEigenVector(pcaObj, df_data, N2Midx, outputFolder, filename):
    fig = plt.figure()
    ax=fig.add_subplot(1,1,1)
    ticks=ax.set_xticks([x for x in range(0, len(df_data.columns))])
    for i in range(N2Midx[0], N2Midx[1]+1):
        plt.plot(pcaObj.components_[i, :], label='Eigen Vector ' + str(i))
    ax.set_xticklabels(df_data.columns,rotation=90,fontsize='small')
    plt.legend()
    plt.grid()
    plt.savefig(os.path.join(outputFolder, filename + '_eigen_vector.svg'))

# Need to Plot the Projection for ith Projection for Different Classification
def N2MProjection(pcaObj, np_array_data, N2Midx):
    V = pcaObj.components_[:, N2Midx[0]:N2Midx[1]]
    ## Defining a function to return Projection
    return np.dot(np_array_data, V)

def PCA_slider_analysis(np_array_data):
    sfreq = Slider(axfreq, 'Freq', 0.1, 30.0, valinit=f0, valstep=delta_f)
    samp = Slider(axamp, 'Amp', 0.1, 10.0, valinit=a0)

def MultipleCorrespondenceAnalysis(df_data, outputFolder, filename, n_components = None):
    mca = prince.MCA(n_components= min(df_data.shape[0], df_data.shape[1]))
    mca.fit(df_data)

    with open(os.path.join(outputFolder, filename + '_mca_info.txt'), 'w') as f:
        f.write('explained_variance_ratio: {}\n'.format(mca.explained_inertia_))
        f.write('pca_components, eigenvectors:\n {}\n'.format(mca.components_))
        f.write('explained_variance, eigenvalues: {}\n'.format(mca.eigenvalues_))

    f.close()

    return mca  # fit the model