'''
@Author: CY Peng
@Date: 2019-12-2
@LastEditTime: 2019-12-2

DataVer  - Author - Note
2019/12/2    CY     Initialization
2020/1/10    CY     FB Prophet Model
2020/1/14    CY     Regressor Model
2020/2/3     CY     Time Series Model
'''
import ast
from configparser import ConfigParser

# General 1d Classifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.naive_bayes import GaussianNB, BernoulliNB
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV, PassiveAggressiveClassifier, RidgeClassifierCV, SGDClassifier, Perceptron
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.svm import SVC, NuSVC, LinearSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier, BaggingClassifier, ExtraTreesClassifier, RandomForestClassifier, GradientBoostingClassifier
from xgboost import XGBClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier, BernoulliRBM

# General Time Series Predictor
from fbprophet import Prophet
from sklearn.gaussian_process import GaussianProcessRegressor
#from sklearn.naive_bayes import GaussianNB, BernoulliNB 
from sklearn.linear_model import PassiveAggressiveRegressor, SGDRegressor #LogisticRegressor, LogisticRegressorCV, RidgeRegressorCV, 
#from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
#from sklearn.svm import SVC, NuSVC, LinearSVC
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import AdaBoostRegressor, BaggingRegressor, ExtraTreesRegressor, RandomForestRegressor, GradientBoostingRegressor
from xgboost import XGBRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.neural_network import MLPRegressor #, BernoulliRBM

classifiers_list = [# Gaussian Processes
                    GaussianProcessClassifier(),
                    # Navies Bayes
                    BernoulliNB(),
                    GaussianNB(),

                    # GLM
                    Perceptron(),
                    LogisticRegression(),
                    LogisticRegressionCV(),
                    PassiveAggressiveClassifier(),
                    RidgeClassifierCV(),
                    SGDClassifier(),

                    # Discriminant Analysis
                    LinearDiscriminantAnalysis(),
                    QuadraticDiscriminantAnalysis(),

                    # SVM
                    SVC(probability=True),
                    NuSVC(probability=True),
                    LinearSVC(),

                    # Ensemble Methods
                    # Linear Aggregation - Bagging
                    BaggingClassifier(),

                    # Weighted Aggregation - Boost
                    AdaBoostClassifier(),
                    GradientBoostingClassifier(),

                    # XGBClassifier
                    XGBClassifier(),

                    # Nonlinear Aggregation - Decision Tree
                    DecisionTreeClassifier(),
                    ExtraTreesClassifier(),

                    # Bagging + Decision Tree
                    RandomForestClassifier(),

                    # Nearest Neighbor
                    KNeighborsClassifier(),

                    # MLP Classifier
                    MLPClassifier(),
                    BernoulliRBM()
                    ]

prophet_regressor = [Prophet()]

regressors_list = [# Gaussian Processes
                   GaussianProcessRegressor(),
				   
				   # GLM
				   #LogisticRegressor(), 
				   #LogisticRegressorCV(),
                   PassiveAggressiveRegressor(),
                   #RidgeRegressorCV(),
                   SGDRegressor(),
                   
                   # Ensemble Methods
                   # Linear Aggregation - Bagging
                   BaggingRegressor(),
                   
                   # Weighted Aggregation - Boost
                   AdaBoostRegressor(),
                   GradientBoostingRegressor(),
                   
                   # XGBRegressor
                   XGBRegressor(),
                   
                   # Nonlinear Aggregation - Decision Tree
                   DecisionTreeRegressor(),
                   ExtraTreesRegressor(),
                   
                   # Bagging + Decision Tree
                   RandomForestRegressor(),
                   
                   # Nearest Neighbor
                   KNeighborsRegressor(),
                   
                   # MLP Classifier
                   MLPRegressor()
                   ]

timeseries_regressors_list = [# Time Series Model with No Trend 
                              #ARMA(), 
                              'ARIMA', 
				              
                              # Time Series Model with Trend
                              #VARMAX([1, 1, 1], (1, 0)), 
                              'SARIMAX',
				              
                              # Time Series State Model
                              'UnobservedComponents',
                              'DynamicFactor'
                              ]

def prophet_grid_param_search(filename):
    model_config = ConfigParser()
    model_config.read(filename + '.txt')
    print("Section: {}".format(model_config.sections()))
    print("Execution Scope: {}".format(model_config['Execution']['scope']))

    kewg = {}
    reg = prophet_regressor[0]
    
    for para in model_config[reg.__class__.__name__]:
        try:
            kewg[para] = ast.literal_eval(model_config.get(reg.__class__.__name__, para))
        except:
            kewg[para] = model_config[reg.__class__.__name__][para]
        #kewg[para] = configObj.get(clf.__class__.__name__, para)
        #try:
        #    kewg[para] = configObj.get(clf.__class__.__name__, para)
        #except:
        #    kewg[para] = configObj[clf.__class__.__name__][para]

    return kewg

def bclf_model_selector(filename):
    classifiers = []
    grid_param = {}
    model_config = ConfigParser()
    model_config.read(filename + '.txt')
    print("Section: {}".format(model_config.sections()))
    print("Execution Scope: {}".format(model_config['Execution']['scope']))
    enable_classifiers_list = model_config['Execution']['scope']

    for clf in classifiers_list:
        if clf.__class__.__name__ in enable_classifiers_list:
            kewg = {}
            classifiers.append(clf)
            for para in model_config[clf.__class__.__name__]:
                try:
                    kewg[para] = ast.literal_eval(model_config.get(clf.__class__.__name__, para))
                except:
                    kewg[para] = model_config[clf.__class__.__name__][para]
                #kewg[para] = configObj.get(clf.__class__.__name__, para)
                #try:
                #    kewg[para] = configObj.get(clf.__class__.__name__, para)
                #except:
                #    kewg[para] = configObj[clf.__class__.__name__][para]
            grid_param[clf.__class__.__name__] = kewg

    return classifiers, grid_param

def reg_model_selector(filename):
    regressors = []
    grid_param = {}
    model_config = ConfigParser()
    model_config.read(filename + '.txt')
    print("Section: {}".format(model_config.sections()))
    print("Execution Scope: {}".format(model_config['Execution']['scope']))
    enable_regressors_list = model_config['Execution']['scope']

    for reg in regressors_list:
        if reg.__class__.__name__ in enable_regressors_list:
            kewg = {}
            regressors.append(reg)
            for para in model_config[reg.__class__.__name__]:
                try:
                    kewg[para] = ast.literal_eval(model_config.get(reg.__class__.__name__, para))
                except:
                    kewg[para] = model_config[reg.__class__.__name__][para]
                #kewg[para] = configObj.get(clf.__class__.__name__, para)
                #try:
                #    kewg[para] = configObj.get(clf.__class__.__name__, para)
                #except:
                #    kewg[para] = configObj[clf.__class__.__name__][para]
            grid_param[reg.__class__.__name__] = kewg

    return regressors, grid_param

def ts_model_grid_param_search(filename):
    ts_regressors = []
    grid_param = {}
    model_config = ConfigParser()
    model_config.read(filename + '.txt')
    print("Section: {}".format(model_config.sections()))
    print("Execution Scope: {}".format(model_config['Execution']['scope']))
    enable_regressors_list = model_config['Execution']['scope']

    for reg_name in timeseries_regressors_list:
        ref_name = enable_regressors_list[1:-1]
        #print(reg_name, ref_name, reg_name==ref_name)
        if reg_name == ref_name:
           kewg = {}
           ts_regressors.append(reg_name)
           for para in model_config[reg_name]:
               try:
                   kewg[para] = ast.literal_eval(model_config.get(reg_name, para))
               except:
                   kewg[para] = model_config[reg_name][para]
               #kewg[para] = configObj.get(clf.__class__.__name__, para)
               #try:
               #    kewg[para] = configObj.get(clf.__class__.__name__, para)
               #except:
               #    kewg[para] = configObj[clf.__class__.__name__][para]
           grid_param[reg_name] = kewg
    print('ParaSetting', grid_param)
    return ts_regressors, grid_param