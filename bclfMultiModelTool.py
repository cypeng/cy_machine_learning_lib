'''
@Author: CY Peng
@Date: 2019-12-3
@LastEditTime: 2019-12-3

DataVer  - Author - Note
2019/12/3     CY    Initialization
2019/12/6     CY    Model Information Reorganization
2019/12/18    CY    Model Information Analysis
2020/1/1      CY    All Models Prediction
'''
import re
import numpy as np
import pandas as pd
import pickle
import shutil
import glob
import os
import matplotlib.pyplot as plt # data visualization

import H5FileManager as H5FM
import CommonUtility as CU
import bclfModelCreatorHelper as MCH
import ModelEvaluation as ME
import ExploratoryDataAnalysis as EDA

output_folder = "model_organization"
h5file_count = 1

def read_data(filename):
    data = pd.read_csv(filename+".csv")
    #data = data.fillna(value="unknown")
    return data

def read_val_data(folder_path):
    df_x_val = read_data(os.path.join(folder_path, 'x_val'))#.iloc[:, 1:]
    df_y_val = read_data(os.path.join(folder_path, 'y_val'))#.iloc[:, 1:]
    return df_x_val, df_y_val

def find_extension_name(folder_path, extension_name):
    return glob.glob(os.path.join(folder_path, extension_name))

def copy_h5_file(h5_file, output_path):
    global h5file_count

    organization_file = os.path.join(output_path, "MultiModelAnalysis.h5")

    # Copy Dataset to Organization File
    datasets_name = H5FM.H5datasets(h5_file, groupName = "ML Analysis")
    for dataset_name in datasets_name:
        Data = H5FM.H52Data(h5_file,  dataset_name, groupName = "ML Analysis")
        if ("CLF Nested CV Table" == dataset_name):
            dataset_name = dataset_name + "_"+str(h5file_count)
            h5file_count += 1
        H5FM.Data2H5(Data, organization_file, dataset_name, groupName = "ML Analysis")

    # CLF Dataset to Organization File
    ori_data = H5FM.H52Data(organization_file, "CLF Nested CV Table_1", groupName="ML Analysis")
    for cv_table_count in range(2, h5file_count):
        concat_data = H5FM.H52Data(organization_file, "CLF Nested CV Table_"+str(cv_table_count), groupName="ML Analysis")
        ori_data = np.concatenate((ori_data, concat_data), axis = 0)
    ori_data = np.sort(ori_data, order='Classifier Test Accuracy Mean')[::-1]
    H5FM.Data2H5(ori_data, organization_file, "CLF Nested CV Table", groupName="ML Analysis")
    H5FM.H5ReSave(organization_file)

def copy_model_data(old_model_folder, new_model_folder):
    if (len(os.listdir(old_model_folder)) >= 1):
        try:
            shutil.copytree(old_model_folder, os.path.join(new_model_folder, old_model_folder.split("\\")[-2]))
        except:
            print("Some issues in {}".format(old_model_folder))
            pass

def get_model_path_info(model_folder):
    best_clf_path_lists = find_extension_name(os.path.join(model_folder,"*"),"*_best_clf.sav")
    tree_based_clf_path_lists, tree_based_clf_folder_lists, best_clf_folder_lists = [], [], []
    prob_based_clf_path_lists, prob_based_clf_folder_lists = [], []
    for clf_path in best_clf_path_lists:
        clf_path_folder = os.path.join(*clf_path.split("\\")[:-1])
        best_clf_folder_lists.append(clf_path_folder)
        modelObj = pickle.load(open(os.path.join(clf_path), 'rb'))
        if (ME.check_tree_based_model(modelObj)):
            tree_based_clf_path_lists.append(clf_path)
            tree_based_clf_folder_lists.append(clf_path_folder)
            #print('Tree Based Model:',clf_path)
            #print(clf_path_folder)

        df_x_val, _ = read_val_data(clf_path_folder)
        if (ME.check_probability_based_model(modelObj, df_x_val)):
            prob_based_clf_path_lists.append(clf_path)
            prob_based_clf_folder_lists.append(clf_path_folder)
        del modelObj, df_x_val
    return best_clf_path_lists, best_clf_folder_lists, \
           tree_based_clf_path_lists, tree_based_clf_folder_lists, \
           prob_based_clf_path_lists, prob_based_clf_folder_lists

def get_cv_results(clf_path_lists, clf_folder_lists, outputFolder, conf_matrix_params):
    clf_table_group = MCH.cv_results_record_init(len(clf_path_lists))
    specs = EDA.multiplot_strategy(len(clf_path_lists))
    fig = plt.figure(figsize=(25, 20))
    plot_setting = {}
    for idx, clf_path in enumerate(clf_path_lists):
        # model load
        clf_path = clf_path_lists[idx]
        # modelObj = pickle.load(open(os.path.join(clf_path), 'rb'))
        clf_name = clf_path.split("\\")[-1].split(".")[0]
        model_name = clf_name.split("_")[0]

        # val data load
        clf_folder = clf_folder_lists[idx]
        df_x_val, df_y_val = read_val_data(clf_folder)

        cv_results_path = find_extension_name(clf_folder, "*_cv_results.sav")[0]
        cv_results = pickle.load(open(os.path.join(cv_results_path), 'rb'))
        MCH.cv_clf_table_record(model_name, clf_table_group, cv_results, idx,
                                outputFolder, outputFolder)

        df_cm, df_clf_report = MCH.cal_confusion_matrix(df_x_val, df_y_val, clf_folder, clf_name,
                                                        conf_matrix_params)
        H5FM.Data2H5(np.array(df_cm), os.path.join(outputFolder, 'MultiModelAnalysis'),
                     '{}_df_cm'.format(model_name), groupName="ML Analysis")
        H5FM.Data2H5(np.array(df_clf_report), os.path.join(outputFolder, 'MultiModelAnalysis'),
                     '{}_df_clf_report'.format(model_name), groupName="ML Analysis")

        # cv plot
        plot_setting['title'] = "{} Learning Curve".format(model_name)
        plt.subplot(specs[idx])
        MCH.cv_plot(cv_results['train_score'], cv_results['test_score'], plot_setting = plot_setting)

        # prediction error 
        pred, abs_err = MCH.cal_pred_error(df_x_val, df_y_val, clf_folder, clf_name)
        val_data = read_data(os.path.join(clf_folder, 'val'))
        val_data['Pred'] = pred
        val_data['abs_err']= abs_err
        val_data.to_csv(os.path.join(clf_folder, 'val_err.csv'), index=False) 		
    
    EDA.multiplot_save(fig, outputFolder, "cv_curve.svg")
    plt.close("all")

def df_data_features_err_multi_model_type_plot(model_folder, type_name, target_name, outputFolder, filename, plot_type = None, ylabel_list = ["No of Instances"], fig_size = (10, 10), missing_data_handle = None, FeaturesEngineeringObj = None):
	# File Path List
    err_file_list = glob.glob(os.path.join(model_folder,'*','val_err.csv'))
    
    print(os.path.join(model_folder,'*','*_err.csv'))
    specs = EDA.multiplot_strategy(len(err_file_list))
    fig = plt.figure(figsize=fig_size)
	
    # Plot Setting Initialization
    plot_setting = {}
    kwargs = {}
    for idx, spec in enumerate(specs):
        df_data = pd.read_csv(err_file_list[idx])
        df_data = FeaturesEngineeringObj(df_data)
        if (missing_data_handle['type'] == 'fillna'):
            df_data = df_data.fillna(value=missing_data_handle['value'])
        if (missing_data_handle['type'] == 'dropna'):
            df_data = df_data.dropna()
   
        folder_name = err_file_list[idx].split("\\")[-2]#.split(".")[0].split("_")[0]
        r = re.compile("([a-zA-Z]+)([0-9]+)")
        m = r.match(folder_name)
        model_name =  m.group(1)
        #print(model_name)
        fig.tight_layout()
        
        # Plot Setting
        if (target_name == None):   
           plot_setting['xlabel'] = type_name  
        else:
           plot_setting['xlabel'] = "{} versus {}".format(type_name, target_name) 
        
        if plot_type == 'Distribution':
           plot_setting['x'] = type_name
           plot_setting['y'] = None
        elif (plot_type == 'Count'):
           kwargs['x'] = type_name
           kwargs['hue'] = target_name
        elif (plot_type == 'Box'):
           kwargs['y'] = type_name
           kwargs['x'] = target_name

        plot_setting['title'] = "{} Model {} Error {} Plot".format(model_name, type_name, plot_type)
        plot_setting['ylabel'] = None
        if (ylabel_list):
            if (len(ylabel_list) > 1):
                plot_setting['ylabel'] = ylabel_list[idx]
            else:
                plot_setting['ylabel'] = ylabel_list[0]

        # Figure Plot
        plt.subplot(spec)
        #print('test', plot_type, kwargs, plot_setting)
        EDA.df_data_fig_plot(df_data, plot_type, plot_setting, **kwargs)
    if target_name == None:
       target = 'None'
    EDA.multiplot_save(fig, outputFolder, filename+"_multi_models_{}_versus_{}_err_{}.svg".format(type_name, target_name, plot_type))
    plt.close("all")

def df_data_multi_bi_clf_roc_plot(clf_path_lists, clf_folder_lists, outputFolder):
    specs = EDA.multiplot_strategy(len(clf_path_lists))
    fig = plt.figure(figsize=(25, 10))
    plot_setting = {}
    for idx, spec in enumerate(specs):
        # model load
        clf_path = clf_path_lists[idx]
        #modelObj = pickle.load(open(os.path.join(clf_path), 'rb'))
        clf_name = clf_path.split("\\")[-1].split(".")[0]
        model_name = clf_name.split("_")[0]

        # val data load
        clf_folder = clf_folder_lists[idx]
        df_x_val, df_y_val = read_val_data(clf_folder)

        # roc plot
        fig.tight_layout()
        plot_setting['title'] = "{} ROC to Multi-Class".format(model_name)
        plt.subplot(spec)
        fpr, tpr, roc_auc = MCH.cal_roc_auc_curve(df_x_val, df_y_val, clf_folder, clf_name)
        ME.binary_clf_roc_curve_plot(fpr, tpr, roc_auc, plot_setting = plot_setting)        
    EDA.multiplot_save(fig, outputFolder, "roc.svg")
    plt.close("all")

def df_data_multi_tree_based_clf_feature_importance_plot(clf_path_lists, clf_folder_lists, outputFolder):
    specs = EDA.multiplot_strategy(len(clf_path_lists))
    fig = plt.figure(figsize=(25, 10))
    plot_setting = {}
    for idx, spec in enumerate(specs):
        # model load
        clf_path = clf_path_lists[idx]
        # modelObj = pickle.load(open(os.path.join(clf_path), 'rb'))
        clf_name = clf_path.split("\\")[-1].split(".")[0]
        model_name = clf_name.split("_")[0]

        # val data load
        clf_folder = clf_folder_lists[idx]
        df_x_val, df_y_val = read_val_data(clf_folder)

        fig.tight_layout()
        plot_setting['title'] = "{} Based Feature Importance".format(model_name)
        plt.subplot(spec)
        modelObj, importances, indices = MCH.get_tree_based_feature_importance(df_x_val, clf_folder, clf_name)
        modelObjGroup = (modelObj, importances, indices)
        ME.df_data_tree_based_feature_importance_plot(modelObjGroup, df_x_val, plot_setting=plot_setting)
    EDA.multiplot_save(fig, outputFolder, "feature_importance.svg")
    plt.close("all")

def all_models_prediction(clf_path_lists, outputFolder, pred_id_column='id', pred_column='sub', data_drop_column='id', pred_filename='test', databasefolder='database'):
    pred_file_data = read_data(os.path.join(databasefolder, pred_filename))
    for clf_path in clf_path_lists:
        # model load
        #clf_path = clf_path_lists[idx]
        modelObj = pickle.load(open(os.path.join(clf_path), 'rb'))
        clf_name = clf_path.split("\\")[-1].split(".")[0]
        #model_name = clf_name.split("_")[0]

        submission = pd.DataFrame({
        pred_id_column: pred_file_data[pred_id_column],
		pred_column: modelObj.predict(pred_file_data.drop(columns=data_drop_column, axis=1))
        })
        submission.to_csv(os.path.join(outputFolder, clf_name + '_submission.csv'), index=False)

def model_organization(model_folder, **kwargs):
    output_folder_path = os.path.join(model_folder, output_folder)
    CU.createOutputFolder(output_folder_path)

    # Parameters Setting
    conf_matrix_params = {}
    conf_matrix_params['confusion_matrix'] = {}
    conf_matrix_params['confusion_matrix']['labels'] = [1, 0]
    conf_matrix_params['clf_report'] = {}
    conf_matrix_params['clf_report']['digits'] = 2

    # model data
    print("model data check")
    model_folder_lists = find_extension_name(model_folder, "*/*/")
    for old_model_folder in model_folder_lists:
        copy_model_data(old_model_folder, output_folder_path)

    # create h5file
    print("Model Information Reorganization")
    best_clf_path_lists, best_clf_folder_lists, \
    tree_based_clf_path_lists, tree_based_clf_folder_lists, \
    prob_based_clf_path_lists, prob_based_clf_folder_lists = get_model_path_info(output_folder_path)
    print(best_clf_path_lists, best_clf_folder_lists)
    print(tree_based_clf_path_lists, tree_based_clf_folder_lists)
    print(prob_based_clf_path_lists, prob_based_clf_folder_lists)
    if (len(best_clf_path_lists) > 0):
       get_cv_results(best_clf_path_lists, best_clf_folder_lists, output_folder_path, conf_matrix_params)
    if (len(tree_based_clf_path_lists) > 0):
       df_data_multi_tree_based_clf_feature_importance_plot(tree_based_clf_path_lists, tree_based_clf_folder_lists, output_folder_path)
    if (len(prob_based_clf_path_lists) > 0):
       df_data_multi_bi_clf_roc_plot(prob_based_clf_path_lists, prob_based_clf_folder_lists, output_folder_path)

    # all data prediction
    try:
        all_models_prediction(best_clf_path_lists, output_folder_path, **kwargs)
    except:
        print("Issues in Prediction File")
        pass
    
    #cv_results_lists = find_extension_name(output_folder_path, "*/*_cv_results.sav")
    #get_cv_results(cv_results_lists, output_folder_path, conf_matrix_params)

    # h5file data
    """
    print("h5 data check")
    h5file_lists = find_extension_name(model_folder, "*\*.h5")
    for h5file in h5file_lists:
        copy_h5_file(h5file, output_folder_path)
    """

    # csv table
    """
    print("csv data check")
    csv_lists = find_extension_name(model_folder, "*\*.csv")
    for csv_file in csv_lists:
        shutil.copy(csv_file, output_folder_path)
    """


