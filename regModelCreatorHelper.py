'''
@Author: CY Peng
@Date: 2020-01-14
@LastEditTime: 2020-01-14

DataVer  - Author - Note
2020/01/14 C.Y.     Initialization from CLF

Ref.
https://datascience.stackexchange.com/questions/45046/cross-validation-for-highly-imbalanced-data-with-undersampling, Imbalance Data Model
https://datascience.stackexchange.com/questions/29520/how-to-plot-learning-curve-and-validation-curve-while-using-pipeline, Grid Search First, Learning Curve Second
https://scikit-learn.org/stable/auto_examples/model_selection/plot_nested_cross_validation_iris.html, nested cross validation
'''
import CommonUtility as CU
import pickle
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import h5py
import H5FileManager as H5FM
import math
import glob
import shutil

import SamplingLib as SM
import ModelEvaluation as ME

# Cross Validation for Multi Classifier
from sklearn.model_selection import cross_validate, cross_val_score, learning_curve, GridSearchCV, train_test_split, validation_curve
from sklearn.multiclass import OneVsRestClassifier

_output_main_folder = 'ML_output'
CU.createOutputFolder(_output_main_folder)

def read_data(filename):
    data = pd.read_csv(filename+".csv")
    #data = data.fillna(value="unknown")
    return data

def cv_plot(train_scores, test_scores, plot_setting = None):
    # box-like grid
    plt.grid()

    train_sizes = range(0, len(train_scores))
    # plot the average training and test score lines at each training set size
    plt.plot(train_sizes, train_scores, 'o-', color="r", label="Training score")
    plt.plot(train_sizes, test_scores, 'o-', color="g", label="Cross-validation score")

    if plot_setting == None:
        plt.title("Learning Curve")
    else:
        plt.title(plot_setting['title'])
    plt.legend(loc="best", labels=['Training score', 'Cross-validation score'])
    plt.xlabel("CV Training No.")
    plt.ylabel("Score")
    
    plt.gca().invert_yaxis()

    # sizes the window for readability and displays the plot
    # shows error from 0 to 1.1
    plt.ylim(-.1, 1.1)

def cal_pred_error(df_x_val, df_y_val, output_path, filename):
    loaded_model = pickle.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
    y_pred = loaded_model.best_estimator_.predict(df_x_val)
    
    df_data_features_err = df_x_val
    df_data_features_err['Pred'] = y_pred
    df_data_features_err['abs_err'] = y_pred - df_y_val.iloc[:,0].values.tolist()
    df_data_features_err.to_csv(os.path.join(output_path, filename+'_err.csv'), index=False)
    return y_pred, y_pred - df_y_val.iloc[:,0].values.tolist()

def cal_reg_error(df_x_val, y_val, output_path, filename):
    loaded_model = pickle.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
    y_pred = loaded_model.predict(df_x_val)
    y_true = y_val.values.tolist()
    print(df_x_val)

    mse, rmse, median_abs_err, mean_abs_err, exp_var_score, r2 = ME.reg_evaluation(y_true, y_pred)
    #err_list = list(zip([mse], [rmse], [median_abs_err], [mean_abs_err], [exp_var_score], [r2]))
    #err_columns = ['mse', 'rms', 'median_absolute_error', 'mean_absolute_error', 'explained_variance_score', 'r2_score']
    df_forecast_performance = pd.DataFrame({'mse': mse,
                                            'rms': rmse,
                                            'median_absolute_error':median_abs_err,
                                            'mean_absolute_error': mean_abs_err, 
                                            'explained_variance_score': exp_var_score, 
                                            'r2_score': r2})
    df_forecast_performance.to_csv(os.path.join(output_path,"{}_performance.csv".format(filename)), index=False)
    return (mse, rmse, median_abs_err, mean_abs_err, exp_var_score, r2)

def get_tree_based_feature_importance(df_val_X, output_path, filename):
    modelObj = pickle.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
    importances, indices = ME.df_data_tree_based_feature_importance(modelObj)
    #ME.df_data_tree_based_feature_importance(loaded_model.best_estimator_, df_val_X, output_path, filename)
    return modelObj, importances, indices

def cv_results_record_init(reg_table_length):
    reg_table_dataType = np.dtype([('Regressor Name', h5py.special_dtype(vlen=str)),
                                   ('Regressor Parameters', h5py.special_dtype(vlen=str)),
                                   ('Regressor Train Explained Variance Mean', np.float),
                                   ('Regressor Test Explained Variance Mean', np.float),
                                   ('Regressor Test Explained Variance 3*STD', np.float),
                                   ('Regressor Train r2 Score Mean', np.float),
                                   ('Regressor Test r2 Score Mean', np.float),
                                   ('Regressor Test r2 Score 3*STD', np.float),
                                   ('Regressor Train Max Error Mean', np.float),
                                   ('Regressor Test Max Error Mean', np.float),
                                   ('Regressor Test Max Error 3*STD', np.float),
                                   ('Regressor Train Net MAE Mean', np.float),
                                   ('Regressor Test Net MAE Mean', np.float),
                                   ('Regressor Test Net MAE 3*STD', np.float),
                                   ('Regressor Train Net MSE Mean', np.float),
                                   ('Regressor Test Net MSE Mean', np.float),
                                   ('Regressor Test Net MSE 3*STD', np.float),
                                   ('Regressor Train Net RMSE Mean', np.float),
                                   ('Regressor Test Net RMSE Mean', np.float),
                                   ('Regressor Test Net RMSE 3*STD', np.float),
#                                   ('Regressor Train Net MSLE Mean', np.float),
#                                   ('Regressor Test Net MSLE Mean', np.float),
#                                   ('Regressor Test Net MSLE 3*STD', np.float),
                                   ('Regressor Train Net Median SE Mean', np.float),
                                   ('Regressor Test Net Median SE Mean', np.float),
                                   ('Regressor Test Net Median SE 3*STD', np.float),
#                                   ('Regressor Train Net MPD Mean', np.float),
#                                   ('Regressor Test Net MPD Mean', np.float),
#                                   ('Regressor Test Net MPD 3*STD', np.float),
#                                   ('Regressor Train Net MGD Mean', np.float),
#                                   ('Regressor Test Net MGD Mean', np.float),
#                                   ('Regressor Test Net MGD 3*STD', np.float),								   
								   ('Regressor Time', np.float)
								   ])
    reg_table = np.zeros((reg_table_length,), dtype=reg_table_dataType)
    return (reg_table, reg_table_dataType)

def cv_reg_table_record(modelname, reg_table_group, cv_results, row_index, base_output_folder, output_folder):
    # Results Record
    (reg_table, reg_table_dataType) = reg_table_group
    #modelname = reg.__class__.__name__
    #row_index = self.reg.index(clf)
    #op = self.output_paths[modelname]
    reg_table['Regressor Name'][row_index] = modelname
    reg_table['Regressor Parameters'][row_index] = cv_results['estimator']#str(clf.get_params())
    reg_table['Regressor Train Explained Variance Mean'][row_index] = cv_results['train_explained_variance'].mean()
    reg_table['Regressor Test Explained Variance Mean'][row_index] = cv_results['test_explained_variance'].mean()
    reg_table['Regressor Test Explained Variance 3*STD'][row_index] = cv_results['test_explained_variance'].std() * 3
    reg_table['Regressor Train r2 Score Mean'][row_index] = cv_results['train_r2'].mean()
    reg_table['Regressor Test r2 Score Mean'][row_index] = cv_results['test_r2'].mean()
    reg_table['Regressor Test r2 Score 3*STD'][row_index] = cv_results['test_r2'].std() * 3
    reg_table['Regressor Train Max Error Mean'][row_index] = cv_results['train_max_error'].mean()
    reg_table['Regressor Test Max Error Mean'][row_index] = cv_results['test_max_error'].mean()
    reg_table['Regressor Test Max Error 3*STD'][row_index] = cv_results['test_max_error'].std() * 3
    reg_table['Regressor Train Net MAE Mean'][row_index] = cv_results['train_neg_mean_absolute_error'].mean()
    reg_table['Regressor Test Net MAE Mean'][row_index] = cv_results['test_neg_mean_absolute_error'].mean()
    reg_table['Regressor Test Net MAE 3*STD'][row_index] = cv_results['test_neg_mean_absolute_error'].std() * 3
    reg_table['Regressor Train Net MSE Mean'][row_index] = cv_results['train_neg_mean_squared_error'].mean()
    reg_table['Regressor Test Net MSE Mean'][row_index] = cv_results['test_neg_mean_squared_error'].mean()
    reg_table['Regressor Test Net MSE 3*STD'][row_index] = cv_results['test_neg_mean_squared_error'].std() * 3
    reg_table['Regressor Train Net RMSE Mean'][row_index] = cv_results['train_neg_root_mean_squared_error'].mean()
    reg_table['Regressor Test Net RMSE Mean'][row_index] = cv_results['test_neg_root_mean_squared_error'].mean()
    reg_table['Regressor Test Net RMSE 3*STD'][row_index] = cv_results['test_neg_root_mean_squared_error'].std() * 3
#    reg_table['Regressor Train Net MSLE Mean'][row_index] = cv_results['train_neg_mean_squared_log_error'].mean()
#    reg_table['Regressor Test Net MSLE Mean'][row_index] = cv_results['test_neg_mean_squared_log_error'].mean()
#    reg_table['Regressor Test Net MSLE 3*STD'][row_index] = cv_results['test_neg_mean_squared_log_error'].std() * 3
    reg_table['Regressor Train Net Median SE Mean'][row_index] = cv_results['train_neg_median_absolute_error'].mean()
    reg_table['Regressor Test Net Median SE Mean'][row_index] = cv_results['test_neg_median_absolute_error'].mean()
    reg_table['Regressor Test Net Median SE 3*STD'][row_index] = cv_results['test_neg_median_absolute_error'].std() * 3
#    reg_table['Regressor Train Net MPD Mean'][row_index] = cv_results['train_neg_mean_poisson_deviance'].mean()
#    reg_table['Regressor Test Net MPD Mean'][row_index] = cv_results['test_neg_mean_poisson_deviance'].mean()
#    reg_table['Regressor Test Net MPD 3*STD'][row_index] = cv_results['test_neg_mean_poisson_deviance'].std() * 3
#    reg_table['Regressor Train Net MGD Mean'][row_index] = cv_results['train_neg_mean_gamma_deviance'].mean()
#    reg_table['Regressor Test Net MGD Mean'][row_index] = cv_results['test_neg_mean_gamma_deviance'].mean()
#    reg_table['Regressor Test Net MGD 3*STD'][row_index] = cv_results['test_neg_mean_gamma_deviance'].std() * 3
    reg_table['Regressor Time'][row_index] = cv_results['fit_time'].mean()

    # Results Save
    reg_cv_table = np.sort(reg_table, order='Regressor Test r2 Score Mean')[::-1]
    H5FM.Data2H5(reg_cv_table, os.path.join(base_output_folder, 'MultiModelAnalysis'),
                'REG Nested CV Table', groupName="ML Analysis", dtype=reg_table_dataType)
    df_reg_cv_table = pd.DataFrame(reg_cv_table)
    df_reg_cv_table.to_csv(os.path.join(output_folder,  modelname + "_reg_table.csv"), index=False)
    df_reg_cv_table.to_csv(os.path.join(output_folder,  "all_model_reg_table.csv"), index=False, float_format = '%.2f')
    df_reg_cv_table.drop(columns = ['Regressor Parameters']).to_csv(os.path.join(output_folder,  "all_model_reg_table_d.csv"), index=False, float_format = '%.2f')

# https://malishoaib.wordpress.com/2017/09/08/sample-size-estimation-for-machine-learning-models-using-hoeffdings-inequality/
# error = sqrt(-1*math.log(significance_level / 2)/n)
# significance_level: type 1 error for the confusion matrix
def Hoeffding_SampleSize(error, significance_level):
    n = (-1 // (2 * error ** 2)) * math.log(significance_level / 2)
    return n

def model_prediction(model_path, X):
    loaded_model = pickle.load(model_path + ".sav", 'rb')
    y_pred = loaded_model.predict(X)
    return y_pred

# Multi-Model Selection for sklearn or xgboost
class MultiModelMLHelper():
    def __init__(self, regs, params):
        self.regs = regs #included pipeline estimator
        self.grid_params = params
        self.ml_output_count = 0
        self.base_path = CU.updateOutputFolder(_output_main_folder+"//", 'MultiModels')
        self.output_paths = self.__output_paths__()
        self.global_test_score = 0

    def __output_paths__(self):
        global _output_main_folder
        paths = {}
        
        for reg in self.regs:
            modelname = reg.__class__.__name__
            paths[modelname] = os.path.join(self.base_path,
                                             modelname + str(self.ml_output_count))
            CU.createOutputFolder(paths[modelname])
        
        setting_files_list = glob.glob('*.txt')
        for copy_file_path in setting_files_list:
            shutil.copy(copy_file_path, self.base_path)
        self.ml_output_count += 1
        return paths

    def model_evaluation(self, x_val, y_val, output_path, filename):
       #self.__get_reg_err__(x_val, y_val, output_path, filename)
       try:
           self.__get_reg_err__(x_val, y_val, output_path, filename)
       except:
           pass
       try:
           self.__get_permulation_importance__(x_val, y_val, output_path, filename)
       except:
           pass
       try:
           self.__get_features_importance__(x_val, output_path, filename)
       except:
           pass
       try:
           self.__get_shap_analysis__(x_val, output_path, filename)
       except:
           pass

    def __get_reg_err__(self, df_val_X, df_val_y, output_path, filename):
        cal_reg_error(df_val_X, y_pred, output_path, filename)
        #loaded_model = pickle.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
        #y_pred = loaded_model.best_estimator_.predict(df_val_X)
        #cal_reg_error(y_pred, y_pred, output_path, filename)

    def __get_shap_analysis__(self, df_val_X, output_path, filename):
        loaded_model = pickle.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
        try:
            ME.df_data_ensemble_tree_shap_force_each_plot(loaded_model.best_estimator_, df_val_X, 2, output_path, filename)
        except:
            pass
        try:
            ME.df_data_ensemble_tree_shap_force_summary_plot(loaded_model.best_estimator_, df_val_X, 2, output_path, filename)
        except:
            pass

    def __get_features_importance__(self, df_val_X, output_path, filename):
        plt.figure()
        modelObj, importances, indices = get_tree_based_feature_importance(df_val_X, output_path, filename)
        modelObjGroup = (modelObj, importances, indices)
        ME.df_data_tree_based_feature_importance_plot(modelObjGroup, df_val_X) #, output_path, filename)
        print(os.path.join(output_path, filename + '_feature_importance.svg'))
        plt.savefig(os.path.join(output_path, filename + '_feature_importance.svg'), bbox_inches='tight')
        plt.close('all')

    def __get_permulation_importance__(self, df_val_X, df_val_y, output_path, filename):
        loaded_model = pickle.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
        ME.df_data_permulation_importance(loaded_model.best_estimator_, df_val_X, df_val_y, output_path, filename, random_state=1)

    def __model_save__(self, reg, est_reg, x_val, y_val):
        modelname = reg.__class__.__name__
        filename = "{}_best".format(modelname)
        op = self.output_paths[modelname]
        pickle.dump(est_reg, open(os.path.join(op, filename + '.sav'), 'wb'))
        self.model_evaluation(x_val, y_val, op, filename)

    def __cv_model_save__(self, reg, cv_results, x_val, y_val):
        modelname = reg.__class__.__name__
        output_folder = self.output_paths[modelname]

        try:
            best_idx = list(cv_results['test_r2']).index(max(cv_results['test_r2']))
            best_reg = cv_results['estimator'][best_idx]
            pickle.dump(best_reg, open(os.path.join(output_folder, '{}_cv_{}_best_reg.sav'.format(modelname, best_idx)), 'wb'))
            if (max(cv_results['test_r2']) > self.global_test_score):
                pickle.dump(best_reg, open(os.path.join(self.base_path, 'best_reg.sav'), 'wb'))
                print('best_model: {} cv_{} in {}'.format(modelname, best_idx, os.path.join(self.base_path, 'best_reg.sav')))
        except:
            pass

        for idx, est_reg in enumerate(cv_results['estimator']):
            filename = "{}_cv{}".format(modelname, idx)
            pickle.dump(est_reg, open(os.path.join(output_folder, filename+'.sav'), 'wb'))
            self.model_evaluation(x_val, y_val, output_folder, filename)

    def __results_record__(self, reg, reg_table_group, est_reg):
        # Results Record
        modelname = reg.__class__.__name__
        output_folder = self.output_paths[modelname]
        row_index = self.regs.index(reg)
        grid_results_table_record(modelname, reg_table_group, est_reg, row_index, self.base_path, output_folder)

        # save the cv results
        pickle.dump(est_reg.cv_results_, open(os.path.join(output_folder, '{}_cv_results.sav'.format(modelname)), 'wb'))
        csv_cv_results = pd.DataFrame.from_dict(est_reg.cv_results_)
        csv_cv_results.to_csv(os.path.join(output_folder, '{}_cv_results.csv'.format(modelname)), index=False)

    def __cv_results_record__(self, reg, reg_table_group, cv_results):
        # save reg table
        modelname = reg.__class__.__name__
        row_index = self.regs.index(reg)
        output_folder = self.output_paths[modelname]
        cv_reg_table_record(modelname, reg_table_group, cv_results, row_index, self.base_path, output_folder)

        # save the cv results
        pickle.dump(cv_results, open(os.path.join(output_folder, '{}_cv_results.sav'.format(modelname)), 'wb'))
        csv_cv_results = pd.DataFrame.from_dict(cv_results)
        csv_cv_results.to_csv(os.path.join(output_folder, '{}_cv_results.csv'.format(modelname)), index=False)

    # Data Preparation
    def data_preparation(self, raw_x, raw_y, tail_name_to_train, fit_model_file_path, FeatureEngineeringFitObj, LabelEncoderFitObj = None, train_drop_column = None, train_size = 0.77):
        for reg in self.regs:
            modelname = reg.__class__.__name__
            op = self.output_paths[modelname]
            
            length = raw_y.shape[0]
            train_idx = int(length*train_size)
            x_train, x_val = raw_x.iloc[0:train_idx,:], raw_x.iloc[train_idx:,:]
            #try:
            y_train, y_val = raw_y.iloc[0:train_idx,:], raw_y.iloc[train_idx:,:]
            #except:
            #    y_train, y_val = raw_y.iloc[0:train_idx,], raw_y.iloc[train_idx:,] 
            x_train.to_csv(os.path.join(op, 'x_raw_train.csv'), index=False)
            x_val.to_csv(os.path.join(op, 'x_raw_val.csv'), index=False)
            y_train.to_csv(os.path.join(op, 'y_raw_train.csv'), index=False)
            y_val.to_csv(os.path.join(op, 'y_raw_val.csv'), index=False)
            
            y_train.to_csv(os.path.join(op, 'y_train.csv'), index=False)
            y_val.to_csv(os.path.join(op, 'y_val.csv'), index=False)
            
            if (LabelEncoderFitObj):
               LabelEncoderFitObj('y_train', op, fit_model_file_path)
               LabelEncoderFitObj('y_val', op, fit_model_file_path)

            train = pd.concat([x_train, y_train], axis=1)
            train.to_csv(os.path.join(op, "train.csv"), index=False)
            
            val=pd.concat([x_val, y_val], axis=1)
            val.to_csv(os.path.join(op, "val.csv"), index=False)

            FeatureEngineeringFitObj("train", op, fit_model_file_path)
            FeatureEngineeringFitObj("val", op, fit_model_file_path)
			
            train_data = read_data(os.path.join(op, "train"+tail_name_to_train))
            if (train_drop_column):
                train_data = train_data.drop(columns= train_drop_column, axis=1)
            train_data.to_csv(os.path.join(op, 'x_train.csv'), index=False)
            cv_test_data = read_data(os.path.join(op, "val"+tail_name_to_train))
            if (train_drop_column):
                cv_test_data = cv_test_data.drop(columns= train_drop_column, axis=1)
            cv_test_data.to_csv(os.path.join(op, 'x_val.csv'), index=False)

    # Non_nested parameter search and scoring
    def train(self, fold_obj, conf_matrix_params):
        reg_table_group = grid_results_record_init(len(self.regs)) #self.__results_record_init__()

        for reg in self.regs:
            modelname = reg.__class__.__name__
            param = self.grid_params[modelname]
            op = self.output_paths[modelname]
			
            x_train, y_train = read_data(os.path.join(op, 'x_train')), read_data(os.path.join(op, 'y_train'))
            x_val, y_val = read_data(os.path.join(op, 'x_val')), read_data(os.path.join(op, 'y_val'))
            #x_train, x_val, y_train, y_val = SM.simple_train_test_data_split(x, y)
            #pd.DataFrame(x_train).to_csv(os.path.join(op, 'x_train.csv'), index=False)
            #pd.DataFrame(x_val).to_csv(os.path.join(op, 'x_val.csv'), index=False)
            #pd.DataFrame(y_train).to_csv(os.path.join(op, 'y_train.csv'), index=False)
            #pd.DataFrame(y_val).to_csv(os.path.join(op, 'y_val.csv'), index=False)

            grid_search = GridSearchCV(estimator=reg, cv=fold_obj, param_grid=param, refit=True)
            est_reg = grid_search.fit(x_train, y_train)
            self.__model_save__(reg, est_reg, x_val, y_val, conf_matrix_params)
            self.__results_record__(reg, reg_table_group, est_reg)
            del grid_search, est_reg, x_train, x_val, y_train, y_val

    # Nested Cross Validation
    def nested_train(self, inner_fold_obj, outer_fold_obj):
        reg_table_group = cv_results_record_init(len(self.regs))

        for reg in self.regs:
            modelname = reg.__class__.__name__
            param = self.grid_params[modelname]
            op = self.output_paths[modelname]

            x_cv, y_cv = read_data(os.path.join(op, 'x_train')), read_data(os.path.join(op, 'y_train'))
            x_val, y_val = read_data(os.path.join(op, 'x_val')), read_data(os.path.join(op, 'y_val'))
            #x_cv, x_val, y_cv, y_val = SM.simple_train_test_data_split(x, y)
            #pd.DataFrame(x_cv).to_csv(os.path.join(op, 'x_cv.csv'), index=False)
            #pd.DataFrame(x_val).to_csv(os.path.join(op, 'x_val.csv'), index=False)
            #pd.DataFrame(y_cv).to_csv(os.path.join(op, 'y_cv.csv'), index=False)
            #pd.DataFrame(y_val).to_csv(os.path.join(op, 'y_val.csv'), index=False)
  
            best_search, cv_results = self.__nested_train__(reg, x_cv, y_cv, param, inner_fold_obj, outer_fold_obj)
            print(cv_results)
            self.__cv_results_record__(reg, reg_table_group, cv_results)
            self.__cv_model_save__(reg, cv_results, x_val, y_val)
            self.__cv_plot__(reg, cv_results['train_r2'], cv_results['test_r2'])
            del best_search, cv_results, x_cv, x_val, y_cv, y_val

    def __nested_train__(self, reg, x_cv, y_cv, param, inner_fold_obj, outer_fold_obj):
        """
        Nested Cross Validation
        Pass the gridSearch estimator to cross_val_score
        This will be your required 10 x 5 cvs
        10 for outer cv and 5 for gridSearch's internal CV
        :param reg:
        :param x_cv:
        :param y_cv:
        :param kwargs:
        best_search = GridSearchCV(estimator=reg, param_grid=param, cv=self.inner_fold_val)
        cross_validate(reg, x_train, y_train, cv=out_fold_val, return_train_score=True, return_estimator=True)
        :return:
        """
        modelname = reg.__class__.__name__
        print("training start: {}".format(modelname))
        best_search = GridSearchCV(estimator=reg,
                                   param_grid=param,
                                   refit=True,
                                   cv=inner_fold_obj)
        cv_results = cross_validate(best_search,
                                    x_cv,
                                    y_cv,
                                    cv=outer_fold_obj,
                                    scoring=('explained_variance',
                                             'max_error',
                                             'neg_mean_absolute_error',
                                             'neg_mean_squared_error',
                                             'neg_root_mean_squared_error',
#                                             'neg_mean_squared_log_error',
                                             'neg_median_absolute_error',
#                                             'neg_mean_poisson_deviance',
#                                             'neg_mean_gamma_deviance',
                                             'r2'),
                                    return_train_score=True,
                                    return_estimator=True)
        print("training end: {}".format(modelname))
        return best_search, cv_results

    def __cv_plot__(self, reg, train_scores, test_scores):
        modelname = reg.__class__.__name__
        op = self.output_paths[modelname]

        plt.figure()
        cv_plot(train_scores, test_scores, plot_setting = None)
        plt.savefig(os.path.join(op, modelname + "_cvCurve.svg"))
        plt.close()

    def learning_curve_plot(self, reg, x_cv, y_cv, fold_obj):
        modelname = reg.__class__.__name__
        op = self.output_paths[modelname]

        train_sizes, train_scores, valid_scores = learning_curve(reg, x_cv, y_cv, cv=fold_obj)
        #print(np.std(train_scores))
        train_scores_mean = np.mean(train_scores, axis=1)
        train_scores_std = np.std(train_scores, axis=1)
        test_scores_mean = np.mean(valid_scores, axis=1)
        test_scores_std = np.std(valid_scores, axis=1)

        plt.figure()
        # box-like grid
        plt.grid()

        # plot the std deviation as a transparent range at each training set size
        plt.fill_between(train_sizes, train_scores_mean - train_scores_std, train_scores_mean + train_scores_std,
                     alpha=0.1, color="r")
        plt.fill_between(train_sizes, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std,
                     alpha=0.1, color="g")

        # plot the average training and test score lines at each training set size
        plt.plot(train_sizes, train_scores_mean, 'o-', color="r", label="Training score")
        plt.plot(train_sizes, test_scores_mean, 'o-', color="g", label="Cross-validation score")

        plt.title(modelname)
        plt.legend(loc="best", labels=['Training score', 'Cross-validation score'])
        plt.xlabel("Training Samples.")
        plt.ylabel("Score")
        plt.gca().invert_yaxis()

        # sizes the window for readability and displays the plot
        # shows error from 0 to 1.1
        plt.ylim(-.1, 1.1)
        plt.savefig(os.path.join(op,modelname + "_learningCurve.svg"))
        plt.close()
