'''
@Author: CY Peng
@Date: 2020-02-03
@LastEditTime: 2020-02-03

DataVer  - Author - Note
2020/02/03 - C.Y. - Initialization

Ref.
1. https://machinelearningmastery.com/grid-search-arima-hyperparameters-with-python/ Grid Search for ARIMAX
2. http://ihong-blog.logdown.com/posts/453361-non-seasonal-arima-case-study ARIMA Model Established
3. https://medium.com/@josemarcialportilla/using-python-and-auto-arima-to-forecast-seasonal-time-series-90877adff03c ARIMA Prediction
4. https://www.statsmodels.org/devel/generated/statsmodels.tsa.statespace.structural.UnobservedComponents.html Unobserved Components Setting
5. https://machinelearningmastery.com/time-series-forecast-uncertainty-using-confidence-intervals-python/ prediction confidence
6. https://aegis4048.github.io/comprehensive_confidence_intervals_for_python_developers confidence intervals
'''

import CommonUtility as CU
import pickle
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import h5py
import H5FileManager as H5FM
import math
import glob
import shutil
import matplotlib.pyplot as plt
from datetime import datetime
from scipy import stats

# Custom Lib
import ExploratoryDataAnalysis as EDA

# Time Series Model
from fbprophet.diagnostics import cross_validation, performance_metrics
from fbprophet.plot import plot_cross_validation_metric
from sklearn.model_selection import ParameterGrid
from statsmodels.tsa.stattools import adfuller
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from statsmodels.tsa.arima_process import ArmaProcess
from statsmodels.tsa.seasonal import seasonal_decompose
from statsmodels.tsa.statespace.sarimax import SARIMAX, SARIMAXResults
from statsmodels.tsa.statespace.varmax import VARMAX
from statsmodels.tsa.arima_model import ARMA, ARIMA, ARIMAResults
from statsmodels.tsa.statespace.structural import UnobservedComponents, UnobservedComponentsResults 
from statsmodels.tsa.statespace.dynamic_factor import DynamicFactor

"""
Autocorrelation - The autocorrelation function (ACF) measures how a series is correlated with itself at different lags.
Partial Autocorrelation - The partial autocorrelation function can be interpreted as a regression of the series against its past lags. The terms can be interpreted the same way as a standard linear regression, that is the contribution of a change in that particular lag while holding others constant.

Ref:
https://www.quora.com/What-is-the-difference-among-auto-correlation-partial-auto-correlation-and-inverse-auto-correlation-while-modelling-an-ARIMA-series
https://stackoverflow.com/questions/35339139/where-is-the-documentation-on-pandas-freq-tags freq tag
https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#offset-aliases frequency and method
https://www.datasciencesmachinelearning.com/2019/01/arimasarima-in-python.html Model Analysis
"""

_output_main_folder = 'ML_output'
CU.createOutputFolder(_output_main_folder)

def read_data(filename):
    data = pd.read_csv(filename+".csv")
    #data = data.fillna(value="unknown")
    return data

def df_data2ts_data(df_train, df_val, ds_column, y_column, freq, resample = None):
    #max_train_val = max(df_train[y_column])
    #min_train_val = min(df_train[y_column])
    #print(df_train, df_val, ds_column, y_column)
    df_train_type = {'datetime': df_train[ds_column[0]].map(lambda x: datetime.strptime(x, '%Y-%m-%d')).tolist(),
                     'y': df_train[y_column].values.tolist()}
    df_train = pd.DataFrame(data = df_train_type) #, index = False
    df_train = df_train.set_index('datetime') #, inplace = True
    #df_train.index = df_train[ds_column[0]]
    df_train = df_train.asfreq(freq, method='pad')

    df_val_type = {'datetime': df_val[ds_column[0]].map(lambda x: datetime.strptime(x, '%Y-%m-%d')).tolist(),
                   'y': df_val[y_column].values.tolist()}
    df_val = pd.DataFrame(data = df_val_type) #, index = False
    df_val = df_val.set_index('datetime') #, inplace = True
    df_val = df_val.asfreq(freq, method='pad')
    
    #print(df_train, df_val)
    #print(df_train.index, df_val.index)
    #df_train = df_train.dropna()
    #df_val = df_val.dropna()
    #df_val.index = df_val[ds_column[0]]
    if resample == 'M':
       df_train = df_train.resample(resample).mean()
       df_train = df_train.asfreq(resample, method='pad')
       df_val = df_val.resample(resample).mean()
       df_val = df_val.asfreq(resample, method='pad')
       #print(df_train, df_train.index)

    return df_train, df_val

def ARIMA_ACF_plot(df_data, y_plot_tag, lags = 25, title = "ACF Plot", ax = None, alpha = 0.05):
    fig = plot_acf(df_data[y_plot_tag],lags=lags,title=title,ax=ax, alpha =alpha) #,layout = 'tightly'
    plt.xlabel('Number of Lags')
    plt.ylabel('correlation')
    #plt.show()
    return fig

def ARIMA_PACF_plot(df_data, y_plot_tag, lags = 25, title = "PACF Plot", ax = None, alpha = 0.05):
    fig = plot_pacf(df_data[y_plot_tag],lags=lags,title=title,ax=ax, alpha =alpha) #,layout = 'tightly'
    plt.xlabel('Number of Lags')
    plt.ylabel('correlation')
    #plt.show()
    return fig

"""
Trend - Consistent upwards or downwards slope of a time series
Seasonality - Clear periodic pattern of a time series(like sine funtion)
Noise - Outliers or missing values

Ref
1. https://machinelearningmastery.com/decompose-time-series-data-trend-seasonality/ Time Series Decomposition
2. https://zhuanlan.zhihu.com/p/35282988 Time Series Analysis
"""

def decomposition(df_data, y_plot_tag, period=360, model = 'additive'):
    decomposed_volume = seasonal_decompose(df_data[y_plot_tag] ,period=period, model = 'additive') #, model= 'multiplicative'
    #print(decomposed_volume.trend)
    #print(df_data)
    return decomposed_volume

"""
Augmented Dickey-Fuller test
An augmented Dickey–Fuller test (ADF) tests the null hypothesis that a unit root is present in a time series sample. It is basically Dickey-Fuller test with more lagged changes on RHS.
"""
def ADF_test(df_data, y_plot_tag):
    # Augmented Dickey-Fuller test on volume of google and microsoft stocks 
    adf = adfuller(df_data[y_plot_tag])
    return adf

"""
A stationary time series is one whose statistical properties such as mean, variance, autocorrelation, etc. are all constant over time.

Strong stationarity: is a stochastic process whose unconditional joint probability distribution does not change when shifted in time. Consequently, parameters such as mean and variance also do not change over time.
Weak stationarity: is a process where mean, variance, autocorrelation are constant throughout the time
Stationarity is important as non-stationary series that depend on time have too many parameters to account for when modelling the time series. diff() method can easily convert a non-stationary series to a stationary series.

We will try to decompose seasonal component of the above decomposed time series.
"""
def stationary_analysis(decomposed_volume):
    decomposed_volume.trend.plot()
    decomposed_volume.trend.diff().plot()

"""
Stats Models Methods

Ref.
https://www.statsmodels.org/dev/statespace.html
"""
def ARIMA_model_established(train_sample, order):
    # Order = (AR,I,MA)
    model = ARIMA(train_sample, order=order)
    result = model.fit()
    print(result.summary())
    result.plot_predict(start=700, end=1000)
    plt.show()

def VARMAX_model_established(train_sample, order, trend):
    # Order = (AR, MA)
    # trend = ‘n’,’c’,’t’,’ct’
    model = VARMAX(train_sample, order=order, trend=trend)
    result = model.fit(maxiter=1000, disp=False)
    print(result.summary())
    predicted_result = result.predict(start=0, end=1000)
    result.plot_diagnostics()

def SARIMAX_model_established(train_sample, order, trend):
    # Order = (AR, I, MA) or (AR, I, MA, S)
    # trend = ‘n’,’c’,’t’,’ct’
    model = SARIMAX(train_sample, order=order, trend=trend)
    result = model.fit(maxiter=1000,disp=False)
    print(result.summary())
    predicted_result = result.predict(start=0, end=500)
    result.plot_diagnostics()

def unobserved_components_model_established(train_sample, order):
    model = UnobservedComponents(train_sample,'local level')
    result = model.fit(maxiter=1000,disp=False)
    print(result.summary())
    predicted_result = result.predict(start=0, end=500)
    result.plot_diagnostics()

def dynamic_factor_model_established(train_sample, order):
    model = DynamicFactor(train_sample, k_factors=1, factor_order=2)
    result = model.fit(maxiter=1000,disp=False)
    print(result.summary())
    predicted_result = result.predict(start=0, end=1000)
    result.plot_diagnostics()

def prophet_err_plot(filename, df_table, title_name = ' CV Test:'):
    specs = EDA.multiplot_strategy(6)
    fig = plt.figure(figsize=(35, 20))

    ax = plt.subplot(specs[0])
    MAPEFig = plot_cross_validation_metric(df_table, metric='mape', ax=ax, figsize=(35, 20))
    plt.grid()
    plt.legend()
    plt.ylabel('Mean Absolute Percent Error, MAPE')
    plt.title(filename + title_name + ' MAPE')

    ax = plt.subplot(specs[1])
    MAEFig = plot_cross_validation_metric(df_table, metric='mae', ax=ax, figsize=(35, 20))
    plt.grid()
    plt.legend()
    plt.ylabel('Mean Absolute Error, MAE')
    plt.title(filename + title_name + ' MAE')

    ax = plt.subplot(specs[2])
    MSEFig = plot_cross_validation_metric(df_table, metric='mse', ax=ax, figsize=(35, 20))
    plt.grid()
    plt.legend()
    plt.ylabel('Mean Squart Error, MSE')
    plt.title(filename + title_name + ' MSE')

    ax = plt.subplot(specs[3])
    RMSEFig = plot_cross_validation_metric(df_table, metric='rmse', ax=ax, figsize=(35, 20))
    plt.grid()
    plt.legend()
    plt.ylabel('Root Mean Squart Error, RMSE')
    plt.title(filename + title_name + ' RMSE')

    ax = plt.subplot(specs[4])
    ExpVarFig = plot_cross_validation_metric(df_table, metric='exp_var_score', ax=ax, figsize=(35, 20))
    plt.grid()
    plt.legend()
    plt.ylabel('Explained Variance Score')
    plt.title(filename + title_name + ' Explained Variance Score')

    ax = plt.subplot(specs[5])
    R2Fig = plot_cross_validation_metric(df_table, metric='r2', ax=ax, figsize=(35, 20))
    plt.grid()
    plt.legend()
    plt.ylabel('R2 Score, R2')
    plt.title(filename + title_name + ' R2 Score')
    
    return R2Fig

def CI_printout(series, interval = 0.95, method = 't'):
    mean_val = series.mean()
    n = series.count()
    stdev = series.std()
    if method == 't':
      test_stat = stats.t.ppf((interval + 1)/2, n)
    elif method == 'z':
      test_stat = stats.norm.ppf((interval + 1)/2)
    lower_bound = mean_val - test_stat * stdev / math.sqrt(n)
    upper_bound = mean_val + test_stat * stdev / math.sqrt(n)
    return lower_bound, upper_bound
  
def ts_cv_test(df_train, start, end, output_path, filename):
    # initial='1825 days', period='365 days'
    if 'SARIMAX' in filename:
       loaded_model = SARIMAXResults.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))    
       predObj = loaded_model.get_prediction(start=start, end=end)
       pred_series = predObj.predicted_mean
    elif 'ARIMA' in filename:
       loaded_model = ARIMAResults.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
       pred_series = loaded_model.predict(start=start, end=end)
       #fig = loaded_model.plot_predict(start='2015', end='2016', plot_insample=True)
       #fig.savefig(os.path.join(output_path, filename + '.svg'))
    elif 'UnobservedComponents' in filename:
       loaded_model = UnobservedComponentsResults.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
    #print(loaded_model.summary_frame())
    #print(loaded_model.conf_int(alpha=0.05))
    df_pred = pred_series.to_frame(name= 'yhat')
    df_cv_table = pd.concat([df_pred,df_train], join='inner', axis=1)
    df_cv_table['ds'] = df_cv_table.index
    df_cv_table['cutoff'] = df_cv_table.index
    lower_bound, upper_bound = CI_printout(df_pred, interval = 0.95, method = 't')
    df_cv_table['yhat_lower'] = lower_bound
    df_cv_table['yhat_upper'] = upper_bound
    df_cv_table = df_cv_table.reset_index()
    #print(df_cv_table)
    #df_performance = None
    #df_cv_table = cross_validation(loaded_model, val_days, **cv_params)
    df_performance = performance_metrics(df_cv_table)
    #print(df_performance)
    
    return df_cv_table, df_performance

def ts_validation(df_val, output_path, filename):
    steps = df_val.shape[0]
    #print(steps)
    if 'SARIMAX' in filename:
       loaded_model = SARIMAXResults.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))    
       predObj = loaded_model.get_forecast(steps = steps)
       pred_series = predObj.predicted_mean
       pred_series_conf = predObj.conf_int()
       print(pred_series_conf)
    elif 'ARIMA' in filename:
       loaded_model = ARIMAResults.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
       pred_series, pred_series_stderr, pred_series_conf = loaded_model.forecast(steps = steps)
    elif 'UnobservedComponents' in filename:
       loaded_model = UnobservedComponentsResults.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
       pred_series, pred_series_stderr, pred_series_conf = loaded_model.forecast(steps = steps)
    df_val_table = pd.DataFrame(pred_series, columns= ['yhat'])
    #df_val_table = pd.concat([df_val_table,df_val], join='inner', axis=1)
    df_val_table['ds'] = df_val.index
    df_val_table = df_val_table.reset_index()

    df_val_table['cutoff'] = [df_val_table.loc[0, 'ds']]*df_val_table.shape[0]
    df_val_table['yhat_lower'] = pred_series_conf.iloc[:, 0].values.tolist()
    df_val_table['yhat_upper'] = pred_series_conf.iloc[:, 1].values.tolist()
    #print('Test LU', pred_series_conf.iloc[:, 0].values.tolist())
    #print(df_val_table)
    df_val_table['y'] = sum(df_val.values.tolist(),[])
    try:
        df_val_table['stderr'] = pred_series_stderr
    except:
        pass
    #print(df_val_table)
    #print(df_val['y'])
    df_performance = performance_metrics(df_val_table)
    #print(df_performance)
    return df_val_table, df_performance

def ts_validation_plot(df_train, df_val, output_path, filename, figname = '', ylabel_tag = '', ax = None):
    #print(os.path.join(output_path, filename + '.sav'))
    #loaded_model = pickle.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
    steps = df_val.shape[0]
    #print(steps)
    if 'SARIMAX' in filename:
       loaded_model = SARIMAXResults.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))    
       predObj = loaded_model.get_forecast(steps = steps, dynamic = True)
       pred_series = predObj.predicted_mean
       pred_series_conf = predObj.conf_int()
    elif 'ARIMA' in filename:
       loaded_model = ARIMAResults.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
       pred_series, pred_series_stderr, pred_series_conf = loaded_model.forecast(steps = steps)
    elif 'UnobservedComponents' in filename:
       loaded_model = UnobservedComponentsResults.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
       pred_series, pred_series_stderr, pred_series_conf = loaded_model.forecast(steps = steps)
    df_val_table = pd.DataFrame(pred_series, columns= ['yhat'])
    #df_val_table = pd.concat([df_val_table,df_val], join='inner', axis=1)
    df_val_table['ds'] = df_val.index
    df_val_table = df_val_table.reset_index()

    df_val_table['cutoff'] = [df_val_table.loc[0, 'ds']]*df_val_table.shape[0]
    df_val_table['yhat_lower'] = pred_series_conf.iloc[:, 0].values.tolist()
    df_val_table['yhat_upper'] = pred_series_conf.iloc[:, 1].values.tolist()
    df_val_table['y'] = sum(df_val.values.tolist(),[])
    try:
        df_val_table['stderr'] = pred_series_stderr
    except:
        pass
    df_train['ds'] = df_train.index
    
	# 
    cutoff = df_val_table['cutoff'].unique()[0]
    df_val_table = df_val_table[df_val_table['cutoff'].values == cutoff]
    if ax == None:
       err_plot = plt.figure(facecolor='w', figsize=(10, 6))
       ax = err_plot.add_subplot(111)
    else:
       err_plot = ax.get_figure()
    plt.plot(df_train['ds'].values, df_train['y'], 'k.-', label = 'Historical Data')
    plt.plot(df_val_table['ds'].values, df_val_table['y'], 'r.-', label = 'True Validation Data')
    plt.plot(df_val_table['ds'].values, df_val_table['yhat'], ls='-', c='#0072B2', label = 'Validation Forecasting Data')
    ax.fill_between(df_val_table['ds'].values, df_val_table['yhat_lower'],
                    df_val_table['yhat_upper'], color='#0072B2',
                    alpha=0.2)
    #ax.axvline(x=pd.to_datetime(cutoff), c='gray', lw=4, alpha=0.5, ls='--')
    #ax.set_ylabel('y')
    #ax.set_xlabel('ds')
    #ax.axvline(x=pd.to_datetime(cutoff) + pd.Timedelta(val_days), c='gray', lw=4,
    #           alpha=0.5, ls='--')

    plt.grid()
    plt.legend()
    plt.xlabel('Date')
    plt.ylabel('Time Series Value, '+ylabel_tag)
    plt.title('{} Time Series Validation Plot'.format(figname))
	
    #df_forecast_plot = loaded_model.plot_predict('2017', '2020', ax = ax, plot_insample=True)
    #plt.grid()
    #plt.plot(df_forecast['ds'], df_val['y'], 'r', label = 'True Validation Data')
    #plt.grid()
	#
    #plt.legend()
    #plt.xlabel('Date')
    #plt.ylabel('Time Series Value, '+ylabel_tag)
    #plt.title('{} Time Series Validation Plot'.format(figname))
    ##plt.savefig(os.path.join(output_path, filename + "_PredPlot.svg"))
    return err_plot

def cv_results_record_init(reg_table_length):
    reg_table_dataType = np.dtype([('Regressior Name', h5py.special_dtype(vlen=str)),
                                   ('Regressior Parameters', h5py.special_dtype(vlen=str)),
                                   ('Regressior CV R2', np.float),
                                   #('Regressior CV Horizon', np.float),
                                   ('Regressior CV Mean Squart Error, MSE', np.float),
                                   #('Regressior CV Mean Squart Error (C), MSE', np.float),
                                   ('Regressior CV Root Mean Squart Error, RMSE', np.float),
                                   ('Regressior CV Median Absolute Error', np.float),
                                   ('Regressior CV Mean Absolute Error, MAE', np.float),
                                   #('Regressior CV Mean Absolute Error (C), MAE', np.float),
                                   ('Regressior CV Explained Variance', np.float),
								   ('Regressior CV MAPE', np.float),
                                   ('Regressior CV Coverage', np.float),
								   ('Regressior Val R2', np.float),
                                   ('Regressior Val Mean Squart Error, MSE', np.float),
                                   ('Regressior Val Root Mean Squart Error, RMSE', np.float),
                                   ('Regressior Val Median Absolute Error', np.float),
								   ('Regressior Val Mean Absolute Error, MAE', np.float),
                                   ('Regressior Val Explained Variance', np.float),
                                   ('Regressior Val MAPE', np.float),
                                   ('Regressior Val Coverage', np.float)])
    reg_table = np.zeros((reg_table_length,), dtype=reg_table_dataType)
    return (reg_table, reg_table_dataType)

def cv_reg_table_record(modelname, reg_table_group, all_results, row_index, base_output_folder, output_folder):
    (reg_table, reg_table_dataType) = reg_table_group
    (df_cv_table, df_cv_performance, df_val_table, df_val_performance) = all_results 
    #print(df_cv_table)
    #print(df_cv_performance)
    #print(df_val_table)
    #print(df_val_performance)
    reg_table['Regressior Name'][row_index] = modelname
    reg_table['Regressior Parameters'][row_index] = None #cv_results['estimator']#str(clf.get_params())
    #reg_table['Regressior CV Horizon'][row_index] = df_cv_performance['horizon'].mean()
    #reg_table['Regressior CV R2'][row_index] = df_cv_performance['r2'].mean()
    #reg_table['Regressior CV Mean Squart Error, MSE'][row_index] = df_cv_performance['mse'].mean()
    ##reg_table['Regressior CV Mean Squart Error (C), MSE'][row_index] = df_cv_performance['mse'].mean()
    #reg_table['Regressior CV Root Mean Squart Error, RMSE'][row_index] = df_cv_performance['rmse'].mean()
    ##reg_table['Regressior CV Median Absolute Error'][row_index] = df_cv_table['median absolute error'].mean()
    #reg_table['Regressior CV Mean Absolute Error, MAE'][row_index] = df_cv_performance['mae'].mean()
    ##reg_table['Regressior CV Mean Absolute Error (C), MAE'][row_index] = df_cv_performance['mae'].mean()
    #reg_table['Regressior CV Explained Variance'][row_index] = df_cv_performance['exp_var_score'].mean()
    #reg_table['Regressior CV MAPE'][row_index] = df_cv_performance['mape'].mean()
    #reg_table['Regressior CV Coverage'][row_index] = df_cv_performance['coverage'].mean()
    reg_table['Regressior Val R2'][row_index] = df_val_performance['r2'].mean()
    reg_table['Regressior Val Mean Squart Error, MSE'][row_index] = df_val_performance['mse'].mean()
    reg_table['Regressior Val Root Mean Squart Error, RMSE'][row_index] = df_val_performance['rmse'].mean()
    #reg_table['Regressior Val Median Absolute Error'][row_index] = df_val_performance['median_absolute_error'].mean()
    reg_table['Regressior Val Mean Absolute Error, MAE'][row_index] = df_val_performance['mae'].mean()
    reg_table['Regressior Val Explained Variance'][row_index] = df_val_performance['exp_var_score'].mean()
    reg_table['Regressior Val MAPE'][row_index] = df_val_performance['mape'].mean()
    reg_table['Regressior Val Coverage'][row_index] = df_val_performance['coverage'].mean()

    # Results Save
    reg_cv_table = np.sort(reg_table, order='Regressior Val Root Mean Squart Error, RMSE')
    H5FM.Data2H5(reg_cv_table, os.path.join(base_output_folder, 'SimpleModelAnalysis'),
                'REG Nested CV Table', groupName="ML Analysis", dtype=reg_table_dataType)
    df_reg_cv_table = pd.DataFrame(reg_cv_table)
    df_reg_cv_table.to_csv(os.path.join(output_folder,  modelname + "_reg_table.csv"), index=False)
    df_reg_cv_table.to_csv(os.path.join(output_folder,  "all_model_reg_table.csv"), index=False, float_format = '%.2f')
    df_reg_cv_table.drop(columns = ['Regressior Parameters']).to_csv(os.path.join(output_folder,  "all_model_reg_table_d.csv"), index=False, float_format = '%.2f')


class tsMLHelper():
      def __init__(self, reg_name, params):
          self.grid_params = ParameterGrid(params[reg_name])
          self.regs_name = self.__grid_search_models_name__(reg_name)
          self.ml_output_count = 0
          self.base_path = CU.updateOutputFolder(_output_main_folder+'//', 'SimpleModel')
          self.output_path = self.__output_paths__()

      def __grid_search_models_name__(self, reg_name):
          regs_name = []
          #print(self.grid_params)
          for idx, param in enumerate(self.grid_params):
              #regs.append(reg(**param))
              regs_name.append(reg_name+'_'+str(idx))
          return regs_name

      def __output_paths__(self):
          global _output_main_folder
          paths = {}
          paths = [None]*len(self.regs_name)
          #regs  = []
          #print(self.grid_params)
          for idx, reg_name in enumerate(self.regs_name):
              modelname = self.regs_name[idx]
              paths[idx] = os.path.join(self.base_path,
                                        modelname)
              CU.createOutputFolder(paths[idx])
              #print(self.grid_params[idx])
              pd.DataFrame(list(self.grid_params[idx].items())).to_csv(os.path.join(paths[idx], 'param_setting.csv'), index = False)
	          #regs.append(reg(, **param))
        
          setting_files_list = glob.glob('*.txt')
          for copy_file_path in setting_files_list:
              shutil.copy(copy_file_path, self.base_path)
          self.ml_output_count += 1
          return paths

      def data_preparation(self, raw_x, raw_y, 
	                       tail_name_to_train, 
						   fit_model_file_path, 
                           FeatureEngineeringFitObj, 
						   LabelEncoderFitObj = None, 
						   train_drop_column = None,
						   train_size = 0.77):
          for idx, modelname in enumerate(self.regs_name):
              op = self.output_path[idx]
              length = raw_y.shape[0]
              train_idx = int(length*train_size)
              x_train, x_val = raw_x.iloc[0:train_idx,:], raw_x.iloc[train_idx:,:]
              y_train, y_val = raw_y.iloc[0:train_idx,:], raw_y.iloc[train_idx:,:]
              x_train.to_csv(os.path.join(op, 'x_raw_train.csv'), index=False)
              x_val.to_csv(os.path.join(op, 'x_raw_val.csv'), index=False)
              y_train.to_csv(os.path.join(op, 'y_raw_train.csv'), index=False)
              y_val.to_csv(os.path.join(op, 'y_raw_val.csv'), index=False)
              
              y_train.to_csv(os.path.join(op, 'y_train.csv'), index=False)
              y_val.to_csv(os.path.join(op, 'y_val.csv'), index=False)
              
              if (LabelEncoderFitObj):
                 LabelEncoderFitObj('y_train', op, fit_model_file_path)
                 LabelEncoderFitObj('y_val', op, fit_model_file_path)

              train = pd.concat([x_train, y_train], axis=1)
              train.to_csv(os.path.join(op, "train.csv"), index=False)
            
              val=pd.concat([x_val, y_val], axis=1)
              val.to_csv(os.path.join(op, "val.csv"), index=False)

              FeatureEngineeringFitObj("train", op, fit_model_file_path)
              FeatureEngineeringFitObj("val", op, fit_model_file_path)
			  
              train_data = read_data(os.path.join(op, "train"+tail_name_to_train))
              if (train_drop_column):
                  train_data = train_data.drop(columns= train_drop_column, axis=1)
              train_data.to_csv(os.path.join(op, 'x_train.csv'), index=False)
              cv_test_data = read_data(os.path.join(op, "val"+tail_name_to_train))
              if (train_drop_column):
                  cv_test_data = cv_test_data.drop(columns= train_drop_column, axis=1)
              cv_test_data.to_csv(os.path.join(op, 'x_val.csv'), index=False)

              #splitObj = timeseries_sampling_split_obj(n_splits = 10, max_train_size = 0.77)
              #return splitObj

      def cv_train(self, ds_column = ['ds'], ts_model_column = ['a'], resample = None):
          reg_table_group = cv_results_record_init(len(self.grid_params))
          regs = []

          for idx, param in enumerate(self.grid_params):
              if (resample == None):
                  resample = param['resample']
              print(param)
              for y_column in ts_model_column:
                  op = self.output_path[idx]
                  reg_name = self.regs_name[idx]
                  filename = y_column + '_' + reg_name
                  x_train, y_train = read_data(os.path.join(op, 'x_train')), read_data(os.path.join(op, 'y_train'))
                  x_val, y_val = read_data(os.path.join(op, 'x_val')), read_data(os.path.join(op, 'y_val'))
                  df_train, df_val = df_data2ts_data(x_train, x_val, ds_column, y_column, param['freq'], resample)

                  # CV Training/Testing and Validation
                  try:
                     if (resample):
                         param['freq'] = resample
                     reg = self.__train__(reg_name, df_train, op, filename, param)
                     regs.append(reg)
                     
                     df_val_table, df_val_performance = self.__validation__(df_train, df_val, op, filename, ylabel_tag = y_column)
                     df_cv_table, df_cv_performance = self.__cv_test__(df_train, op, filename)
                     self.__cv_results_record__(reg_table_group, df_cv_table, df_cv_performance, df_val_table, df_val_performance, op, 'ts_'+ filename, idx)
                     plt.close('all')
                  except:
                     continue
          self.regs = regs 

      def train(self, x_train, y_train):
          obj = self.regs[0].fit(x_train, y_train)
          self.__model_save__(obj)           

      def __cv_results_record__(self, reg_table_group, df_cv_table, df_cv_performance, df_val_table, df_val_performance, output_path, filename, row_index):
          #print(df_val_table)
          df_cv_table.to_csv(os.path.join(output_path,"{}_cv_table.csv".format(filename)), index=False)
          df_cv_performance.to_csv(os.path.join(output_path,"{}_cv_performance.csv".format(filename)), index=False)
          df_val_table.to_csv(os.path.join(output_path,"{}_val_table.csv".format(filename)), index=False)
          df_val_performance.to_csv(os.path.join(output_path,"{}_val_performance.csv".format(filename)), index=False)
          all_results = (df_cv_table, df_cv_performance, df_val_table, df_val_performance)
          cv_reg_table_record(filename, reg_table_group, all_results, row_index, self.base_path, output_path)		   

      def __validation__(self, df_train, df_val, output_path, filename, ylabel_tag = None):
          # Time Series Training/ Testing
          #ts_cv_test(df_train, datetime(2015,1,1), datetime(2016,1,1), output_path, filename)
          df_forecast_table, df_forecast_performance = ts_validation(df_val, output_path, filename)
          ValErrFig = prophet_err_plot(filename, df_forecast_table, title_name = ' Validation:')
          ValErrFig.savefig(os.path.join(output_path, filename + "_ValidationEvaluation.svg") )

          df_forecast_plot = ts_validation_plot(df_train, df_val, output_path, filename, figname = filename, ylabel_tag = ylabel_tag, ax = None)
          df_forecast_plot.savefig(os.path.join(output_path, filename + "_ForecastPlot.svg") )        
          #df_cv_table, df_cv_performance = prophet_cv_test(output_path, filename, **cv_params)
          #CVErrFig = prophet_err_plot(filename, df_cv_table, title_name = ' CV Test:')
          #CVErrFig.savefig(os.path.join(output_path, filename + "_CV_Evaluation.svg") )
          #
          #cvFig = prophet_cv_plot(df_cv_table, output_path, filename, figname = 'Prophet_'+filename, ylabel_tag = ylabel_tag, ax = None, **cv_params)
          #
          #cvFig.savefig(os.path.join(output_path, filename + "_CVPlot.svg") )

          return df_forecast_table, df_forecast_performance
 
      def __cv_test__(self, df_train, output_path, filename):
          # Time Series Training/ Testing
          #ts_cv_test(df_train, datetime(2015,1,1), datetime(2016,1,1), output_path, filename)
          
          #print(df_train)
          cv_test_idx = int(df_train.shape[0]*0.7)
          #print(df_train.index.tolist())
          #print(df_train.index.tolist()[cv_test_idx])
          #print(df_train.index.tolist()[-1])
          df_cv_table, df_cv_performance = ts_cv_test(df_train, df_train.index.tolist()[cv_test_idx], df_train.index.tolist()[-1], output_path, filename)
          #print(df_cv_table)
          CVErrFig = prophet_err_plot(filename, df_cv_table, title_name = ' CV Test:')
          CVErrFig.savefig(os.path.join(output_path, filename + "_CV_Evaluation.svg") )

          #cvFig = prophet_cv_plot(df_cv_table, output_path, filename, figname = 'Prophet_'+filename, ylabel_tag = ylabel_tag, ax = None, **cv_params)
          #
          #cvFig.savefig(os.path.join(output_path, filename + "_CVPlot.svg") )

          return df_cv_table, df_cv_performance

      def __train__(self, reg_name, df_train, output_path, filename, param):
          if 'SARIMAX' in reg_name:
             print('SARIMAX Start')
             order = (param['p0'],param['d0'],param['q0'])
             seasonal_order = (param['p'],param['d'],param['q'],param['s'])
             trend = param['trend']#‘n’,’c’,’t’,’ct’
             reg = SARIMAX(df_train, order = order, seasonal_order=seasonal_order, trend = trend, enforce_stationarity=False, enforce_invertibility=False, freq = param['freq'])  
          elif 'ARIMA' in reg_name:
             print('ARIMA Start')
             order = (param['p'],param['d'],param['q'])
             reg = ARIMA(df_train, order = order, freq = param['freq'])
          elif 'UnobservedComponents' in reg_name:
             print('UnobservedComponents Start')
             #print(param['level'])
             #print(param)
             reg = UnobservedComponents(df_train, level = param['level'])

          result = reg.fit(disp=False)
          result.save(open(os.path.join(output_path, "{}.sav".format(filename)), 'wb'))
          #pickle.dump(reg, open(os.path.join(output_path, "{}.sav".format(filename)), 'wb'))
          f = open(os.path.join(output_path,  filename + "_summary.csv"),'w')
          f.write(result.summary().as_csv())
          f.close()
          try:
              plt = result.plot_diagnostics()
              plt.savefig(os.path.join(output_path,  filename + "_result_diagnostics.svg"))
          except:
              pass
		  
          #print(result)
          #pred_series = result.predict(start=datetime(2015,1,1),end= datetime(2016,1,1))
          #print('check')
          #print(pred_series)
          #pd.DataFrame(result.summary().as_csv()).to_csv(os.path.join(output_path,  filename + "_reg_table.csv"), index=False)
          #pd.DataFrame(result.summary().as_csv()).to_csv(os.path.join(output_path,  filename + "_reg_table_d.csv"), index=False, float_format = '%.2f')