'''
@Author: CY Peng
@Date: 2019-11-6
@LastEditTime: 2020/01/14

DataVer  - Author - Note
2019/11/6     CY    Initialization
2019/11/7     CY    Model Helper
2019/11/11     CY   ROC Plot
2019/11/11     CY   Features Importance
2019/12/4      CY   Save the Model Results Detail
2019/12/6     CY    Function Reorganization
2019/12/18    CY    Prediction Error Calculation
2019/12/20    CY    Data Preparation
2020/01/14    CY    New CV Results

Ref.
https://datascience.stackexchange.com/questions/45046/cross-validation-for-highly-imbalanced-data-with-undersampling, Imbalance Data Model
https://datascience.stackexchange.com/questions/29520/how-to-plot-learning-curve-and-validation-curve-while-using-pipeline, Grid Search First, Learning Curve Second
https://scikit-learn.org/stable/auto_examples/model_selection/plot_nested_cross_validation_iris.html, nested cross validation
'''
import CommonUtility as CU
import pickle
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import h5py
import H5FileManager as H5FM
import math
import glob
import shutil

import SamplingLib as SM
import ModelEvaluation as ME

# Cross Validation for Multi Classifier
from sklearn.model_selection import cross_validate, cross_val_score, learning_curve, GridSearchCV, train_test_split, validation_curve
from sklearn.multiclass import OneVsRestClassifier

_output_main_folder = 'ML_output'

def read_data(filename):
    data = pd.read_csv(filename+".csv")
    #data = data.fillna(value="unknown")
    return data

def cv_plot(train_scores, test_scores, plot_setting = None):
    # box-like grid
    plt.grid()

    train_sizes = range(0, len(train_scores))
    # plot the average training and test score lines at each training set size
    plt.plot(train_sizes, train_scores, 'o-', color="r", label="Training score")
    plt.plot(train_sizes, test_scores, 'o-', color="g", label="Cross-validation score")

    if plot_setting == None:
        plt.title("Learning Curve")
    else:
        plt.title(plot_setting['title'])
    plt.legend(loc="best", labels=['Training score', 'Cross-validation score'])
    plt.xlabel("CV Training No.")
    plt.ylabel("Score")
    
    plt.gca().invert_yaxis()

    # sizes the window for readability and displays the plot
    # shows error from 0 to 1.1
    plt.ylim(-.1, 1.1)

def cal_pred_error(df_x_val, df_y_val, output_path, filename):
    loaded_model = pickle.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
    y_pred = loaded_model.best_estimator_.predict(df_x_val)
    df_data_features_err = df_x_val
    df_data_features_err['Pred'] = y_pred
    df_data_features_err['abs_err'] = y_pred - df_y_val.iloc[:,0].values.tolist()
    df_data_features_err.to_csv(os.path.join(output_path, filename+'_err.csv'), index=False)
    return y_pred, y_pred - df_y_val.iloc[:,0].values.tolist()

def cal_confusion_matrix(df_x_val, df_y_val, output_path, filename, conf_matrix_params):
    print(os.path.join(output_path, filename + '.sav'))
    loaded_model = pickle.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
    y_pred = loaded_model.best_estimator_.predict(df_x_val)
    df_cm, df_clf_report = ME.binary_clf_confusion_matrix(df_y_val, y_pred, conf_matrix_params, output_path, filename)
    return df_cm, df_clf_report

def cal_roc_auc_curve(df_x_val, df_y_val, output_path, filename):
    loaded_model = pickle.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
    y_pred_proba = loaded_model.best_estimator_.predict_proba(df_x_val)
    y_val_proba = np.array(df_y_val).repeat(2, axis=1)
    y_val_proba[:, 0] = 1 - y_val_proba[:, 0]
    #print(y_pred_proba)
    #print(y_val_proba)
    fpr, tpr, roc_auc = ME.binary_clf_cal_roc_auc(y_val_proba, y_pred_proba)
    return fpr, tpr, roc_auc

def get_tree_based_feature_importance(df_val_X, output_path, filename):
    modelObj = pickle.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
    importances, indices = ME.df_data_tree_based_feature_importance(modelObj)
    #ME.df_data_tree_based_feature_importance(loaded_model.best_estimator_, df_val_X, output_path, filename)
    return modelObj, importances, indices

def grid_results_record_init(table_length):
    table_dataType = np.dtype([('Classifier Name', h5py.special_dtype(vlen=str)),
                               ('Classifier Parameters', h5py.special_dtype(vlen=str)),
                               ('Classifier Train Accuracy Mean', np.float),
                               ('Classifier Time', np.float)])
    clf_table = np.zeros((table_length,), dtype=table_dataType)
    return (clf_table, table_dataType)

def grid_results_table_record(modelname, clf_table_group, est_clf, row_index, base_output_folder, output_folder):
    # Results Record
    (clf_table, table_dataType) = clf_table_group
    #modelname = clf.__class__.__name__
    #row_index = self.clfs.index(clf)
    #op = self.output_paths[modelname]
    clf_table['Classifier Name'][row_index] = modelname
    clf_table['Classifier Parameters'][row_index] = str(est_clf.best_params_)
    clf_table['Classifier Train Accuracy Mean'][row_index] = est_clf.best_score
    clf_table['Classifier Time'][row_index] = est_clf.refit_time_

    # Results Save
    clf_table = np.sort(clf_table, order='Classifier Train Accuracy Mean')[::-1]
    H5FM.Data2H5(clf_table, os.path.join(base_output_folder, 'MultiModelAnalysis'),
                 'CLF Grid Training Table', groupName="ML Analysis", dtype=table_dataType)
    df_clf_table = pd.DataFrame(clf_table)
    df_clf_table.to_csv(os.path.join(output_folder, modelname + "_clf_table.csv"), index=False)

def cv_results_record_init(clf_table_length):
    clf_table_dataType = np.dtype([('Classifier Name', h5py.special_dtype(vlen=str)),
                                   ('Classifier Parameters', h5py.special_dtype(vlen=str)),
                                   ('Classifier Train Accuracy Mean', np.float),
                                   ('Classifier Test Accuracy Mean', np.float),
                                   ('Classifier Test Accuracy 3*STD', np.float),
                                   ('Classifier Train Balanced Accuracy Mean', np.float),
                                   ('Classifier Test Balanced Accuracy Mean', np.float),
                                   ('Classifier Test Balanced Accuracy 3*STD', np.float),
                                   ('Classifier Train Average Precision Mean', np.float),
                                   ('Classifier Test Average Precision Mean', np.float),
                                   ('Classifier Test Average Precision 3*STD', np.float),
                                   ('Classifier Train Neg Brier Score Mean', np.float),
                                   ('Classifier Test Neg Brier Score Mean', np.float),
                                   ('Classifier Test Neg Brier Score 3*STD', np.float),
                                   ('Classifier Train f1 Mean', np.float),
                                   ('Classifier Test f1 Mean', np.float),
                                   ('Classifier Test f1 3*STD', np.float),
                                   ('Classifier Train f1 Micro Mean', np.float),
                                   ('Classifier Test f1 Micro Mean', np.float),
                                   ('Classifier Test f1 Micro 3*STD', np.float),
                                   ('Classifier Train f1 Macro Mean', np.float),
                                   ('Classifier Test f1 Macro Mean', np.float),
                                   ('Classifier Test f1 Macro 3*STD', np.float),
                                   ('Classifier Train Precision Mean', np.float),
                                   ('Classifier Test Precision Mean', np.float),
                                   ('Classifier Test Precision 3*STD', np.float),
                                   ('Classifier Train Recall Mean', np.float),
                                   ('Classifier Test Recall Mean', np.float),
                                   ('Classifier Test Recall 3*STD', np.float),                                   	   
                                   ('Classifier Train Jaccard Mean', np.float),
                                   ('Classifier Test Jaccard Mean', np.float),
                                   ('Classifier Test Jaccard 3*STD', np.float), 								   
                                   ('Classifier Train Roc Auc Mean', np.float),
                                   ('Classifier Test Roc Auc Mean', np.float),
                                   ('Classifier Test Roc Auc 3*STD', np.float), 
                                   ('Classifier Train Roc Auc Ovr Mean', np.float),
                                   ('Classifier Test Roc Auc Ovr Mean', np.float),
                                   ('Classifier Test Roc Auc Ovr 3*STD', np.float), 								   
                                   ('Classifier Train Roc Auc Ovo Mean', np.float),
                                   ('Classifier Test Roc Auc Ovo Mean', np.float),
                                   ('Classifier Test Roc Auc Ovo 3*STD', np.float), 								   
                                   ('Classifier Time', np.float)])
    clf_table = np.zeros((clf_table_length,), dtype=clf_table_dataType)
    return (clf_table, clf_table_dataType)

def cv_clf_table_record(modelname, clf_table_group, cv_results, row_index, base_output_folder, output_folder):
    # Results Record
    (clf_table, clf_table_dataType) = clf_table_group
    #modelname = clf.__class__.__name__
    #row_index = self.clfs.index(clf)
    #op = self.output_paths[modelname]
    clf_table['Classifier Name'][row_index] = modelname
    clf_table['Classifier Parameters'][row_index] = cv_results['estimator']#str(clf.get_params())
    clf_table['Classifier Train Accuracy Mean'][row_index] = cv_results['train_accuracy'].mean()
    clf_table['Classifier Test Accuracy Mean'][row_index] = cv_results['test_accuracy'].mean()
    clf_table['Classifier Test Accuracy 3*STD'][row_index] = cv_results['test_accuracy'].std() * 3
    clf_table['Classifier Train Balanced Accuracy Mean'][row_index] = cv_results['train_balanced_accuracy'].mean()
    clf_table['Classifier Test Balanced Accuracy Mean'][row_index] = cv_results['test_balanced_accuracy'].mean()
    clf_table['Classifier Test Balanced Accuracy 3*STD'][row_index] = cv_results['test_balanced_accuracy'].std() * 3
    clf_table['Classifier Train Average Precision Mean'][row_index] = cv_results['train_average_precision'].mean()
    clf_table['Classifier Test Average Precision Mean'][row_index] = cv_results['test_average_precision'].mean()
    clf_table['Classifier Test Average Precision 3*STD'][row_index] = cv_results['test_average_precision'].std() * 3
    clf_table['Classifier Train Neg Brier Score Mean'][row_index] = cv_results['train_neg_brier_score'].mean()
    clf_table['Classifier Test Neg Brier Score Mean'][row_index] = cv_results['test_neg_brier_score'].mean()
    clf_table['Classifier Test Neg Brier Score 3*STD'][row_index] = cv_results['test_neg_brier_score'].std() * 3
    clf_table['Classifier Train f1 Mean'][row_index] = cv_results['train_f1'].mean()
    clf_table['Classifier Test f1 Mean'][row_index] = cv_results['test_f1'].mean()
    clf_table['Classifier Test f1 3*STD'][row_index] = cv_results['test_f1'].std() * 3
    clf_table['Classifier Train f1 Micro Mean'][row_index] = cv_results['train_f1_micro'].mean()
    clf_table['Classifier Test f1 Micro Mean'][row_index] = cv_results['test_f1_micro'].mean()
    clf_table['Classifier Test f1 Micro 3*STD'][row_index] = cv_results['test_f1_micro'].std() * 3
    clf_table['Classifier Train f1 Macro Mean'][row_index] = cv_results['train_f1_macro'].mean()
    clf_table['Classifier Test f1 Macro Mean'][row_index] = cv_results['test_f1_macro'].mean()
    clf_table['Classifier Test f1 Macro 3*STD'][row_index] = cv_results['test_f1_macro'].std() * 3	
    clf_table['Classifier Train Precision Mean'][row_index] = cv_results['train_precision'].mean()
    clf_table['Classifier Test Precision Mean'][row_index] = cv_results['test_precision'].mean()
    clf_table['Classifier Test Precision 3*STD'][row_index] = cv_results['test_precision'].std() * 3
    clf_table['Classifier Train Recall Mean'][row_index] = cv_results['train_recall'].mean()
    clf_table['Classifier Test Recall Mean'][row_index] = cv_results['test_recall'].mean()
    clf_table['Classifier Test Recall 3*STD'][row_index] = cv_results['test_recall'].std() * 3
    clf_table['Classifier Train Jaccard Mean'][row_index] = cv_results['train_jaccard'].mean()
    clf_table['Classifier Test Jaccard Mean'][row_index] = cv_results['test_jaccard'].mean()
    clf_table['Classifier Test Jaccard 3*STD'][row_index] = cv_results['test_jaccard'].std() * 3
    #clf_table['Classifier Train Roc Auc Mean'][row_index] = cv_results['train_roc_auc'].mean()
    #clf_table['Classifier Test Roc Auc Mean'][row_index] = cv_results['test_roc_auc'].mean()
    #clf_table['Classifier Test Roc Auc 3*STD'][row_index] = cv_results['test_roc_auc'].std() * 3
    #clf_table['Classifier Train Roc Auc Ovr Mean'][row_index] = cv_results['train_roc_auc_ovr'].mean()
    #clf_table['Classifier Test Roc Auc Ovr Mean'][row_index] = cv_results['test_roc_auc_ovr'].mean()
    #clf_table['Classifier Test Roc Auc Ovr 3*STD'][row_index] = cv_results['test_roc_auc_ovr'].std() * 3
    #clf_table['Classifier Train Roc Auc Ovo Mean'][row_index] = cv_results['train_roc_auc_ovo'].mean()
    #clf_table['Classifier Test Roc Auc Ovo Mean'][row_index] = cv_results['test_roc_auc_ovo'].mean()
    #clf_table['Classifier Test Roc Auc Ovo 3*STD'][row_index] = cv_results['test_roc_auc_ovo'].std() * 3  
    clf_table['Classifier Time'][row_index] = cv_results['fit_time'].mean()

    # Results Save
    clf_cv_table = np.sort(clf_table, order='Classifier Test Accuracy Mean')[::-1]
    H5FM.Data2H5(clf_cv_table, os.path.join(base_output_folder, 'MultiModelAnalysis'),
                'CLF Nested CV Table', groupName="ML Analysis", dtype=clf_table_dataType)
    df_clf_cv_table = pd.DataFrame(clf_cv_table)
    df_clf_cv_table.to_csv(os.path.join(output_folder,  modelname + "_clf_table.csv"), index=False)
    df_clf_cv_table.to_csv(os.path.join(output_folder,  "all_model_clf_table.csv"), index=False, float_format = '%.2f')
    df_clf_cv_table.drop(columns = ['Classifier Parameters']).to_csv(os.path.join(output_folder,  "all_model_clf_table_d.csv"), index=False, float_format = '%.2f')

# https://malishoaib.wordpress.com/2017/09/08/sample-size-estimation-for-machine-learning-models-using-hoeffdings-inequality/
# error = sqrt(-1*math.log(significance_level / 2)/n)
# significance_level: type 1 error for the confusion matrix
def Hoeffding_SampleSize(error, significance_level):
    n = (-1 // (2 * error ** 2)) * math.log(significance_level / 2)
    return n

def model_prediction(model_path, X):
    loaded_model = pickle.load(model_path + ".sav", 'rb')
    y_pred = loaded_model.predict(X)
    return y_pred

# Simple Model Creator for sklearn or xgboost
class SimpleMLHelper():
    def __init__(self, clf, **params):
        #params['random_state']= seed
        self.clf = clf(**params)
        self.ml_output_count = 0
        self.base_path = CU.updateOutputFolder(_output_main_folder + '\\', 'SimpleModel')
        self.output_path = self.__output_path__()

    def __output_path__(self):
        global _output_main_folder
        path = os.path.join(self.base_path,
                            self.clf.__class__.__name__+str(self.ml_output_count))
        CU.createOutputFolder(path)
        setting_files_list = glob.glob('*.txt')
        for copy_file_path in setting_files_list:
            shutil.copy(copy_file_path, path)
        self.ml_output_count += 1
        return path

    def train(self, x_train, y_train):
        obj = self.clf.fit(x_train, y_train)
        self.__model_save__(obj)

    def predict(self, x):
        return self.clf.predict(x)

    def fit(self, x, y):
        obj = self.clf.fit(x, y)
        self.__model_save__(obj)

    def __model_save__(self, est_clf):
        modelname = self.clf.__class__.__name__
        pickle.dump(est_clf, open(os.path.join(self.output_path,  "{}_best.sav".format(modelname)),'wb'))

    def feature_importances(self, x, y):
        print(self.clf.fit(x, y).feature_importances_)

# Multi-Model Selection for sklearn or xgboost
class MultiModelMLHelper():
    def __init__(self, clfs, params):
        self.clfs = clfs #included pipeline estimator
        self.grid_params = params
        self.ml_output_count = 0
        self.base_path = CU.updateOutputFolder(_output_main_folder + '\\', 'MultiModels')
        self.output_paths = self.__output_paths__()
        self.global_test_score = 0

    def __output_paths__(self):
        global _output_main_folder
        paths = {}
        
        for clf in self.clfs:
            modelname = clf.__class__.__name__
            paths[modelname] = os.path.join(self.base_path,
                                             modelname + str(self.ml_output_count))
            CU.createOutputFolder(paths[modelname])
        
        setting_files_list = glob.glob('*.txt')
        for copy_file_path in setting_files_list:
            shutil.copy(copy_file_path, self.base_path)
        self.ml_output_count += 1
        return paths

    def model_evaluation(self, x_val, y_val, output_path, filename, conf_matrix_params):
        try:
            self.__get_roc_curve_plot__(x_val, y_val, output_path, filename)
        except:
            pass
        try:
            self.__get_confusion_matrix__(x_val, y_val, output_path, filename, conf_matrix_params)
        except:
            pass
        try:
            self.__get_permulation_importance__(x_val, y_val, output_path, filename)
        except:
            pass
        try:
            self.__get_features_importance__(x_val, output_path, filename)
        except:
            pass
        try:
            self.__get_shap_analysis__(x_val, output_path, filename)
        except:
            pass

    def __get_shap_analysis__(self, df_val_X, output_path, filename):
        loaded_model = pickle.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
        try:
            ME.df_data_ensemble_tree_shap_force_each_plot(loaded_model.best_estimator_, df_val_X, 2, output_path, filename)
        except:
            pass
        try:
            ME.df_data_ensemble_tree_shap_force_summary_plot(loaded_model.best_estimator_, df_val_X, 2, output_path, filename)
        except:
            pass

    def __get_features_importance__(self, df_val_X, output_path, filename):
        plt.figure()
        modelObj, importances, indices = get_tree_based_feature_importance(df_val_X, output_path, filename)
        modelObjGroup = (modelObj, importances, indices)
        ME.df_data_tree_based_feature_importance_plot(modelObjGroup, df_val_X) #, output_path, filename)
        print(os.path.join(output_path, filename + '_feature_importance.svg'))
        plt.savefig(os.path.join(output_path, filename + '_feature_importance.svg'), bbox_inches='tight')
        plt.close('all')

    def __get_permulation_importance__(self, df_val_X, df_val_y, output_path, filename):
        loaded_model = pickle.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
        ME.df_data_permulation_importance(loaded_model.best_estimator_, df_val_X, df_val_y, output_path, filename, random_state=1)

    def __get_roc_curve_plot__(self, x_val, y_val, output_path, filename):
        # Plot all ROC curves
        plt.figure()
        fpr, tpr, roc_auc = cal_roc_auc_curve(x_val, y_val, output_path, filename)
        ME.binary_clf_roc_curve_plot(fpr, tpr, roc_auc)
        plt.savefig(os.path.join(output_path, filename + '_roc.svg'))
        plt.close('all')

    def __get_confusion_matrix__(self, x_val, y_val, output_path, filename, conf_matrix_params):
        df_cm, df_clf_report = cal_confusion_matrix(x_val, y_val, output_path, filename, conf_matrix_params)
        self.__confusion_matrix_save__(df_cm, df_clf_report, filename)
        del loaded_model, df_cm, df_clf_report

    def __confusion_matrix_save__(self, df_cm, df_clf_report, filename):
        H5FM.Data2H5(np.array(df_cm), os.path.join(self.base_path, 'MultiModelAnalysis'),
                     filename + '_cm', groupName="ML Analysis")
        H5FM.Data2H5(np.array(df_clf_report), os.path.join(self.base_path, 'MultiModelAnalysis'),
                     filename + '_clf_report', groupName="ML Analysis")

    def __model_save__(self, clf, est_clf, x_val, y_val, conf_matrix_params):
        modelname = clf.__class__.__name__
        filename = "{}_best".format(modelname)
        op = self.output_paths[modelname]
        pickle.dump(est_clf, open(os.path.join(op, filename + '.sav'), 'wb'))
        self.model_evaluation(x_val, y_val, op, filename, conf_matrix_params)

    def __cv_model_save__(self, clf, cv_results, x_val, y_val, conf_matrix_params):
        modelname = clf.__class__.__name__
        output_folder = self.output_paths[modelname]

        try:
            best_idx = list(cv_results['test_accuracy']).index(max(cv_results['test_accuracy']))
            best_clf = cv_results['estimator'][best_idx]
            pickle.dump(best_clf, open(os.path.join(output_folder, '{}_cv_{}_best_clf.sav'.format(modelname, best_idx)), 'wb'))
            if (max(cv_results['test_accuracy']) > self.global_test_score):
                pickle.dump(best_clf, open(os.path.join(self.base_path, 'best_clf.sav'), 'wb'))
                print('best_model: {} cv_{} in {}'.format(modelname, best_idx, os.path.join(self.base_path, 'best_clf.sav')))
        except:
            pass

        for idx, est_clf in enumerate(cv_results['estimator']):
            filename = "{}_cv{}".format(modelname, idx)
            pickle.dump(est_clf, open(os.path.join(output_folder, filename+'.sav'), 'wb'))
            self.model_evaluation(x_val, y_val, output_folder, filename, conf_matrix_params)

    def __results_record__(self, clf, clf_table_group, est_clf):
        # Results Record
        modelname = clf.__class__.__name__
        output_folder = self.output_paths[modelname]
        row_index = self.clfs.index(clf)
        grid_results_table_record(modelname, clf_table_group, est_clf, row_index, self.base_path, output_folder)

        # save the cv results
        pickle.dump(est_clf.cv_results_, open(os.path.join(output_folder, '{}_cv_results.sav'.format(modelname)), 'wb'))
        csv_cv_results = pd.DataFrame.from_dict(est_clf.cv_results_)
        csv_cv_results.to_csv(os.path.join(output_folder, '{}_cv_results.csv'.format(modelname)), index=False)

    def __cv_results_record__(self, clf, clf_table_group, cv_results):
        # save clf table
        modelname = clf.__class__.__name__
        row_index = self.clfs.index(clf)
        output_folder = self.output_paths[modelname]
        cv_clf_table_record(modelname, clf_table_group, cv_results, row_index, self.base_path, output_folder)

        # save the cv results
        pickle.dump(cv_results, open(os.path.join(output_folder, '{}_cv_results.sav'.format(modelname)), 'wb'))
        csv_cv_results = pd.DataFrame.from_dict(cv_results)
        csv_cv_results.to_csv(os.path.join(output_folder, '{}_cv_results.csv'.format(modelname)), index=False)

    # Data Preparation
    def data_preparation(self, raw_x, raw_y, tail_name_to_train, fit_model_file_path, FeatureEngineeringFitObj, LabelEncoderFitObj = None, train_drop_column = None):
        for clf in self.clfs:
            modelname = clf.__class__.__name__
            op = self.output_paths[modelname]
            
            x_train, x_val, y_train, y_val = SM.simple_train_test_data_split(raw_x, raw_y)
            pd.DataFrame(x_train).to_csv(os.path.join(op, 'x_raw_train.csv'), index=False)
            pd.DataFrame(x_val).to_csv(os.path.join(op, 'x_raw_val.csv'), index=False)
            pd.DataFrame(y_train).to_csv(os.path.join(op, 'y_raw_train.csv'), index=False)
            pd.DataFrame(y_val).to_csv(os.path.join(op, 'y_raw_val.csv'), index=False)

            pd.DataFrame(y_train).to_csv(os.path.join(op, 'y_train.csv'), index=False)
            pd.DataFrame(y_val).to_csv(os.path.join(op, 'y_val.csv'), index=False)
            
            if (LabelEncoderFitObj):
                LabelEncoderFitObj('y_train', op, fit_model_file_path)
                LabelEncoderFitObj('y_val', op, fit_model_file_path)

            train = pd.concat([x_train, y_train], axis=1)
            train.to_csv(os.path.join(op, "train.csv"), index=False)
            
            val=pd.concat([x_val, y_val], axis=1)
            val.to_csv(os.path.join(op, "val.csv"), index=False)
            
            FeatureEngineeringFitObj("train", op, fit_model_file_path)
            FeatureEngineeringFitObj("val", op, fit_model_file_path)

            train_data = read_data(os.path.join(op, "train"+tail_name_to_train))
            if (train_drop_column):
                train_data = train_data.drop(columns= train_drop_column, axis=1)
            train_data.to_csv(os.path.join(op, 'x_train.csv'), index=False)
            cv_test_data = read_data(os.path.join(op, "val"+tail_name_to_train))
            if (train_drop_column):
                cv_test_data = cv_test_data.drop(columns= train_drop_column, axis=1)
            cv_test_data.to_csv(os.path.join(op, 'x_val.csv'), index=False)

    # Non_nested parameter search and scoring
    def train(self, fold_obj, conf_matrix_params):
        clf_table_group = grid_results_record_init(len(self.clfs)) #self.__results_record_init__()

        for clf in self.clfs:
            modelname = clf.__class__.__name__
            param = self.grid_params[modelname]
            op = self.output_paths[modelname]
			
            x_train, y_train = read_data(os.path.join(op, 'x_train')), read_data(os.path.join(op, 'y_train'))
            x_val, y_val = read_data(os.path.join(op, 'x_val')), read_data(os.path.join(op, 'y_val'))
            #x_train, x_val, y_train, y_val = SM.simple_train_test_data_split(x, y)
            #pd.DataFrame(x_train).to_csv(os.path.join(op, 'x_train.csv'), index=False)
            #pd.DataFrame(x_val).to_csv(os.path.join(op, 'x_val.csv'), index=False)
            #pd.DataFrame(y_train).to_csv(os.path.join(op, 'y_train.csv'), index=False)
            #pd.DataFrame(y_val).to_csv(os.path.join(op, 'y_val.csv'), index=False)

            grid_search = GridSearchCV(estimator=clf, cv=fold_obj, param_grid=param, refit=True)
            est_clf = grid_search.fit(x_train, y_train)
            self.__model_save__(clf, est_clf, x_val, y_val, conf_matrix_params)
            self.__results_record__(clf, clf_table_group, est_clf)
            del grid_search, est_clf, x_train, x_val, y_train, y_val

    # Nested Cross Validation
    def nested_train(self, inner_fold_obj, outer_fold_obj, conf_matrix_params):
        clf_table_group = cv_results_record_init(len(self.clfs))

        for clf in self.clfs:
            modelname = clf.__class__.__name__
            param = self.grid_params[modelname]
            op = self.output_paths[modelname]

            x_cv, y_cv = read_data(os.path.join(op, 'x_train')), read_data(os.path.join(op, 'y_train'))
            x_val, y_val = read_data(os.path.join(op, 'x_val')), read_data(os.path.join(op, 'y_val'))
            #x_cv, x_val, y_cv, y_val = SM.simple_train_test_data_split(x, y)
            #pd.DataFrame(x_cv).to_csv(os.path.join(op, 'x_cv.csv'), index=False)
            #pd.DataFrame(x_val).to_csv(os.path.join(op, 'x_val.csv'), index=False)
            #pd.DataFrame(y_cv).to_csv(os.path.join(op, 'y_cv.csv'), index=False)
            #pd.DataFrame(y_val).to_csv(os.path.join(op, 'y_val.csv'), index=False)
  
            best_search, cv_results = self.__nested_train__(clf, x_cv, y_cv, param, inner_fold_obj, outer_fold_obj)
            self.__cv_results_record__(clf, clf_table_group, cv_results)
            self.__cv_model_save__(clf, cv_results, x_val, y_val, conf_matrix_params)
            self.__cv_plot__(clf, cv_results['train_accuracy'], cv_results['test_accuracy'])
            del best_search, cv_results, x_cv, x_val, y_cv, y_val

    def __nested_train__(self, clf, x_cv, y_cv, param, inner_fold_obj, outer_fold_obj):
        """
        Nested Cross Validation
        Pass the gridSearch estimator to cross_val_score
        This will be your required 10 x 5 cvs
        10 for outer cv and 5 for gridSearch's internal CV
        :param clf:
        :param x_cv:
        :param y_cv:
        :param kwargs:
        best_search = GridSearchCV(estimator=clf, param_grid=param, cv=self.inner_fold_val)
        cross_validate(clf, x_train, y_train, cv=out_fold_val, return_train_score=True, return_estimator=True)
        :return:
        """
        modelname = clf.__class__.__name__
        print("training start: {}".format(modelname))
        best_search = GridSearchCV(estimator=clf,
                                   param_grid=param,
                                   refit=True,
                                   cv=inner_fold_obj)
        cv_results = cross_validate(best_search,
                                    x_cv,
                                    y_cv,
                                    cv=outer_fold_obj,
                                    scoring=('accuracy',
                                             'balanced_accuracy',
                                             'average_precision',
                                             'neg_brier_score',
                                             'f1',
                                             'f1_micro',
                                             'f1_macro',
                                             'f1_weighted',
                                             #'f1_samples',
                                             'precision',
                                             'recall',
                                             'jaccard'
#                                             'roc_auc',
#                                             'roc_auc_ovr',
#                                             'roc_auc_ovo',
#                                             'roc_auc_ovr_weighted',
#                                             'roc_auc_ovo_weighted' 
											 ),
                                    return_train_score=True,
                                    return_estimator=True)
        print("training end: {}".format(modelname))
        return best_search, cv_results

    def __cv_plot__(self, clf, train_scores, test_scores):
        modelname = clf.__class__.__name__
        op = self.output_paths[modelname]

        plt.figure()
        cv_plot(train_scores, test_scores, plot_setting = None)
        plt.savefig(os.path.join(op, modelname + "_cvCurve.svg"))
        plt.close()

    def learning_curve_plot(self, clf, x_cv, y_cv, fold_obj):
        modelname = clf.__class__.__name__
        op = self.output_paths[modelname]

        train_sizes, train_scores, valid_scores = learning_curve(clf, x_cv, y_cv, cv=fold_obj)
        #print(np.std(train_scores))
        train_scores_mean = np.mean(train_scores, axis=1)
        train_scores_std = np.std(train_scores, axis=1)
        test_scores_mean = np.mean(valid_scores, axis=1)
        test_scores_std = np.std(valid_scores, axis=1)

        plt.figure()
        # box-like grid
        plt.grid()

        # plot the std deviation as a transparent range at each training set size
        plt.fill_between(train_sizes, train_scores_mean - train_scores_std, train_scores_mean + train_scores_std,
                     alpha=0.1, color="r")
        plt.fill_between(train_sizes, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std,
                     alpha=0.1, color="g")

        # plot the average training and test score lines at each training set size
        plt.plot(train_sizes, train_scores_mean, 'o-', color="r", label="Training score")
        plt.plot(train_sizes, test_scores_mean, 'o-', color="g", label="Cross-validation score")

        plt.title(modelname)
        plt.legend(loc="best", labels=['Training score', 'Cross-validation score'])
        plt.xlabel("Training Samples.")
        plt.ylabel("Score")
        plt.gca().invert_yaxis()

        # sizes the window for readability and displays the plot
        # shows error from 0 to 1.1
        plt.ylim(-.1, 1.1)
        plt.savefig(os.path.join(op,modelname + "_learningCurve.svg"))
        plt.close()
