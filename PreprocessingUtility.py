'''
@Author: CY Peng
@Date: 2019-11-13
@LastEditTime: 2019-11-13

DataVer  - Author - Note
2019/11/13     CY   Initialization
2019/11/20     CY   Impution
2019/11/22     CY   Missing Data Processing: Sklearn One Hot Encoder Matrix to Nan Matrix

Note
Ordinal encoders (cannot be used on nominal data):
Ordinal
Binary

Categorical encoders (works on all type of categorical variables):
One-Hot (or Dummy)
Simple
Sum (Deviation)
Backward Difference

Categorical encodes which requires special attention when used on nominal data:
Helmert
Orthogonal Polynomial

Reference
https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.FeatureHasher.html, Feature Hashing
https://scikit-learn.org/stable/modules/classes.html#module-sklearn.preprocessing, Preprocessing API
https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.FeatureHasher.html, Feature Hashing
https://www.datacamp.com/community/tutorials/categorical-data, categorical data processing
https://towardsdatascience.com/6-different-ways-to-compensate-for-missing-values-data-imputation-with-examples-6022d9ca0779, Imputation
https://towardsdatascience.com/the-use-of-knn-for-missing-values-cf33d935c637, knn imputation
https://towardsdatascience.com/preprocessing-encode-and-knn-impute-all-categorical-features-fast-b05f50b4dfaa, encoder and imputation
https://impyute.readthedocs.io/en/master/api/cross_sectional_imputation.html, imputation module
https://blog.myyellowroad.com/using-categorical-data-in-machine-learning-with-python-from-dummy-variables-to-deep-category-66041f734512, Categorical Data Processing
https://autoimpute.readthedocs.io/en/latest/, autoimputation
https://thelonenutblog.wordpress.com/2017/03/16/categorical-variables-encoding/, categorical data encoding
'''
import os
import pickle
import sys
import numpy as np
from sklearn.preprocessing import OneHotEncoder, OrdinalEncoder, Normalizer, StandardScaler, MinMaxScaler, RobustScaler
from sklearn.feature_extraction import FeatureHasher
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import SimpleImputer, IterativeImputer
from impyute.imputation.cs import fast_knn, mice
from sklearn_pandas import CategoricalImputer
#import Resource
#from autoimpute.imputations.mis_classifier import MissingnessClassifier
#from autoimpute.imputations import SingleImputer, MultipleImputer
#from sklearn.externals import joblib

def cat_feature_hasher(df_data, outputFolder, filename, **kewgs):
    fh = FeatureHasher(**kewgs)
    fhObj = fh.fit(df_data)
    pickle.dump(fhObj, open(os.path.join(outputFolder, filename + '_fh.sav'), 'wb'))
    return fhObj

def cat_one_hot_encoder(df_data, outputFolder, filename,**kewgs):
    enc = OneHotEncoder(**kewgs)
    oheObj = enc.fit(df_data)
    pickle.dump(oheObj, open(os.path.join(outputFolder, filename + '_ohe.sav'), 'wb'))
    return oheObj

def cat_ord_encoder(df_data, outputFolder, filename,**kewgs):
    enc = OrdinalEncoder(**kewgs)
    ordObj = enc.fit(df_data)
    pickle.dump(ordObj, open(os.path.join(outputFolder, filename + '_ord.sav'), 'wb'))
    return ordObj

def num_normalizer(df_data, outputFolder, filename, **kewgs):
    # Normalize samples individually to unit norm.
    # Each sample (i.e. each row of the data matrix) with at least one non zero component is rescaled independently of other samples so that its norm (l1 or l2) equals one.
    # ‘l2’ by default
    norObj = Normalizer(**kewgs).fit(df_data)
    pickle.dump(norObj, open(os.path.join(outputFolder, filename + '_nor.sav'), 'wb'))
    return norObj

def num_standardization(df_data, outputFolder, filename, **kewgs):
    # Standardize features by removing the mean and scaling to unit variance
    scalerObj = StandardScaler(**kewgs).fit(df_data)
    pickle.dump(scalerObj, open(os.path.join(outputFolder, filename + '_std.sav'), 'wb'))
    return scalerObj

def num_rubust(df_data, outputFolder, filename, **kewgs):
    # Standardize features by removing the mean and scaling to unit variance
    rubustObj = RobustScaler(**kewgs).fit(df_data)
    pickle.dump(rubustObj, open(os.path.join(outputFolder, filename + '_rub.sav'), 'wb'))
    return rubustObj

def num_minmax(df_data, outputFolder, filename, **kewgs):
    # This estimator scales and translates each feature individually such that it is in the given range on the training set, e.g. between zero and one.
    minmaxObj = MinMaxScaler(**kewgs).fit(df_data)
    pickle.dump(minmaxObj, open(os.path.join(outputFolder, filename + '_minmax.sav'), 'wb'))
    return minmaxObj

def num_simple_imputer(df_data, outputFolder, filename, missing_values = np.nan, strategy='most_frequent', **kewgs):
    # strategy: Mean/Median
    # Pros: Easy and fast, Works well with small numerical datasets
    # Cons:
    # Doesn’t factor the correlations between features. It only works on the column level.
    # Will give poor results on encoded categorical features (do NOT use it on categorical features).
    # Not very accurate.
    # Doesn’t account for the uncertainty in the imputations.
    # strategy: Most Frequent
    # Pros: Works well with categorical features.
    # Cons:
    # It also doesn’t factor the correlations between features.
    # It can introduce bias in the data.
    impObj = SimpleImputer(missing_values=missing_values, strategy=strategy, **kewgs)  # for median imputation replace 'mean' with 'median'
    impObj.fit(df_data)
    pickle.dump(impObj, open(os.path.join(outputFolder, filename + '_imp.sav'), 'wb'))
    # imputed_train_df = imp_mean.transform(train)
    return impObj

def num_fastKNN(df_data, rec_limit_os, outputFolder, filename, k= 30, **kewgs):
    # Pros:
    # Can be much more accurate than the mean, median or most frequent imputation methods (It depends on the dataset).
    # Cons:
    # Computationally expensive. KNN works by storing the whole training dataset in memory.
    # K-NN is quite sensitive to outliers in the data (unlike SVM)
    #Resource.setrlimit(Resource.RLIMIT_STACK, [0x10000000, Resource.RLIM_INFINITY])
    sys.setrecursionlimit(rec_limit_os)  # Increase the recursion limit of the OS
    impObj = fast_knn(df_data, k=k, **kewgs)
    pickle.dump(impObj, open(os.path.join(outputFolder, filename + '_imp_knn.sav'), 'wb'))
    return impObj

def num_mice_imputer(df_data, outputFolder, filename):
    impObj = mice(df_data)
    pickle.dump(impObj, open(os.path.join(outputFolder, filename + '_imp_mice.sav'), 'wb'))
    return impObj

def cat_simple_imputer(df_data, outputFolder, filename, missing_values = np.nan, strategy='most_frequent', **kewgs):
    impObj = CategoricalImputer(missing_values=missing_values, strategy=strategy, **kewgs)
    impObj.fit(df_data)
    pickle.dump(impObj, open(os.path.join(outputFolder, filename + '_imp_cat.sav'), 'wb'))
    return impObj

def num_clf_imputer(df_data, outputFolder, filename, clf = None, pred = 'all'):
    clfImpObj = MissingnessClassifier(classifier = clf, predictors = pred)
    clfImpObj.fit(df_data)
    pickle.dump(clfImpObj, open(os.path.join(outputFolder, filename + '_imp_clf.sav'), 'wb'))
    return clfImpObj

def num_multiple_imputer(df_data, outputFolder, filename, **kewgs):
    multipleImp = MultipleImputer(**kewgs)
    multipleImp.fit(df_data)
    pickle.dump(multipleImp, open(os.path.join(outputFolder, filename + '_imp_multiple.sav'), 'wb'))
    return multipleImp

def num_iterative_imputer(df_data, outputFolder, filename, **kewgs):
    impObj = IterativeImputer(**kewgs)
    impObj.fit(df_data)
    pickle.dump(impObj, open(os.path.join(outputFolder, filename + '_imp_multiple.sav'), 'wb'))
    return impObj

def convert_missing_ohe_to_nan(ohe_cat_data, ohe_columns, missing_name = 'unknown'):
    var, total_var = 0, 0
    str_var = "x{}".format(var)
    var_dict = {}
    var_dict[str_var] = []
    for cat in ohe_columns: #oheObj.get_feature_names():
        if str_var in cat:
            var_dict[str_var].append(cat)
        else:
            var += 1
            total_var = var
            str_var = "x{}".format(var)
            var_dict[str_var] = []
            var_dict[str_var].append(cat)

    for var in range(total_var+1):
        str_var = "x{}".format(var)
        for cat in var_dict[str_var]:
            if missing_name in cat:
                unknown_list = ohe_cat_data[ohe_cat_data[cat] == 1].index.tolist()
                for idx in unknown_list:
                    for c_cat in var_dict[str_var]:
                        # ohe_cat_data.at[idx, c_cat] = -1
                        ohe_cat_data.loc[idx, c_cat] = np.nan
                ohe_cat_data = ohe_cat_data.drop(cat, axis=1)
    return ohe_cat_data

def df_data_fill_with_most_freq(df_data, type_list):
    for cat in type_list:
        freq_port = df_data.loc[:, cat].dropna().mode()[0]
        df_data[cat] = df_data.loc[:, cat].fillna(freq_port)
    return df_data

def df_data_fill_with_random(df_data, type_list):
    '''Fill `df2`'s column with name `column` with random data based on non-NaN data from `column`'''
    df = df_data.copy()
    for cat in type_list:
        df[cat] = df[cat].apply(lambda x: np.random.choice(df[cat].dropna().values) if np.isnan(x) else x)
    df_data = df
    return df_data