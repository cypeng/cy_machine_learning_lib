'''
@Author: CY Peng
@Date: 2019-11-6
@LastEditTime: 2019-11-11

DataVer  - Author - Note
2019/11/6     CY    Initialization
2019/11/11    CY    Data Format Update
2020/01/07    CY    Samping Time Series

Ref.
https://medium.com/analytics-vidhya/balance-your-data-using-smote-98e4d79fcddb, Smote Algorithm for Imbalance Data
https://imbalanced-learn.org/en/stable/index.html, Sampling Method for Imbalance Data
https://www.mikulskibartosz.name/nested-cross-validation-in-time-series-forecasting-using-scikit-learn-and-statsmodels/, Sampling for Time Series & Cross Validation
https://www.angioi.com/time-nested-cv-with-sklearn/, Sampling for Time Series & Cross Validation
'''
import sklearn.model_selection as ms
import imblearn as imb
import pandas as pd

# Time Series Split
def timeseries_sampling_split_obj(n_splits = 5, max_train_size = None):
    """
    :param n_splits, ex: 5
    :param max_train_size, ex: None, 0.77 
    :return:
    tscv_fold_Obj
    for train_index, test_index in tscv_fold_Obj.split(X):
        #print("TRAIN:", train_index, "TEST:", test_index)
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
    """
    tscv_fold_Obj = ms.TimeSeriesSplit(n_splits = n_splits, max_train_size = max_train_size)
    return tscv_fold_Obj

def two_stages_timeseries_sampling_split_obj(outer_n_splits = 10, outer_random_state = 0, inner_n_splits = 5, inner_random_state = 0):
    outer_fold_obj = ms.TimeSeriesSplit(n_splits= outer_n_splits)
    inner_fold_obj = ms.TimeSeriesSplit(n_splits= inner_n_splits)
    return outer_fold_obj, inner_fold_obj
    
# array2dataframe
def resampling_array2dataframe(X_resampled, y_resampled, df_X, df_y):
    df_X_resample = pd.DataFrame(X_resampled, columns=df_X.columns)
    df_y_resample = pd.DataFrame(y_resampled, columns=df_y.columns)
    return df_X_resample, df_y_resample

# Sklearn General Sampling
def simple_train_test_data_split(df_X, df_y, training_size = 0.77):
    """
    :param training_size: 0.77
    :return:
    df_X_train, df_X_val, df_y_train, df_y_val
    """
    X_train, X_val, y_train, y_val = ms.train_test_split(df_X, df_y, test_size=1 - training_size)
    df_X_train, df_y_train = resampling_array2dataframe(X_train, y_train, df_X, df_y)
    df_X_val, df_y_val = resampling_array2dataframe(X_val, y_val, df_X, df_y)

    return df_X_train, df_X_val, df_y_train, df_y_val

def simple_train_test_val_split(df_X, df_y, cv_size=0.8, training_size=0.77):
    X_cv, X_val, y_cv, y_val = ms.train_test_split(df_X, df_y, test_size=1 - cv_size)
    X_train, X_test, y_train, y_test = ms.train_test_split(X_cv, y_cv, test_size=1 - training_size)
    df_X_train, df_y_train = resampling_array2dataframe(X_train, y_train, df_X, df_y)
    df_X_test, df_y_test = resampling_array2dataframe(X_test, y_test, df_X, df_y)
    df_X_val, df_y_val = resampling_array2dataframe(X_val, y_val, df_X, df_y)
    return df_X_train, df_X_test, df_X_val, y_train, df_y_test, df_y_val

def two_stages_sampling_stratified_shuffle_split_obj(outer_n_splits = 10, outer_random_state = 0, inner_n_splits = 5, inner_random_state = 0):
    outer_fold_obj = ms.StratifiedShuffleSplit(n_splits= outer_n_splits, random_state= outer_random_state)
    inner_fold_obj = ms.StratifiedShuffleSplit(n_splits= inner_n_splits, random_state= inner_random_state)
    return outer_fold_obj, inner_fold_obj

# Imbalance Over-Sampling
def imb_data_RANDOMsampling(df_X, df_y, random_state = 0):
    """
    :param df_X:
    :param df_y:
    :param random_state:
    :return:
    df_X_resampled, df_y_resampled
    """
    ros = imb.over_sampling.RandomOverSampler(random_state= random_state)
    X_resampled, y_resampled = ros.fit_resample(df_X, df_y)
    return resampling_array2dataframe(X_resampled, y_resampled, df_X, df_y)

def imb_data_SMOTEsampling(df_X, df_y, method = 'general', **kwargs):
    """
    :param df_X:
    :param df_y:
    :param method:
    :param kwargs:
     imb.over_sampling.SMOTENC(categorical_features=[0, 2], random_state=0)
    :return:
    df_X_resampled, df_resampled
    """
    if ('general' == method):
        X_resampled, y_resampled = imb.over_sampling.SMOTE().fit_resample(df_X, df_y)
        return resampling_array2dataframe(X_resampled, y_resampled, df_X, df_y)
    elif ('Borderline' == method):
        X_resampled, y_resampled = imb.over_sampling.BorderlineSMOTE.fit_resample(df_X, df_y)
        return resampling_array2dataframe(X_resampled, y_resampled, df_X, df_y)
    elif ('nc' == method):
        smote_nc = imb.over_sampling.SMOTENC(**kwargs)
        X_resampled, y_resampled = smote_nc.fit_resample(df_X, df_y)
        return resampling_array2dataframe(X_resampled, y_resampled, df_X, df_y)

def imb_data_ADASYNsampling(df_X, df_y):
    """

    :param df_X:
    :param df_y:
    :return:
    df_X_resampled, df_y_resampled
    """
    X_resampled, y_resampled = imb.over_sampling.SMOTE().ADASYN().fit_resample(df_X, df_y)
    return resampling_array2dataframe(X_resampled, y_resampled, df_X, df_y)

# Imbalance Under-Sampling
def imb_data_ClusterCentroids(df_X, df_y, random_state = 0):
    cc = imb.under_sampling.ClusterCentroids(random_state=random_state)
    X_resampled, y_resampled = cc.fit_resample(df_X, df_y)
    return resampling_array2dataframe(X_resampled, y_resampled, df_X, df_y)

def imb_data_RandomUnderSampler(df_X, df_y, random_state = 0):
    rus  = imb.under_sampling.RandomUnderSampler(random_state=random_state)
    X_resampled, y_resampled = rus.fit_resample(df_X, df_y)
    return resampling_array2dataframe(X_resampled, y_resampled, df_X, df_y)

def imb_data_NearMiss(df_X, df_y, version = 1):
    nm1 = imb.under_sampling.NearMiss(version= version)
    X_resampled, y_resampled = nm1.fit_resample(df_X, df_y)
    return resampling_array2dataframe(X_resampled, y_resampled, df_X, df_y)

def imb_data_NearestNeighbours(df_X, df_y, type = 'All', random_state = 0):
    if ('All' == type):
        allknn = imb.under_sampling.AllKNN()
        X_resampled, y_resampled = allknn.fit_resample(df_X, df_y)
        return resampling_array2dataframe(X_resampled, y_resampled, df_X, df_y)
    elif ('Edited' == type):
        enn = imb.under_sampling.EditedNearestNeighbours()
        X_resampled, y_resampled = enn.fit_resample(df_X, df_y)
        return resampling_array2dataframe(X_resampled, y_resampled, df_X, df_y)
    elif ('RepeatedEdited' == type):
        renn = imb.under_sampling.RepeatedEditedNearestNeighbours()
        X_resampled, y_resampled = renn.fit_resample(df_X, df_y)
        return resampling_array2dataframe(X_resampled, y_resampled, df_X, df_y)
    elif ( 'Condensed'== type):
        cnn = imb.under_sampling.CondensedNearestNeighbour(random_state=random_state)
        X_resampled, y_resampled = cnn.fit_resample(df_X, df_y)
        return resampling_array2dataframe(X_resampled, y_resampled, df_X, df_y)

def imb_data_OneSidedSelection(df_X, df_y, random_state=0):
    oss = imb.under_sampling.OneSidedSelection(random_state=random_state)
    X_resampled, y_resampled = oss.fit_resample(df_X, df_y)
    return resampling_array2dataframe(X_resampled, y_resampled, df_X, df_y)

def imb_data_NeighbourhoodCleaningRule(df_X, df_y):
    ncr = imb.under_sampling.NeighbourhoodCleaningRule()
    X_resampled, y_resampled = ncr.fit_resample(df_X, df_y)
    return resampling_array2dataframe(X_resampled, y_resampled, df_X, df_y)

def imb_data_InstanceHardnessThreshold(df_X, df_y, random_state=0, **kwargs):
    """

    :param df_X:
    :param df_y:
    :param kwargs:
    imb.under_sampling.InstanceHardnessThreshold(random_state=0, estimator = LogisticRegression(solver = 'lbfgs', multi_class = 'auto'))
    :return:
    """
    iht = imb.under_sampling.InstanceHardnessThreshold(random_state=random_state, **kwargs)
    X_resampled, y_resampled = iht.fit_resample(df_X, df_y)
    return resampling_array2dataframe(X_resampled, y_resampled, df_X, df_y)

# Imbalance Combine Sampling
def imb_data_CSMOTE(df_X, df_y, random_state=0, type='enn'):
    if ('enn' == type):
        smote_enn = imb.combine.SMOTEENN(random_state=random_state)
        X_resampled, y_resampled = smote_enn.fit_resample(df_X, df_y)
        return resampling_array2dataframe(X_resampled, y_resampled, df_X, df_y)
    elif ('omek' == type):
        smote_tomek = imb.combine.SMOTETomek(random_state=random_state)
        X_resampled, y_resampled = smote_tomek.fit_resample(df_X, df_y)
        return resampling_array2dataframe(X_resampled, y_resampled, df_X, df_y)