'''
@Author: CY Peng
@Date: 2020-01-07
@LastEditTime: 2020-01-07

DataVer  - Author - Note
2019/02/17 - C.Y. - First Release, Using FB API - fbprophet to predict the fund NAV
2019/02/21 - C.Y. - Setting: Output Folder and ML Prediction
2020/01/07 - C.Y. - Initialization
2020/01/14 - C.Y. - Evaluation

Reference
https://otexts.com/fpp2/, Forecasting: Principles and Practice Rob J Hyndman and George Athanasopoulos
https://pythondata.com/stock-market-forecasting-with-prophet/, stock prediction
https://research.fb.com/prophet-forecasting-at-scale/, prophet document
https://github.com/facebook/prophet/issues/671, Grid Search for Propohet
https://medium.com/acing-ai/how-to-evaluate-regression-models-d183b4f5853d, evaluation for time series
https://www.ritchieng.com/machine-learning-evaluate-linear-regression-model/, regression model evaluation
https://www.codespeedy.com/model-evaluation-metrics-in-regression-models-with-python/, regression model evaluation
https://hub.packtpub.com/cross-validation-strategies-for-time-series-forecasting-tutorial/, time series cross validation
https://towardsdatascience.com/analysis-of-stock-market-cycles-with-fbprophet-package-in-python-7c36db32ecd0, prophet prediction
https://github.com/facebook/prophet/tree/master/notebooks, fbprophet Example
https://zhuanlan.zhihu.com/p/37663120, Regression Evaluation
https://www.dezyre.com/recipes/use-regression-metrics-in-python, regression metrics 
https://robjhyndman.com/hyndsight/tscv/, time series corss validate error calculation
'''
import CommonUtility as CU
import pickle
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import h5py
import H5FileManager as H5FM
import math
import glob
import shutil
from datetime import datetime

# Custom Lib
import SamplingLib as SM
import ModelEvaluation as ME
import ExploratoryDataAnalysis as EDA

# Model Import
from fbprophet import Prophet
from fbprophet.diagnostics import cross_validation, performance_metrics #, plot_cross_validation_metric
from fbprophet.plot import plot_cross_validation_metric

# Cross Validation for Multi Classifier
from sklearn.model_selection import ParameterGrid, cross_validate, cross_val_score, learning_curve, GridSearchCV, train_test_split, validation_curve
from sklearn.multiclass import OneVsRestClassifier

_output_main_folder = 'ML_output'
CU.createOutputFolder(_output_main_folder)

def read_data(filename):
    data = pd.read_csv(filename+".csv")
    #data = data.fillna(value="unknown")
    return data

def df_data2prophet_data(df_train, df_val, ds_column, y_column):
    max_train_val = max(df_train[y_column])
    min_train_val = min(df_train[y_column])
    df_train_type = {'ds': sum(df_train[ds_column].values.tolist(),[]),
                     'y': df_train[y_column].values.tolist(),
                     'cap': max_train_val,
                     'floor': min_train_val}
    #print(df_train_type)
    df_train = pd.DataFrame(data = df_train_type) #, index = False

    df_val_type = {'ds': sum(df_val[ds_column].values.tolist(),[]),
                   'y': df_val[y_column].values.tolist(),
                   'cap': max_train_val,
                   'floor': min_train_val}
    #print(df_train_type)
    df_val = pd.DataFrame(data = df_val_type) #, index = False

    return df_train, df_val

def convert_prophet_timestamp(stamp):
    try:
        fmt = '%Y-%m-%d'
        print(stamp)
        return datetime.strptime(stamp, fmt)
    except:
        fmt = '%Y/%m/%d'
        return datetime.strptime(stamp, fmt)

def cal_propet_timestamp(stamp1, stamp2):
    tstamp1 = convert_prophet_timestamp(stamp1)
    tstamp2 = convert_prophet_timestamp(stamp2)
    if tstamp1 > tstamp2:
       td = tstamp1 - tstamp2
    else:
       td = tstamp2 - tstamp1
    td_mins = int(round(td.total_seconds() / 60))
    td_hrs  = int(td_mins/60)
    td_days = int(td_hrs/24)
    return td_mins, td_hrs, td_days

def prophet_cv_test(output_path, filename, val_days = '365 days', **cv_params):
    # initial='1825 days', period='365 days'
    loaded_model = pickle.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
    df_cv_table = cross_validation(loaded_model, val_days, **cv_params)
    df_performance = performance_metrics(df_cv_table)
    return df_cv_table, df_performance

def prophet_err_plot(filename, df_table, title_name = ' CV Test:'):
    specs = EDA.multiplot_strategy(6)
    fig = plt.figure(figsize=(35, 20))

    ax = plt.subplot(specs[0])
    MAPEFig = plot_cross_validation_metric(df_table, metric='mape', ax=ax, figsize=(35, 20))
    plt.grid()
    plt.legend()
    plt.ylabel('Mean Absolute Percent Error, MAPE')
    plt.title(filename + title_name + ' MAPE')

    ax = plt.subplot(specs[1])
    MAEFig = plot_cross_validation_metric(df_table, metric='mae', ax=ax, figsize=(35, 20))
    plt.grid()
    plt.legend()
    plt.ylabel('Mean Absolute Error, MAE')
    plt.title(filename + title_name + ' MAE')

    ax = plt.subplot(specs[2])
    MSEFig = plot_cross_validation_metric(df_table, metric='mse', ax=ax, figsize=(35, 20))
    plt.grid()
    plt.legend()
    plt.ylabel('Mean Squart Error, MSE')
    plt.title(filename + title_name + ' MSE')

    ax = plt.subplot(specs[3])
    RMSEFig = plot_cross_validation_metric(df_table, metric='rmse', ax=ax, figsize=(35, 20))
    plt.grid()
    plt.legend()
    plt.ylabel('Root Mean Squart Error, RMSE')
    plt.title(filename + title_name + ' RMSE')

    ax = plt.subplot(specs[4])
    ExpVarFig = plot_cross_validation_metric(df_table, metric='exp_var_score', ax=ax, figsize=(35, 20))
    plt.grid()
    plt.legend()
    plt.ylabel('Explained Variance Score')
    plt.title(filename + title_name + ' Explained Variance Score')

    ax = plt.subplot(specs[5])
    R2Fig = plot_cross_validation_metric(df_table, metric='r2', ax=ax, figsize=(35, 20))
    plt.grid()
    plt.legend()
    plt.ylabel('R2 Score, R2')
    plt.title(filename + title_name + ' R2 Score')
    
    return R2Fig

def prophet_cv_plot(df_cv, output_path, filename, figname = '', ylabel_tag = '', ax = None, val_days = '365 days', **cv_params):
    reg = pickle.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
    cutoff = df_cv['cutoff'].unique()[0]
    df_cv = df_cv[df_cv['cutoff'].values == cutoff]

    if ax == None:
       cv_err_plot = plt.figure(facecolor='w', figsize=(10, 6))
       ax = cv_err_plot.add_subplot(111)
    else:
       cv_err_plot = ax.get_figure()
    plt.plot(reg.history['ds'].values, reg.history['y'], 'k.', label = 'Historical Data')
    plt.plot(df_cv['ds'].values, df_cv['yhat'], ls='-', c='#0072B2', label = 'CV Test Forecasting Data')
    ax.fill_between(df_cv['ds'].values, df_cv['yhat_lower'],
                    df_cv['yhat_upper'], color='#0072B2',
                    alpha=0.2)
    ax.axvline(x=pd.to_datetime(cutoff), c='gray', lw=4, alpha=0.5, ls='--')
    ax.set_ylabel('y')
    ax.set_xlabel('ds')
    #ax.text(x=pd.to_datetime('2010-01-01'),y=12, s='Initial', color='black',
    #       fontsize=16, fontweight='bold', alpha=0.8)
    #ax.text(x=pd.to_datetime('2012-08-01'),y=12, s='Cutoff', color='black',
    #       fontsize=16, fontweight='bold', alpha=0.8)
    ax.axvline(x=pd.to_datetime(cutoff) + pd.Timedelta(val_days), c='gray', lw=4,
               alpha=0.5, ls='--')
    #ax.text(x=pd.to_datetime('2013-01-01'),y=6, s='Horizon', color='black',
    #       fontsize=16, fontweight='bold', alpha=0.8)

    plt.grid()
    plt.legend()
    plt.xlabel('Date')
    plt.ylabel('Time Series Value, '+ylabel_tag)
    plt.title('{} Time Series Cross Validation Test Plot'.format(figname))
    return cv_err_plot

def prophet_validation(df_x_val, output_path, filename):
    loaded_model = pickle.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
    # Validation
    df_forecast_table = loaded_model.predict(df_x_val)
    df_forecast_table['y'] = df_x_val['y']
    df_forecast_table['cutoff'] = [df_forecast_table.loc[0, 'ds']]*df_forecast_table.shape[0]
    df_forecast_table['ds'] = pd.to_datetime(df_forecast_table.loc[:,'ds'])
    df_forecast_table['cutoff'] = pd.to_datetime(df_forecast_table.loc[:,'cutoff'])
    df_forecast_performance = performance_metrics(df_forecast_table)
    return df_forecast_table, df_forecast_performance

def prophet_validation_components_plot(df_forecast, output_path, filename):
    loaded_model = pickle.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
    # Validation Plot
    plt.title(filename + ' Component Analysis')
    df_forecast_components_plot = loaded_model.plot_components(df_forecast)
    return df_forecast_components_plot

def prophet_validation_plot(df_train, df_val, output_path, filename, figname = '', ylabel_tag = '', ax = None):
    #print(os.path.join(output_path, filename + '.sav'))
    loaded_model = pickle.load(open(os.path.join(output_path, filename + '.sav'), 'rb'))
    #print(pd.DataFrame({'ds': df_val['ds'], 
	#                    'floor': [1*min(df_train['y'])]*df_val.shape[0], 
    #                    'cap': [1*max(df_train['y'])]*df_val.shape[0]}))

    df_train, df_val = df_data2prophet_data(df_train, df_val, ['ds'], ['y'])
    df_forecast = loaded_model.predict(pd.DataFrame(df_val))
    df_forecast_plot = loaded_model.plot(df_forecast, ax = ax)
    plt.grid()
    plt.plot(df_forecast['ds'], df_val['y'], 'r', label = 'True Validation Data')
    plt.grid()

    plt.legend()
    plt.xlabel('Date')
    plt.ylabel('Time Series Value, '+ylabel_tag)
    plt.title('{} Time Series Validation Plot'.format(figname))
    #plt.savefig(os.path.join(output_path, filename + "_PredPlot.svg"))
    return df_forecast_plot
    
def cv_results_record_init(reg_table_length):
    reg_table_dataType = np.dtype([('Regressior Name', h5py.special_dtype(vlen=str)),
                                   ('Regressior Parameters', h5py.special_dtype(vlen=str)),
                                   ('Regressior CV R2', np.float),
                                   #('Regressior CV Horizon', np.float),
                                   ('Regressior CV Mean Squart Error, MSE', np.float),
                                   #('Regressior CV Mean Squart Error (C), MSE', np.float),
                                   ('Regressior CV Root Mean Squart Error, RMSE', np.float),
                                   ('Regressior CV Median Absolute Error', np.float),
                                   ('Regressior CV Mean Absolute Error, MAE', np.float),
                                   #('Regressior CV Mean Absolute Error (C), MAE', np.float),
                                   ('Regressior CV Explained Variance', np.float),
								   ('Regressior CV MAPE', np.float),
                                   ('Regressior CV Coverage', np.float),
								   ('Regressior Val R2', np.float),
                                   ('Regressior Val Mean Squart Error, MSE', np.float),
                                   ('Regressior Val Root Mean Squart Error, RMSE', np.float),
                                   ('Regressior Val Median Absolute Error', np.float),
								   ('Regressior Val Mean Absolute Error, MAE', np.float),
                                   ('Regressior Val Explained Variance', np.float),
                                   ('Regressior Val MAPE', np.float),
                                   ('Regressior Val Coverage', np.float)])
    reg_table = np.zeros((reg_table_length,), dtype=reg_table_dataType)
    return (reg_table, reg_table_dataType)

def cv_reg_table_record(modelname, reg_table_group, all_results, row_index, base_output_folder, output_folder):
    (reg_table, reg_table_dataType) = reg_table_group
    (df_cv_table, df_cv_performance, df_val_table, df_val_performance) = all_results 
    print(df_cv_table)
    print(df_cv_performance)
    print(df_val_table)
    print(df_val_performance)
    reg_table['Regressior Name'][row_index] = modelname
    reg_table['Regressior Parameters'][row_index] = None #cv_results['estimator']#str(clf.get_params())
    #reg_table['Regressior CV Horizon'][row_index] = df_cv_performance['horizon'].mean()
    reg_table['Regressior CV R2'][row_index] = df_cv_performance['r2'].mean()
    reg_table['Regressior CV Mean Squart Error, MSE'][row_index] = df_cv_performance['mse'].mean()
    #reg_table['Regressior CV Mean Squart Error (C), MSE'][row_index] = df_cv_performance['mse'].mean()
    reg_table['Regressior CV Root Mean Squart Error, RMSE'][row_index] = df_cv_performance['rmse'].mean()
    #reg_table['Regressior CV Median Absolute Error'][row_index] = df_cv_table['median absolute error'].mean()
    reg_table['Regressior CV Mean Absolute Error, MAE'][row_index] = df_cv_performance['mae'].mean()
    #reg_table['Regressior CV Mean Absolute Error (C), MAE'][row_index] = df_cv_performance['mae'].mean()
    reg_table['Regressior CV Explained Variance'][row_index] = df_cv_performance['exp_var_score'].mean()
    reg_table['Regressior CV MAPE'][row_index] = df_cv_performance['mape'].mean()
    reg_table['Regressior CV Coverage'][row_index] = df_cv_performance['coverage'].mean()
    reg_table['Regressior Val R2'][row_index] = df_val_performance['r2'].mean()
    reg_table['Regressior Val Mean Squart Error, MSE'][row_index] = df_val_performance['mse'].mean()
    reg_table['Regressior Val Root Mean Squart Error, RMSE'][row_index] = df_val_performance['rmse'].mean()
    #reg_table['Regressior Val Median Absolute Error'][row_index] = df_val_performance['median_absolute_error'].mean()
    reg_table['Regressior Val Mean Absolute Error, MAE'][row_index] = df_val_performance['mae'].mean()
    reg_table['Regressior Val Explained Variance'][row_index] = df_val_performance['exp_var_score'].mean()
    reg_table['Regressior Val MAPE'][row_index] = df_val_performance['mape'].mean()
    reg_table['Regressior Val Coverage'][row_index] = df_val_performance['coverage'].mean()

    # Results Save
    reg_cv_table = np.sort(reg_table, order='Regressior CV Root Mean Squart Error, RMSE')
    H5FM.Data2H5(reg_cv_table, os.path.join(base_output_folder, 'SimpleModelAnalysis'),
                'REG Nested CV Table', groupName="ML Analysis", dtype=reg_table_dataType)
    df_reg_cv_table = pd.DataFrame(reg_cv_table)
    df_reg_cv_table.to_csv(os.path.join(output_folder,  modelname + "_reg_table.csv"), index=False)
    df_reg_cv_table.to_csv(os.path.join(output_folder,  "all_model_reg_table.csv"), index=False, float_format = '%.2f')
    df_reg_cv_table.drop(columns = ['Regressior Parameters']).to_csv(os.path.join(output_folder,  "all_model_reg_table_d.csv"), index=False, float_format = '%.2f')

class fbprophetMLHelper():
      def __init__(self, params):
          #self.reg = Prophet(**params)
          self.grid_params = ParameterGrid(params)
          self.regs, self.regs_name = self.__grid_search_models__()
          #print(self.regs, self.regs_name)
          self.ml_output_count = 0
          self.base_path = CU.updateOutputFolder(_output_main_folder+'//', 'SimpleModel')
          self.output_path = self.__output_paths__()
      
      def __grid_search_models__(self):
          regs, regs_name = [], []
          for idx, param in enumerate(self.grid_params):
              regs.append(Prophet(**param))
              reg = Prophet()
              regs_name.append(reg.__class__.__name__+'_'+str(idx))

          return regs, regs_name

      def __output_paths__(self):
          global _output_main_folder
          paths = {}
          paths = [None]*len(self.regs)
        
          for idx, reg in enumerate(self.regs):
              modelname = self.regs_name[idx]
              paths[idx] = os.path.join(self.base_path,
                                        modelname)
              CU.createOutputFolder(paths[idx])
              #print(self.grid_params[idx])
              pd.DataFrame(self.grid_params[idx]).to_csv(os.path.join(paths[idx], 'param_setting.csv'), index = False)
        
          setting_files_list = glob.glob('*.txt')
          for copy_file_path in setting_files_list:
              shutil.copy(copy_file_path, self.base_path)
          self.ml_output_count += 1
          return paths

      def data_preparation(self, raw_x, raw_y, 
	                       tail_name_to_train, 
						   fit_model_file_path, 
                           FeatureEngineeringFitObj, 
						   LabelEncoderFitObj = None, 
						   train_drop_column = None,
						   train_size = 0.77):
          for idx, modelname in enumerate(self.regs_name):
              op = self.output_path[idx]
              length = raw_y.shape[0]
              train_idx = int(length*train_size)
              x_train, x_val = raw_x.iloc[0:train_idx,:], raw_x.iloc[train_idx:,:]
              y_train, y_val = raw_y.iloc[0:train_idx,:], raw_y.iloc[train_idx:,:]
              x_train.to_csv(os.path.join(op, 'x_raw_train.csv'), index=False)
              x_val.to_csv(os.path.join(op, 'x_raw_val.csv'), index=False)
              y_train.to_csv(os.path.join(op, 'y_raw_train.csv'), index=False)
              y_val.to_csv(os.path.join(op, 'y_raw_val.csv'), index=False)
              
              y_train.to_csv(os.path.join(op, 'y_train.csv'), index=False)
              y_val.to_csv(os.path.join(op, 'y_val.csv'), index=False)
              
              if (LabelEncoderFitObj):
                 LabelEncoderFitObj('y_train', op, fit_model_file_path)
                 LabelEncoderFitObj('y_val', op, fit_model_file_path)

              train = pd.concat([x_train, y_train], axis=1)
              train.to_csv(os.path.join(op, "train.csv"), index=False)
            
              val=pd.concat([x_val, y_val], axis=1)
              val.to_csv(os.path.join(op, "val.csv"), index=False)

              FeatureEngineeringFitObj("train", op, fit_model_file_path)
              FeatureEngineeringFitObj("val", op, fit_model_file_path)
			  
              train_data = read_data(os.path.join(op, "train"+tail_name_to_train))
              if (train_drop_column):
                  train_data = train_data.drop(columns= train_drop_column, axis=1)
              train_data.to_csv(os.path.join(op, 'x_train.csv'), index=False)
              cv_test_data = read_data(os.path.join(op, "val"+tail_name_to_train))
              if (train_drop_column):
                  cv_test_data = cv_test_data.drop(columns= train_drop_column, axis=1)
              cv_test_data.to_csv(os.path.join(op, 'x_val.csv'), index=False)

              #splitObj = timeseries_sampling_split_obj(n_splits = 10, max_train_size = 0.77)
              #return splitObj

      def train(self, x_train, y_train):
          obj = self.regs[0].fit(x_train, y_train)
          self.__model_save__(obj)	  
	  
      def cv_train(self, ds_column = ['ds'], ts_model_column = ['a'], **cv_params):
          reg_table_group = cv_results_record_init(len(self.regs))

          for idx, reg in enumerate(self.regs):
              op = self.output_path[idx]
              # Data Preparation
              x_train, y_train = read_data(os.path.join(op, 'x_train')), read_data(os.path.join(op, 'y_train'))
              x_val, y_val = read_data(os.path.join(op, 'x_val')), read_data(os.path.join(op, 'y_val'))

              # Data Organization
              for y_column in ts_model_column:
                  filename = y_column + '_'+ str(idx)
                  df_train, df_val = df_data2prophet_data(x_train, x_val, ds_column, y_column)
                  #df_train = df_data2prophet_data(x_train, ds_column, y_column)
                  #df_val = df_data2prophet_data(x_val, ds_column, y_column)

                  # CV Training/Testing and Validation
                  self.__train__(reg, df_train, op, y_column + '_'+ str(idx))
                  df_cv_table, df_cv_performance = self.__cv_test__(df_val, y_val, op,  filename, ylabel_tag = y_column, **cv_params)
                  df_val_table, df_val_performance = self.__validation__(df_train, df_val, op, filename, ylabel_tag = y_column)
                  self.__cv_results_record__(reg_table_group, df_cv_table, df_cv_performance, df_val_table, df_val_performance, op, 'prophet_'+ filename, idx)

      def __cv_results_record__(self, reg_table_group, df_cv_table, df_cv_performance, df_val_table, df_val_performance, output_path, filename, row_index):
          df_cv_table.to_csv(os.path.join(output_path,"{}_cv_table.csv".format(filename)), index=False)
          df_cv_performance.to_csv(os.path.join(output_path,"{}_cv_performance.csv".format(filename)), index=False)
          df_val_table.to_csv(os.path.join(output_path,"{}_val_table.csv".format(filename)), index=False)
          df_val_performance.to_csv(os.path.join(output_path,"{}_val_performance.csv".format(filename)), index=False)
          all_results = (df_cv_table, df_cv_performance, df_val_table, df_val_performance)
          cv_reg_table_record(filename, reg_table_group, all_results, row_index, self.base_path, output_path)		   

      def __cv_test__(self, df_val, y_val, output_path, filename, ylabel_tag = None, **cv_params):
          # Time Series Training/ Testing
          df_cv_table, df_cv_performance = prophet_cv_test(output_path, filename, **cv_params)
          CVErrFig = prophet_err_plot(filename, df_cv_table, title_name = ' CV Test:')
          CVErrFig.savefig(os.path.join(output_path, filename + "_CV_Evaluation.svg") )
          
          #MSEFig, RMSEFig, MAEFig, MAPEFig, ExpVarFig, R2Fig = prophet_err_plot(filename, df_cv_table, title_name = ' CV Test:')
          #MSEFig.savefig(os.path.join(output_path, filename + "_CVMSEPlot.svg") )
          #RMSEFig.savefig(os.path.join(output_path, filename + "_CVRMSEPlot.svg") )
          #MAEFig.savefig(os.path.join(output_path, filename + "_CVMAEPlot.svg") )
          #MAPEFig.savefig(os.path.join(output_path, filename + "_CVMAPEPlot.svg") )
          #ExpVarFig.savefig(os.path.join(output_path, filename + "_CVExpVarPlot.svg") )
          #R2Fig.savefig(os.path.join(output_path, filename + "_CVR2Plot.svg") )
          
          cvFig = prophet_cv_plot(df_cv_table, output_path, filename, figname = 'Prophet_'+filename, ylabel_tag = ylabel_tag, ax = None, **cv_params)
          
          cvFig.savefig(os.path.join(output_path, filename + "_CVPlot.svg") )
          return df_cv_table, df_cv_performance
 
      def __validation__(self, df_train, df_val, output_path, filename, ylabel_tag = None):
          df_forecast_table, df_forecast_performance = prophet_validation(df_val, output_path, filename)
          ValErrFig = prophet_err_plot(filename, df_forecast_table, title_name = ' Validation:')
          ValErrFig.savefig(os.path.join(output_path, filename + "_ValidationEvaluation.svg") )
          #MSEFig, RMSEFig, MAEFig, MAPEFig, ExpVarFig, R2Fig = prophet_err_plot(filename, df_forecast_table, title_name = ' Validation:')
          #MSEFig.savefig(os.path.join(output_path, filename + "_ValMSEPlot.svg") )
          #RMSEFig.savefig(os.path.join(output_path, filename + "_ValMSEPlot.svg") )
          #MAEFig.savefig(os.path.join(output_path, filename + "_ValMAEPlot.svg") )
          #MAPEFig.savefig(os.path.join(output_path, filename + "_ValMAPEPlot.svg") )
          #ExpVarFig.savefig(os.path.join(output_path, filename + "_ValExpVarPlot.svg") )
          #R2Fig.savefig(os.path.join(output_path, filename + "_ValR2Plot.svg") )          

          df_forecast_components_plot = prophet_validation_components_plot(df_forecast_table, output_path, filename)
          df_forecast_plot = prophet_validation_plot(df_train, df_val, output_path, filename, figname = 'Prophet_'+filename, ylabel_tag = ylabel_tag, ax = None)
          df_forecast_components_plot.savefig(os.path.join(output_path, filename + "_ForecastComponentsPlot.svg") )
          df_forecast_plot.savefig(os.path.join(output_path, filename + "_ForecastPlot.svg") )          
          return df_forecast_table, df_forecast_performance

      def __train__(self, reg, df_train, output_path, filename):
          reg.fit(df_train)
          pickle.dump(reg, open(os.path.join(output_path, "{}.sav".format(filename)), 'wb'))

"""
# Time Series Validation
#tscv_splitObj = SM.timeseries_sampling_split_obj(max_train_size = 0.77)
#scoring = 'neg_mean_absolute_error'
#results = cross_val_score(model, df_train, df_val, cv=tscv_splitObj, scoring=scoring)
#print(results)

df_forecast = reg.predict(df_val)
y_true = y_val['y'].values.tolist()
y_pred = df_forecast['yhat'].values.tolist()
mse, rmse, median_abs_err, mean_abs_err, exp_var_score, r2 = ME.reg_evaluation(y_true, y_pred)
err_list = list(zip([mse], [rmse], [median_abs_err], [mean_abs_err], [mean_sqr_log_err], [exp_var_score], [r2]))
err_columns = ['mse', 'rms', 'median_absolute_error', 'mean_absolute_error', 'mean_squared_log_error', 'explained_variance_score', 'r2_score']
pd.DataFrame(err_list, columns = err_columns)

#df_forecast['y'] = y_val['y']
#df_forecast_performance = performance_metrics(df_forecast)
#H5FM.Data2H5(df_cv, os.path.join(base_output_folder, 'SimpleModelAnalysis'), 'CV Table', groupName="ML Analysis", dtype=table_dataType)

#tscv_splitObj = SM.timeseries_sampling_split_obj(max_train_size = 0.77)

#for train_index, test_index in tscv_splitObj.split(x_train):
#    print("TRAIN:", train_index, "TEST:", test_index)
#cv_results = cross_validation(reg)
#scores = cross_val_score(reg, x_train, y_train, cv=tscv_splitObj)

		  df_cv_table_dtypes = np.dtype([('ds', h5py.special_dtype(vlen=str)),
                                         ('yhat', np.float),
                                         ('yhat_lower',np.float),
                                         ('yhat_upper',np.float),
                                         ('y', np.float),
                                         ('cutoff', h5py.special_dtype(vlen=str))])
          df_cv_performance_dtypes = np.dtype([('horizon', np.float),
                                               ('mse', np.float),
                                               ('rmse',np.float),
                                               ('mae',np.float),
                                               ('mape', np.float),
                                               ('coverage', np.float)])
          df_val_forecast_dtypes = np.dtype([('ds', h5py.special_dtype(vlen=str)),
                                             ('trend', np.float),
                                             ('cap',np.float),
                                             ('yhat_lower',np.float),
                                             ('yhat_upper', np.float),
                                             ('trend_lower', np.float),
                                             ('trend_upper', np.float),
                                             ('additive_terms', np.float),
                                             ('additive_terms_lower', np.float),
                                             ('additive_terms_upper', np.float),
                                             ('weekly', np.float),
                                             ('weekly_lower', np.float),
                                             ('weekly_upper', np.float),
                                             ('yearly', np.float),
                                             ('yearly_lower', np.float),
                                             ('yearly_upper', np.float ),
                                             ('multiplicative_terms', np.float),
                                             ('multiplicative_terms_lower', np.float),
                                             ('multiplicative_terms_upper', np.float),
                                             ('yhat', np.float)])
          df_val_forecast_performance_dtypes = np.dtype([('mse', np.float),
                                                         ('rmse', np.float),
                                                         ('median_absolute_error',np.float),
                                                         ('mean_absolute_error',np.float),
                                                         ('explained_variance_score', np.float),
                                                         ('r2_score', np.float)])
          
		  #df_cv_table_dtypes = np.dtype(list(zip(df_cv_table.dtypes.index, df_cv_table.dtypes)))
          numpy_cv_table= np.array(df_cv_table.values) #, dtype=df_cv_table_dtypes
          #df_cv_performance_dtypes = np.dtype(list(zip(df_cv_performance.dtypes.index, df_cv_performance.dtypes)))
          numpy_cv_performance = np.array(df_cv_performance.values) #, dtype=df_cv_performance_dtypes
          #df_val_forecast_dtypes = np.dtype(list(zip(df_val_forecast.dtypes.index, df_val_forecast.dtypes)))
          numpy_val_forecast = np.array(df_val_forecast.values) #, dtype=df_val_forecast_dtypes
          #df_val_forecast_performance_dtypes = np.dtype(list(zip(df_val_forecast_performance.dtypes.index, df_val_forecast_performance.dtypes)))
          numpy_val_forecast_performance = np.array(df_val_forecast_performance.values) #, dtype=df_val_forecast_performance_dtypes
														 
          H5FM.Data2H5(numpy_cv_table, os.path.join(self.base_path, 'SimpleModelAnalysis'),
                       filename + ' CV Table', groupName="ML Analysis", dtype=df_cv_table_dtypes) #	  
          H5FM.Data2H5(numpy_cv_performance, os.path.join(self.base_path, 'SimpleModelAnalysis'),
                       filename + ' CV Performance', groupName="ML Analysis", dtype=df_cv_performance_dtypes) #
          H5FM.Data2H5(numpy_val_forecast, os.path.join(self.base_path, 'SimpleModelAnalysis'),
                       filename + ' Validation Predictation', groupName="ML Analysis", dtype=df_val_forecast_dtypes) #
          H5FM.Data2H5(numpy_val_forecast_performance, os.path.join(self.base_path, 'SimpleModelAnalysis'),
                       filename + ' Validation Performance', groupName="ML Analysis", dtype=df_val_forecast_performance_dtypes) #					   
		  
"""			  