'''
@Author: CY Peng
@Date: 2019-11-06
@LastEditTime: 2020-01-14

DataVer  - Author - Note
2019/11/6     CY    Initialization
2019/11/11    CY   ROC Plot
2019/11/11    CY   Features Importance
2020/01/14    CY    Regression Error

Ref.
https://stackoverflow.com/questions/45376410/how-to-get-roc-curve-for-decision-tree, roc curve
https://scikit-learn.org/stable/auto_examples/model_selection/plot_roc_crossval.html#sphx-glr-auto-examples-model-selection-plot-roc-crossval-py, roc curve
'''

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
from itertools import cycle
from scipy import interp
import math

# Machine Learning
from sklearn.metrics import accuracy_score, log_loss, confusion_matrix, classification_report, roc_curve, auc
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split

from sklearn.metrics import mean_squared_error
from sklearn.metrics import median_absolute_error
from sklearn.metrics import mean_squared_log_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import explained_variance_score
from sklearn.metrics import r2_score

# Permutation Importance
import eli5  # Permutation Importance
from eli5.sklearn import PermutationImportance # Permutation Importance
#from IPython.display import display
from IPython.core.display import HTML

# Shap Value
import shap  # package used to calculate Shap values

# Regression Evaluation
def reg_evaluation(y_true_list, y_pred_list):
    mse, rmse, median_abs_err, mean_abs_err, exp_var_score = [], [], [], [], []
    for idx in range(0, len(y_true_list)):

        y_true, y_pred = [y_true_list[idx]], [y_pred_list[idx]]
        
        e_mse = mean_squared_error(y_true,y_pred) #, multioutput= 'raw_values'
        e_rmse = np.sqrt(mean_squared_error(y_true,y_pred)) #, multioutput= 'raw_values'
        e_median_abs_err = median_absolute_error(y_true,y_pred) #, multioutput= 'raw_values'
        e_mean_abs_err = mean_absolute_error(y_true,y_pred) #, multioutput= 'raw_values'
#        mean_sqr_log_err = mean_squared_log_error(y_true,y_pred)
        e_exp_var_score = explained_variance_score(y_true,y_pred) #, multioutput= 'raw_values'
        mse.append(e_mse)
        rmse.append(e_rmse)
        median_abs_err.append(e_median_abs_err)
        mean_abs_err.append(e_mean_abs_err)
        exp_var_score.append(e_exp_var_score)
    r2 = pd.Series([r2_score(y_true_list, y_pred_list)]).repeat(len(y_true_list)).tolist() #, multioutput= 'raw_values'
	
    return (mse, rmse, median_abs_err, mean_abs_err, exp_var_score, r2)

# Classification Evaluation
# ROC Curve and Confusion Matrix
def binary_clf_confusion_matrix(y_true, y_pred, params, outputFolder, filename):
    """
    :param y_true:
    :param y_pred:
    :param outputFolder:
    :param params:
       confusion_matrix(y, y_pred, labels=[1, 0])
       classification_report(y, y_pred, labels=None, sample_weight=None, digits=2, output_dict=True)
    :return:
    """
    params1 = params['confusion_matrix']
    params2 = params['clf_report']
    params2['output_dict'] = True
    cm = confusion_matrix(y_true, y_pred, **params1)
    clf_report = classification_report(y_true, y_pred, **params2)

    df_clf_report = pd.DataFrame(clf_report)
    df_clf_report.T.to_csv(os.path.join(outputFolder, filename + "_verify_clf_report.csv"))

    df_cm = pd.DataFrame(cm)
    df_cm.T.to_csv(os.path.join(outputFolder, filename + "_verify_cm.csv"))
    return df_cm, df_clf_report

def binary_clf_roc_curve_plot(fpr, tpr, roc_auc, plot_setting = None):
    plt.plot(fpr["micro"], tpr["micro"],
             label='micro-average, area = {0:0.2f}' # ROC curve
                   ''.format(roc_auc["micro"]),
             color='deeppink', linestyle=':', linewidth=4)

    plt.plot(fpr["macro"], tpr["macro"],
             label='macro-average, area = {0:0.2f}' # ROC curve
                   ''.format(roc_auc["macro"]),
             color='navy', linestyle=':', linewidth=4)

    colors = cycle(['aqua', 'darkorange', 'cornflowerblue'])
    for i, color in zip(range(2), colors):
        plt.plot(fpr[i], tpr[i], color=color, lw=2,
                 label='class {0}, area = {1:0.2f}' #ROC curve of 
                       ''.format(i, roc_auc[i]))
    plt.plot([0, 1], [0, 1], 'k--', lw=2)
    #plt.xlim([0.0, 1.0])
    #plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    if plot_setting == None:
        plt.title('Some Extension of Receiver Operating Characteristic to Multi-Class')
    else:
        plt.title(plot_setting['title'])
    #plt.ylabel(plot_setting['ylabel'])
    #plt.xlabel(plot_setting['xlabel'])
    plt.legend(loc="lower right")
    plt.grid()

def binary_clf_cal_roc_auc(y_true, y_pred):
    # Compute ROC curve and ROC area for each class
    fpr, tpr, roc_auc = dict(), dict(), dict()
    for clf_label in range(2):
        fpr[clf_label], tpr[clf_label], thresholds = roc_curve(y_true[:, clf_label], y_pred[:, clf_label])
        roc_auc[clf_label] = auc(fpr[clf_label], tpr[clf_label])

    # Compute micro-average ROC curve and ROC area
    fpr["micro"], tpr["micro"], thresholds = roc_curve(y_true.ravel(), y_pred.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    # Plot ROC curves for the multiclass problem
    # Compute macro-average ROC curve and ROC area
    # First aggregate all false positive rates
    all_fpr = np.unique(np.concatenate([fpr[i] for i in range(2)]))

    # Then interpolate all ROC curves at this points
    mean_tpr = np.zeros_like(all_fpr)
    for i in range(2):
        mean_tpr += interp(all_fpr, fpr[i], tpr[i])

    # Finally average it and compute AUC
    mean_tpr /= 2

    fpr["macro"] = all_fpr
    tpr["macro"] = mean_tpr
    roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])
    return fpr, tpr, roc_auc

def check_prophet_ts_based_model(modelObj):
    #print(modelObj.__class__.__name__)
    if 'Prophet' in modelObj.__class__.__name__:
      return True
    return False

def check_basic_ts_model(modelObj):
    if 'ARIMA' in modelObj.__class__.__name__:
      return True
    if 'SARIMAX' in modelObj.__class__.__name__:
      return True
    if 'UnobservedComponents' in modelObj.__class__.__name__:
      return True
    return False

def check_tree_based_model(modelObj):
    try:
        check = modelObj.feature_importances_
        del check
        return True
    except:
        pass
    try:
        check = modelObj.best_estimator_.feature_importances_
        del check
        return True
    except:
        pass
    return False

def check_probability_based_model(modelObj, X):
    try:
        check = modelObj.predict_proba(X)
        del check
        return True
    except:
        pass
    try:
        check = modelObj.best_estimator_.predict_proba(X)
        del check
        return True
    except:
        pass
    return False

def df_data_tree_based_feature_importance(modelObj):
    try:
        importances = modelObj.feature_importances_
    except:
        importances = modelObj.best_estimator_.feature_importances_
    indices = np.argsort(importances)[::-1]
    #print("Feature ranking:")
    #for f in range(df_val_X.shape[1]):
    #    print("{} feature {} {}".format(df_val_X.columns[f], indices[f], importances[indices[f]]))
    return importances, indices

def df_data_tree_based_feature_importance_plot(modelObjGroup, df_val_X, plot_setting = None):
    modelObj, importances, indices = modelObjGroup
    try:
        std = np.std([tree.feature_importances_ for tree in modelObj.estimators_],
                     axis=0)
        plt.bar(range(df_val_X.shape[1]), importances[indices],
                color="r", yerr=std[indices], align="center")
    except:
        plt.bar(range(df_val_X.shape[1]), importances[indices],
                color="r", align="center")
    plt.xticks(range(df_val_X.shape[1]), df_val_X.columns[indices], rotation=90)
    plt.xlim([-1, df_val_X.shape[1]])
    plt.grid()
    if plot_setting == None:
        plt.title("Feature importances Plot")
    else:
        plt.title(plot_setting['title'])
        #plt.ylabel(plot_setting['ylabel'])
        #plt.xlabel(plot_setting['xlabel'])
    plt.xlabel("Features")
    plt.ylabel("Importance")
    #modelObj.visualize_features()

def df_data_permulation_importance(modelObj, df_val_X, df_val_y, outputFolder, filename, random_state = 1):
    perm = PermutationImportance(modelObj, random_state=random_state).fit(df_val_X, df_val_y)
    piHTML = eli5.show_weights(perm, feature_names = df_val_X.columns.tolist())
    with open(os.path.join(outputFolder, filename + '_permulation_importance.html'), 'w') as f:
        f.write(piHTML.data)
    f.close()

def df_data_ensemble_tree_shap_force_each_plot(modelObj, df_val_X, output_class_num, outputFolder, filename):
    """"
    Note
    We usually use the one val_X
    """
    idx = np.random.randint(len(df_val_X), size=1)
    one_val_X = df_val_X.iloc[idx,:]
    explainer = shap.TreeExplainer(modelObj)
    shap_values = explainer.shap_values(one_val_X)

    for idx in range(0, output_class_num):
        #print("Type {}".format(idx))
        shap.initjs()
        shapHTML = shap.force_plot(explainer.expected_value[idx], shap_values[idx], one_val_X, matplotlib = False)
        with open(os.path.join(outputFolder, '{}_type_{}_single_shap.html'.format(filename, idx)), 'w') as f:
            f.write(shapHTML.data)
        f.close()

def df_data_ensemble_tree_shap_force_summary_plot(modelObj, df_val_X, output_class_num, outputFolder, filename):
    """"
    Note
    We usually use the small val_X
    """
    idx = np.random.randint(len(df_val_X), size=int(0.1*len(df_val_X)))
    val_X = df_val_X.iloc[idx, :]
    explainer = shap.TreeExplainer(modelObj)
    shap_values = explainer.shap_values(val_X)

    for idx in range(0, output_class_num):
        print("Type {}".format(idx))
        shap.initjs()
        shapHTML = shap.force_plot(explainer.expected_value[idx], shap_values[idx], val_X, matplotlib = False)
        with open(os.path.join(outputFolder, '{}_type_{}_shap.html'.format(filename, idx)), 'w') as f:
            f.write(shapHTML.data)
        f.close()

    shap.summary_plot(shap_values, df_val_X, show=False)
    plt.grid()
    plt.savefig(os.path.join(outputFolder, '{}_shap_summary.svg'.format(filename)))
    #with open(os.path.join(outputFolder, '{}_shap_summary.html'.format(filename)), 'w') as f:
    #    f.write(shapHTML.data)
    plt.close("all")

def df_data_features_ranking_roc_curve(df_X, df_y, outputFolder, filename):
    plt.figure(figsize=(13, 7))
    y_ = np.array(df_y).T[0]
    sum_y_ = sum(y_ == 1)
    sum__y_ = sum(y_ == 0)
    pidx = np.where(y_ == 1)
    nidx = np.where(y_ == 0)
    #print(math.ceil(df_X.shape[1] / 10))
    for col in df_X.columns:
        tpr, fpr = [], []
        for threshold in np.linspace(min(df_X[col]), max(df_X[col]), 100):
            detP = df_X[col] < threshold
            TP_TN = (np.array(detP.astype(int)) == y_).astype(int)
            FP_FN = (np.array(detP.astype(int)) == 1-y_).astype(int)
            sum_TP = sum(TP_TN[pidx])
            sum_FP = sum(FP_FN[nidx])
            tpr.append(sum_TP / sum_y_)  # TP/P, aka recall
            fpr.append(sum_FP / sum__y_)  # FP/N
        this_auc = auc(fpr, tpr)
        if (this_auc < .5):
            aux = tpr
            tpr = fpr
            fpr = aux
            this_auc = auc(fpr, tpr)
        if (this_auc > .5):
            plt.plot(fpr, tpr, label=col + ', auc = '+ str(np.round(this_auc, decimals=3)))
    plt.title('ROC curve')
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend()
    plt.grid()
    plt.savefig(os.path.join(outputFolder, '{}_features_ranking_roc.svg'.format(filename)))
    #plt.show()

# Margins on the training set are predictive of margins of the test set, which is why margins are a better measure of performance than the training error.
# We want to see how variable is the CDF of the scores, as a function of the size of the training set.
# We use bootstrap sampling.
# We plot the CDF for each class for each of the bootstrap samples.
def plot_margins(_train_size):
    plt.figure(figsize=(8, 6))
    for i in range(10):
        X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=_train_size, test_size=1 - _train_size)
        X_test, X_val, y_test, y_val = train_test_split(X_test, y_test, test_size=0.5)

        dtrain = xgb.DMatrix(X_train, label=y_train)
        dval = xgb.DMatrix(X_val, y_val)
        dtest = xgb.DMatrix(X_test, label=y_test)
        legends = []
        # Use early_stopping_rounds=5 to enable early stopping
        for num_round in [10, 100]:
            bst = xgb.train(plst, dtrain, num_round, evallist, verbose_eval=False)
            y_pred = bst.predict(dtest, ntree_limit=bst.best_ntree_limit, output_margin=True)
            thresholds = sorted(np.unique(np.round(y_pred, 2)))
            error_cuv, error_ger = xgbh.get_error_values(y_pred, y_test, thresholds)
            legends += ['Cuviers %d' % num_round, 'Gervais %d' % num_round]
            _style = ['y', 'g'] if num_round == 100 else ['b', 'r']
            xgbh.get_margin_plot(error_cuv, error_ger, thresholds, legends=legends, style=_style)

        plt.grid(which='major', linestyle='-', linewidth='0.5', color='gray')
        plt.grid(which='minor', linestyle=':', linewidth='0.5', color='gray')
        thr = thresholds / (np.max(thresholds) - np.min(thresholds))
    plt.title('data_size=%4.3f' % (X_train.shape[0]))
    plt.show()

# The CDFs provide information on the variation of the aggregate. If we want to estimate the confidence on a single example we need to compute the variance per example
# Each line segment represents the average man and std of a set of examples, stratified by the score.
def get_error_ranges(error_type_samp, thresholds_samp, num_chunks=20):
    error_type_bin = np.array(np.array(error_type_samp) * num_chunks, dtype=int)
    error_type_bin[error_type_bin == num_chunks] = num_chunks - 1

    min_type = np.zeros(num_chunks, dtype=float)
    max_type = np.zeros(num_chunks, dtype=float)

    normalizing_factor = (max(thresholds_samp) - min(thresholds_samp))

    for i in range(num_chunks):
        min_type[i] = thresholds_samp[np.min(np.where(error_type_bin == i))] / normalizing_factor
        max_type[i] = thresholds_samp[np.max(np.where(error_type_bin == i))] / normalizing_factor

    return min_type, max_type

def generate_samples(data, size=500, num_chunks=20):
    for i in range(200):
        if i == 0:
            min_type = np.zeros(num_chunks, dtype=float)
            max_type = np.zeros(num_chunks, dtype=float)

        # Sampling Random indices for selection
        samp_indices = np.random.randint(len(data), size=size)

        # Test data and labels
        X_samp = data[samp_indices, :-1]
        y_samp = np.array(data[samp_indices, -1], dtype=int)

        # Test predictions
        dsamp = xgb.DMatrix(X_samp, label=y_samp)
        y_samp_pred = bst.predict(dsamp, ntree_limit=bst.best_ntree_limit, output_margin=True)

        thresholds_samp = sorted(np.unique(np.round(y_samp_pred, 2)))
        error_cuv_samp, error_ger_samp = xgbh.get_error_values(y_samp_pred, y_samp, thresholds_samp)

        min_cuv_samp, max_cuv_samp = get_error_ranges(error_cuv_samp, thresholds_samp)
        if i == 0:
            min_cuv = min_cuv_samp
            max_cuv = max_cuv_samp
        else:
            min_cuv[min_cuv > min_cuv_samp] = min_cuv_samp[min_cuv > min_cuv_samp]
            max_cuv[max_cuv < max_cuv_samp] = max_cuv_samp[max_cuv < max_cuv_samp]

    for i in range(20):
        plt.plot([min_cuv[i], max_cuv[i]], [i / 20.0, i / 20.0], 'b')

"""
plt.figure(figsize=(8, 6))
#legends = ['Cuviers', 'Gervais']

#Best thresholds from the ROC Analysis
thr_lower_index = np.min(np.where((tpr > 0.95)))
thr_upper_index = np.max(np.where((tpr  < 0.6)))
thr_lower, thr_upper = thresholds[thr_lower_index], thresholds[thr_upper_index]
thr_lower_norm = thr_lower/(np.max(thresholds) - np.min(thresholds))
thr_upper_norm = thr_upper/(np.max(thresholds) - np.min(thresholds))
print("Thresholds (lower, upper):", thr_lower_norm, thr_upper_norm)

generate_samples(data, num_chunks=20)

#xgbh.get_margin_plot(error_cuv_100, error_ger_100, thresholds_100, legends = legends, style=['y', 'g'])
plt.plot([thr_lower_norm, thr_lower_norm], [0, 1], 'm:')
plt.plot([thr_upper_norm, thr_upper_norm], [0, 1], 'm:')
plt.grid(which='major', linestyle='-', linewidth='0.5', color='gray')
plt.grid(which='minor', linestyle=':', linewidth='0.5', color='gray')
plt.xlabel('Score')
plt.ylabel('CDF')
legends = ['Cuviers_100', 'Gervais_100']
plt.legend(legends)
plt.show()
"""




