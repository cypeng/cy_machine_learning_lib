'''
@Author: CY Peng
@Date: 2019-10-31
@LastEditTime: 2019-11-5

DataVer  - Author - Note
2019/11/1     CY    Initialization
2019/11/5     CY    Plot Update

Ref.
https://matplotlib.org/3.1.0/gallery/pyplots/auto_subplots_adjust.html, auto subplot adjust
https://github.com/matplotlib/matplotlib/issues/8997, auto subplot adjust
https://pandas.pydata.org/pandas-docs/version/0.23.4/generated/pandas.DataFrame.plot.html, pandas plot
https://www.analyticsvidhya.com/blog/2018/06/comprehensive-guide-recommendation-engine-python/, Recommendation System Engine
'''
# Common Lib
import CommonUtility as CU
import numpy as np

from grid_strategy import strategies
from grid_strategy.strategies import SquareStrategy

# Statistics
import pandas as pd
#import pandas_profiling as pp # statistics report
from statsmodels.formula.api import ols  # statistics
import statsmodels.api as sm # statistics

# Data Visualization
import os
import seaborn as sns # data visualization
import matplotlib.pyplot as plt # data visualization
import plotly.express as px # data visualization
import plotly.graph_objs as go # go object plot

#_output_folder = "EDA_output"

# Step 1 Check Data Profile
# Basic Data File Profile
def df_data_check(df_data, outputFolder, filename):
    #global _output_folder

    print('1. Data File Shape:', df_data.shape)
    missing_val_count_by_column = (df_data.isnull().sum())
    missing_val_count_by_column.to_csv(os.path.join(outputFolder, "B1_"+filename + "_missing_val_count.csv"), na_rep='NULL')
    missing_val_percentage_by_column = (df_data.isnull().sum())/len(df_data)
    missing_val_percentage_by_column.to_csv(os.path.join(outputFolder, "B1_"+filename + "_missing_val_percentage.csv"), na_rep='NULL')
    print('2. Data File Missing Value:', missing_val_count_by_column[missing_val_count_by_column > 0])
    #isna_array_check = df_data.isna().any().any()
    check_data_nan = df_data.isna().sum()
    check_data_nan.to_csv(os.path.join(outputFolder, "B2_"+filename + "_check_data_nan.csv"), na_rep='NULL')
    #pd.DataFrame(isna_array_check).to_csv(os.path.join(_output_folder, filename + "_isna_array_check.csv"), na_rep='NULL')
    print("3. None Values in File: {}".format(check_data_nan))
    print('4. Data File Variables Information:')
    data_info = df_data.info()
    #data_info.to_csv(os.path.join(_output_folder, filename + "_variables_info.csv"), na_rep='NULL')
    df_data.dtypes.to_csv(os.path.join(outputFolder, "B3_"+filename + "_dtypes.csv"), na_rep='NULL')
    print(data_info)
    print('5. Data File Variables Unique Number:')
    nunique_data = df_data.nunique()
    nunique_data.to_csv(os.path.join(outputFolder, "B4_"+filename + "_nunique_data.csv"), na_rep='NULL')
    print(nunique_data)
    print('6. Look into the Some Data for Head!')
    head_data = df_data.head().T
    head_data.to_csv(os.path.join(outputFolder, "B5_"+filename + "_head_data_check.csv"), na_rep='NULL')
    print(head_data)
    print('7. Look into the Some Data for Tail!')
    tail_data = df_data.tail().T
    tail_data.to_csv(os.path.join(outputFolder, "B6_"+filename + "_tail_data_check.csv"), na_rep='NULL')
    print(tail_data)

# Data Type Check
def df_data_type_check(df_data, outputFolder, filename):
    f = open(os.path.join(outputFolder, "B8_"+filename + "_type_detail.csv"), 'w')
    num_col = [c for i, c in enumerate(df_data.columns) if df_data.dtypes[i] in [np.number]]
    f.write('Number Data: {}\n'.format(num_col))
    cat_col = [c for i, c in enumerate(df_data.columns) if df_data.dtypes[i] in [np.object]]
    f.write('Object Data: {}\n'.format(cat_col))

    f.close()

def df_data_numerical_type_describe(df_data, outputFolder, filename):
    #global _output_folder
    CU.createOutputFolder(outputFolder)

    describe_data = df_data.describe(include = [np.number])
    print("Basic Information:", describe_data.T)
    #print("Data Type Information:", df_data.dtypes)
    describe_data.to_csv(os.path.join(outputFolder,"B7_"+filename+"_num_type_describe.csv"), na_rep='NULL')
    return describe_data

def df_data_percentage_description(df_data, target_name, outputFolder, filename):
    for type_name in df_data.columns:
        try:
            type_target_output = pd.crosstab(df_data[type_name], df_data[target_name], normalize = "index")
            #type_target_output = df_data.groupby(type_name)[target_name].value_counts(normalize=True).unstack()#.fillna(0)
            #df_group = df_data.groupby(type_name)[target_name].describe()
            #print(df_group)
            #type_target_output = df_group.value_counts(normalize=True).unstack()
            #type_target.loc['Row_sum'] = type_target.apply(lambda x: x.sum())
            type_target_output.loc['Row_sum_percentage'] = type_target_output.apply(lambda x: x.sum()/len(x))
            type_target_output['Col_sum'] = type_target_output.apply(lambda x: x.sum(), axis=1)
            type_target_output.to_csv(os.path.join(outputFolder, filename + "_group_by_"+ type_name+'_focus_'+target_name + "_type_count.csv"), na_rep='NULL')
        except:
            pass
        type_target = pd.DataFrame((df_data[type_name].value_counts()/len(df_data)))
        type_target.to_csv(os.path.join(outputFolder, filename + "_group_by_"+ type_name + "_type_count.csv"), na_rep='NULL')

# Basic Data Type Check
def df_datatype_check(df_data, type_list):
    for type_name in type_list:
        try:
            temp = df_data[type_name]
        except:
            print('Some Issues in Data Type: {}!'.format(type_name))
            pass
    print('Data Type Check OK!')

# ANOVA test
def df_data_ANOVA_test(df_data, type_list, outputFolder, filename):
    count = 1
    for idx in range(0, len(type_list)-1):
        for jdx in range(idx+1, len(type_list)):
            type_name1 = type_list[idx]
            type_name2 = type_list[jdx]
            if type_name1 != type_name2:
                mod = ols(type_name1 + '~' + type_name2, data=df_data).fit()
                # do type 2 anova
                aov_table = sm.stats.anova_lm(mod, typ=2)
                aov_table.to_csv(os.path.join(outputFolder,"ANV"+str(count)+"_"+filename+"_"+str(type_name1)+"_"+str(type_name2)+"_aov.csv"), na_rep='NULL')
                print('ANOVA Table:'+type_name1+' v.s. ' + type_name2)
                print('-'*100)
                print(aov_table)
                print(' '*100)
                count += 1

# Step 2 Check Data Distribution & Count
# Figure Plot Usage
def multiplot_strategy(multiplot_no):
    """
    :param multiplot_no:
    :return:

    Exmaple:
    fig = plt.figure(figsize=(10,10))
        for idx, spec in enumerate(specs):
        if (xlabel_unit):
            unit_label = ", " + xlabel_unit[idx]
        else:
            unit_label = ""
        type_name = type_list[idx]
        fig.tight_layout()
        plt.subplot(spec)
        __df_data_fig_plot(df_data,
                           plot_type,
                           plot_setting,
                           **kwargs)
    fig.savefig(os.path.join(_output_folder, filename + "_" + plot_type +".svg"),
                dpi=100,
                bbox_inches='tight')
    plt.close("all")
    """
    plotSquareStrategy = SquareStrategy()
    specs = plotSquareStrategy.get_grid(multiplot_no)
    return specs

def multiplot_save(fig, outputFolder, plot_filename):
    fig.savefig(os.path.join(outputFolder, plot_filename), dpi=100, bbox_inches='tight')

def df_data_fig_plot(df_data, plot_type, plot_setting, **kwargs):
    """
    :param df_data:
    :param plot_type:
    :param kwargs:
           Most Plots:
           x=x_data_name, y=y_data_name, hue=hue_data_name, row=row_name, col=col_name
           Excluded: dist plot
           df_data[x_data_name], df_data[y_data_name], label = label_name
    :return:
    """
    # Relational Plots
    if ('Rel'== plot_type):
        sns.relplot(data=df_data, **kwargs)
    elif ('Scatter' == plot_type):
        sns.scatterplot(data=df_data, **kwargs)
    elif ('Line' == plot_type):
        sns.lineplot(data=df_data, **kwargs)
    # Categorical Plots
    elif ('Cat' == plot_type):
        sns.catplot(data=df_data, **kwargs)
    elif ('Strip' == plot_type):
        sns.stripplot(data=df_data, **kwargs)
    elif ('Swarm' == plot_type):
        sns.swarmplot(data=df_data, **kwargs)
    elif ('Box' == plot_type):
        sns.boxplot(data=df_data, **kwargs)
    elif ('Violin' == plot_type):
        sns.violinplot(data=df_data, **kwargs)
    elif ('Boxen' == plot_type):
        sns.boxenplot(data=df_data, **kwargs)
    elif ('Point' == plot_type):
        sns.pointplot(data=df_data, **kwargs)
    elif ('Bar' == plot_type):
        ax = sns.barplot(data=df_data, **kwargs)
        ax.set_xticklabels(ax.get_xticklabels(), rotation=90)
    elif ('Count' == plot_type):
        ax = sns.countplot(data=df_data, **kwargs)
        ax.set_xticklabels(ax.get_xticklabels(), rotation=90)
    elif ('Radar' == plot_type):
        __pure_radar_plot__(data = df_data, **kwargs)
    # Distribution Plots
    elif ('Joint' == plot_type):
        sns.jointplot(data=df_data, **kwargs)
    elif ('Distribution' == plot_type):
        if (plot_setting['y']):
            sns.distplot(df_data[plot_setting['x']], df_data[plot_setting['y']], kde=False, **kwargs)
        else:
            sns.distplot(df_data[plot_setting['x']], kde=False, **kwargs)
    elif ('Kde' == plot_type):
        sns.kdeplot(data=df_data, **kwargs)

    plt.title(plot_setting['title'])
    plt.ylabel(plot_setting['ylabel'])
    plt.xlabel(plot_setting['xlabel'])
    plt.xticks(rotation=90)
    #plt.legend(plot_setting['lengend'])
    plt.grid()

# Multi-Type Plot
def df_data_multi_type_plot(df_data, type_list, target_name, outputFolder, filename, plot_type = None, ylabel_list = ["No of Instances"], fig_size = (10, 10)):
    specs = multiplot_strategy(len(type_list))
    fig = plt.figure(figsize=fig_size)

    # Plot Setting Initialization
    plot_setting = {}
    kwargs = {}
    for idx, spec in enumerate(specs):
        type_name = type_list[idx]
        fig.tight_layout()
        
        # Plot Setting
        if (target_name == None):   
           plot_setting['xlabel'] = type_name  
        else:
           plot_setting['xlabel'] = "{} versus {}".format(type_name, target_name) 
        
        if plot_type == 'Distribution':
           plot_setting['x'] = type_name
           plot_setting['y'] = None
        elif (plot_type == 'Count'):
           kwargs['x'] = type_name
           kwargs['hue'] = target_name
        elif (plot_type == 'Box'):
           kwargs['y'] = type_name
           kwargs['x'] = target_name

        plot_setting['title'] = "{} {} Plot".format(type_name, plot_type)
        plot_setting['ylabel'] = None
        if (ylabel_list):
            if (len(ylabel_list) > 1):
                plot_setting['ylabel'] = ylabel_list[idx]
            else:
                plot_setting['ylabel'] = ylabel_list[0]

        # Figure Plot
        plt.subplot(spec)
        #print('test', plot_type, kwargs, plot_setting)
        df_data_fig_plot(df_data, plot_type, plot_setting, **kwargs)
    if target_name == None:
       target = 'None'
    multiplot_save(fig, outputFolder, "{}_multi_type_versus_{}_{}.svg".format(filename, target_name, plot_type))
    plt.close("all")
    
def df_data_multi_target_plot(df_data, type_name, target_list, outputFolder, filename, plot_type = None, ylabel_list = ["No of Instances"], fig_size = (10, 10)):
    specs = multiplot_strategy(len(target_list))
    fig = plt.figure(figsize=fig_size)

    # Plot Setting Initialization
    plot_setting = {}
    kwargs = {}
    for idx, spec in enumerate(specs):
        target_name = target_list[idx]
        fig.tight_layout()
        
        # Plot Setting
        plot_setting['xlabel'] = type_name  
        if plot_type == 'Distribution':
           plot_setting['x'] = type_name
           plot_setting['y'] = None
        elif (plot_type == 'Count'):
           kwargs['x'] = type_name
           kwargs['hue'] = target_name
        elif (plot_type == 'Box'):
           kwargs['y'] = type_name
           kwargs['x'] = target_name
        plot_setting['title'] = "{} {} Plot".format(type_name, plot_type)
        plot_setting['ylabel'] = None
        if (ylabel_list):
            if (len(ylabel_list) > 1):
                plot_setting['ylabel'] = ylabel_list[idx]
            else:
                plot_setting['ylabel'] = ylabel_list[0]

        # Figure Plot
        plt.subplot(spec)
        #print('test', plot_type, kwargs, plot_setting)
        df_data_fig_plot(df_data, plot_type, plot_setting, **kwargs)
    if target_name == None:
       target = 'None'
    multiplot_save(fig, outputFolder, "{}_{}_versus_multi_type_{}.svg".format(filename, type_name, plot_type))
    plt.close("all")

def df_data_multi_type_percentage_value_bar_plot(df_data, type_list, target_name, outputFolder, filename, ylabel_list = None, fig_size = (10, 10)):
    specs = multiplot_strategy(len(type_list))
    fig = plt.figure(figsize=fig_size)
    plot_setting = {}
    if ylabel_list == None:
       plot_setting['ylabel'] = "No of Instances"
    for idx, spec in enumerate(specs):
        type_name = type_list[idx]
        if (ylabel_list):
            plot_setting['ylabel'] = ylabel_list[idx]
        fig.tight_layout()
        ax = fig.add_subplot(spec)
        props = df_data.groupby(type_name)[target_name].value_counts(normalize=True).unstack()
        props.plot(kind='bar', stacked='True', ax=ax)
        plt.grid()
        plt.xlabel(type_name)
        plt.ylabel('Percentage, %')
        plt.title("{} Percentage Bar Plot".format(type_name))
    plt.savefig(os.path.join(outputFolder, filename + "_per_bar.svg"), dpi=100, bbox_inches='tight')
    plt.close("all")

"""
def df_data_multi_type_bar_plot(df_data, type_list, target_list, outputFolder, filename, ylabel_list = None):
    specs = multiplot_strategy(len(type_list)*len(target_list))
    fig = plt.figure(figsize=(10, 10))
    plot_setting = {}
    kwargs = {}
    plot_setting['title'] = "Bar Plot"
    if ylabel_list == None:
        plot_setting['ylabel'] = "No of Instances"
    for idx, spec in enumerate(specs):
        for target_name in target_list:
            type_name = type_list[idx]
            fig.tight_layout()
            plot_setting['xlabel'] = type_name
            #kwargs['label'] = type_name
            kwargs['x'] = type_name
            if target_name != type_name:
                kwargs['y'] = target_name
            else:
                kwargs['y'] = None
            if (ylabel_list):
                plot_setting['ylabel'] = ylabel_list[idx]
            plt.subplot(spec)
            df_data_fig_plot(df_data, 'bar', plot_setting, **kwargs)
    multiplot_save(fig, outputFolder, filename + "_bar.svg")
    plt.close("all")

def df_data_multi_type_box_plot(df_data, type_list, target_list, outputFolder, filename, ylabel_list = None):
    specs = multiplot_strategy(len(type_list)*len(target_list))
    fig = plt.figure(figsize=(10, 10))
    plot_setting = {}
    kwargs = {}
    if ylabel_list == None:
       plot_setting['ylabel'] = "No of Instances"
    for idx, spec in enumerate(specs):
        for target_name in target_list:
            type_name = type_list[idx]
            fig.tight_layout()
            plot_setting['title'] = "{} Box Plot".format(type_name)
            plot_setting['xlabel'] = type_name
            #kwargs['label'] = type_name
            kwargs['y'] = type_name
            kwargs['x'] = target_name
            if (ylabel_list):
                plot_setting['ylabel'] = ylabel_list[idx]
            plt.subplot(spec)
            df_data_fig_plot(df_data, 'box', plot_setting, **kwargs)
    multiplot_save(fig, outputFolder, filename + "_box.svg")
    plt.close("all")

def df_data_multi_type_radar_plot(df_data, type_list, target_list, outputFolder, filename):
    specs = multiplot_strategy(len(type_list)*len(target_list))
    fig = plt.figure(figsize=(10, 10))
    plot_setting = {}
    kwargs = {}
    plot_setting['title'] = "Radar Plot"
    #plot_setting['ylabel'] = "No of Instances"
    for idx, spec in enumerate(specs):
        type_name = type_list[idx]
        fig.tight_layout()
        #plot_setting['xlabel'] = type_name
        #kwargs['label'] = type_name
        kwargs['index'] = type_name
        if target_name != type_name:
            kwargs['col'] = target_name
        else:
            kwargs['col'] = None
        plt.subplot(spec)
        df_data_fig_plot(df_data, 'radar', plot_setting, **kwargs)
    multiplot_save(fig, outputFolder, filename + "_radar.svg")
    plt.close("all")

def df_data_multi_type_count_plot(df_data, type_list, target_list, outputFolder, filename, ylabel_list = None):
    specs = multiplot_strategy(len(type_list)*len(target_list))
    fig = plt.figure(figsize=(10, 10))
    plot_setting = {}
    kwargs = {}
    if ylabel_list == None:
       plot_setting['ylabel'] = "No of Instances"
    for idx, spec in enumerate(specs):
        for target_name in target_list:
            type_name = type_list[idx]
            if (ylabel_list):
                plot_setting['ylabel'] = ylabel_list[idx]
            plot_setting['title'] = "{} Count Plot".format(type_name)
            fig.tight_layout()
            plot_setting['xlabel'] = type_name
            kwargs['x'] = type_name
            kwargs['hue'] = target_name
            plt.subplot(spec)
            df_data_fig_plot(df_data, 'count', plot_setting, **kwargs)
    multiplot_save(fig, outputFolder, filename + "_count.svg")
    plt.close("all")

def df_data_multi_type_linehistogram_plot(df_data, type_list, outputFolder, filename, ylabel_list = None):
    specs = multiplot_strategy(len(type_list))
    fig = plt.figure(figsize=(10, 10))
    plt.title('Numerical Data Distribution Plot')
    plot_setting = {}
    if ylabel_list == None:
       plot_setting['ylabel'] = "No of Instances, %"
    for idx, spec in enumerate(specs):
        kwargs = {}
        if (ylabel_list):
            plot_setting['ylabel'] = ylabel_list[idx]
        type_name = type_list[idx]
        plot_setting['title'] = "{} Distribution".format(type_name)
        fig.tight_layout()
        plot_setting['xlabel'] = type_name
        plot_setting['x'] = type_name
        plot_setting['y'] = None
        kwargs['label'] = type_name
        plt.subplot(spec)
        df_data_fig_plot(df_data, 'dist', plot_setting, **kwargs)
    multiplot_save(fig, outputFolder, filename + "_dist.svg")
    plt.close("all")
"""

# Line Histogram Plot
def df_data_linehistogram_plot(df_data, type_list, outputFolder, filename):
    for type_name in type_list:
        fig = plt.figure()
        sns.distplot(df_data[type_name], label=type_name)
        plt.title('Distribution Plot for ' + type_name)
        plt.ylabel('No of Instances')
        plt.xlabel(type_name)
        plt.grid()
        fig.savefig(os.path.join(outputFolder, filename+"_"+type_name+"_dist.svg"), dpi=100, bbox_inches='tight')
        plt.close("all")

# Bar Plot
def df_data_bar_plot(df_data, type_list, outputFolder, filename, xlabel_unit = None):
    for type_name in type_list:
        fig = plt.figure()
        sns.barplot(df_data[type_name], label=type_name)
        plt.title('Distribution Plot for ' + type_name)
        plt.ylabel('No of instances')
        plt.grid()
        fig.savefig(os.path.join(outputFolder, filename+"_"+type_name + "_bar.svg"), dpi=100, bbox_inches='tight')
        plt.close("all")

# Box Plot
def df_data_box_plot(df_data, type_list, target_list, outputFolder, filename):
    for type_name in type_list:
        for target_name in target_list:
            fig = plt.figure()
            #plt.subplot(121)
            sns.boxplot(y= df_data[type_name], hue=None)
            plt.ylabel('No of instances: ' + type_name)
            plt.title('Box Plot for ' + type_name)
            #plt.subplot(122)
            plt.figure()
            sns.catplot(data = df_data, x=target_name, y=type_name, kind="boxen")
            plt.ylabel('No of instances: ' + type_name)
            plt.title('Box Plot (Versus Target Type) for ' + type_name)
            plt.grid()
            fig.savefig(os.path.join(outputFolder, filename+"_"+type_name + "_" + target_name+"_box.svg"), dpi=100, bbox_inches='tight')
            plt.close("all")

def df_data_scatter_plot(df_data, type_list, target_list, outputFolder, filename):
    for idx in range(0, len(type_list) - 1):
        type_name1 = df_data.columns[idx]
        for jdx in range(idx + 1, len(df_data.columns)):
            type_name2 = df_data.columns[jdx]
            for target_name in target_list:
                fig = plt.figure()
                sns.scatterplot(data=df_data, x=type_name1, y=type_name2, hue=target_name)
                # sns.regplot(data = data, x=type_name1, y=type_name2,)
                plt.title('Scatter Plot for ' + type_name1 + type_name2)
                plt.grid()
                fig.savefig(os.path.join(outputFolder, filename + "_" + type_name1 + "_" + type_name2 + "_"+ target_name + "_scatter.svg"),
                            dpi=100, bbox_inches='tight')
                plt.close("all")

# Count Plot
def df_data_count_plot(df_data, type_list, target_list, outputFolder, filename):
    for type_name in type_list:
        for target_name in target_list:
            fig = plt.figure()
            sns.countplot(x= type_name, hue=target_name, data=df_data)
            #face_plt = sns.FacetGrid(data, col=target_name, height=4, aspect=.5)
            #face_plt.map(sns.countplot, type_name)
            fig.savefig(os.path.join(outputFolder,
                                     filename + "_" + type_name + "_" + target_name + "_count.svg"),
                        dpi=100, bbox_inches='tight')
            plt.close("all")

# Pure Radar Plot
def __pure_radar_plot__(data = None, index = None, col = None):
    crosstab_data = pd.crosstab(index= data[index], columns=data[col])
    target_name_list = crosstab_data.columns
    type_name_list = crosstab_data.index
    cat = [index + ' ' + str(a) for a in target_name_list]
    layout = go.Layout(
        polar=dict(
            radialaxis=dict(
                visible=True,
                range=[0, 3000]
            )),
        title=go.layout.Title(
            text="Radar Plot: " + index + " v.s. " + col),
        showlegend=True
    )
    fig = go.Figure(layout=layout)
    for idx in range(0, len(type_name_list)):
        print(crosstab_data)
        print(crosstab_data.loc[idx].values)
        print(list(crosstab_data.loc[idx].values))
        #values = list(crosstab_data.loc[idx].values)  # /(crosstab_data.loc[idx].sum())
        #print(type(values))
        fig.add_trace(go.Scatterpolar(
            r= list(crosstab_data.loc[idx].values),
            theta=cat,
            fill='toself',
            name=str(idx)
        ))
    # fig.show()
    #fig.savefig(os.path.join(_output_folder,
    #                         filename + "_" + type_name + "_" + target_name + "_radar.svg"),
    #            dpi=100, bbox_inches='tight')
    #plt.close("all")

# Radar plot
def df_data_radar_plot(df_data, type_list, target_list, outputFolder, filename):
    for type_name in type_list:
        for target_name in target_list:
            fig = plt.figure()
            crosstab_data = pd.crosstab(index=df_data[type_name], columns=df_data[target_name])
            target_name_list = crosstab_data.columns
            type_name_list = crosstab_data.index
            cat = [target_name + ' ' + str(a) for a in target_name_list]
            layout = go.Layout(
                polar=dict(
                    radialaxis=dict(
                        visible=True,
                        range=[0, 3000]
                    )),
                title=go.layout.Title(
                    text="Radar Plot: " + type_name + " v.s. " + target_name),
                showlegend=True
            )
            fig = go.Figure(layout=layout)
            for idx in range(0, len(type_name_list)):
                values = list(crosstab_data.loc[idx].values)  # /(crosstab_data.loc[idx].sum())
                print(type(values))
                fig.add_trace(go.Scatterpolar(
                    r=values,
                    theta=cat,
                    fill='toself',
                    name=str(idx)
                ))
            #fig.show()
            fig.savefig(os.path.join(outputFolder,
                                     filename + "_" + type_name + "_" + target_name + "_radar.svg"),
                        dpi=100, bbox_inches='tight')
            plt.close("all")

# Pair Plot
def df_data_numerical_type_pairscatter_plot(df_data, type_list, target_name, outputFolder, filename):
    #plt.title('Pair Scatter Plot Versus Target Type')
    #sns_plot = sns.PairGrid(data= df_data, vars = type_list, hue= target_name)
    #sns_plot.map_diag(sns.distplot, hist=False) #, bins = 10, edgecolor =  'k', color = 'darkred'
    #sns_plot.map_lower(sns.scatterplot)
    #sns_plot.map_upper(sns.kdeplot)
    sns_plot = sns.pairplot(data=df_data, vars=type_list, hue=target_name, diag_kind='kde') #vars=type_list,  hue=target_name, ,diag_kind='kde'
    sns_plot.savefig(os.path.join(outputFolder,filename + "_pairplot.svg"))
    sns_plot.savefig(os.path.join(outputFolder, filename + "_pairplot.png"))
    #plt.grid()

def df_data_numerical_type_correlation_heatmap(df_data, type_list, outputFolder, filename):
    plt.figure()
    colormap = plt.cm.RdBu
    plt.title('Linear Method for Correlaion')
    sns_plot = sns.heatmap(df_data.loc[:, type_list].corr(),
                           cmap=colormap,
                           square=True,
                           linecolor='white',
                           annot=True,
                           vmax=.3) #linewidths=0.1, vmax=1.0,
                           #square=True, cmap=colormap, linecolor='white', annot=True)
    sns_plot.figure.savefig(os.path.join(outputFolder, filename + '_linear_correlation_map.svg'))
    sns_plot.figure.savefig(os.path.join(outputFolder, filename + '_linear_correlation_map.png'))
    #fig.savefig(os.path.join(_output_folder, filename + "LinearCorrelationMethod.svg"), dpi=100, bbox_inches='tight')

    for method_type in ["pearson", "kendall", "spearman"]:
        plt.figure()
        colormap = plt.cm.RdBu
        plt.title('{} Method for Correlaion'.format(method_type))
        sns_plot = sns.heatmap(df_data.loc[:, type_list].corr(method=method_type),
                               cmap=colormap,
                               square=True,
                               linecolor='white',
                               annot=True,
                               vmax=.3) #linewidths=0.1, vmax=1.0,
        #square=True, cmap=colormap, linecolor='white', annot=True)

        sns_plot.figure.savefig(os.path.join(outputFolder, filename + '_'+method_type +'_correlation_map.svg'))
        sns_plot.figure.savefig(os.path.join(outputFolder, filename + '_'+method_type + '_correlation_map.png'))
        #sns_plot.figure.savefig(os.path.join(output_folder,filename+'_correlation_map.svg'))
        #fig.savefig(os.path.join(_output_folder, type_name + ".svg"), dpi=100, bbox_inches='tight')
    plt.close("all")

"""
# HTML Version
def df_data_ProfileReport(df_data, type_list):
    return pp.ProfileReport(df_data[type_list])
"""