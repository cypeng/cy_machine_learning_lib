'''
@Author: CY Peng
@Date: 2019-07-28
@LastEditTime: 2019-07-28

Ref.
https://zhuanlan.zhihu.com/p/40154620

'''
import os
import pickle
import pandas as pd
import numpy as np
import h5py
import H5FileManager as H5FM
import CommonUtility as CU
import math

import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.naive_bayes import GaussianNB, BernoulliNB
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV, PassiveAggressiveClassifier, RidgeClassifierCV, SGDClassifier, Perceptron
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.svm import SVC, NuSVC, LinearSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier, BaggingClassifier, ExtraTreesClassifier, RandomForestClassifier, GradientBoostingClassifier
from xgboost import XGBClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier, BernoulliRBM

from sklearn.metrics import accuracy_score, log_loss, confusion_matrix, classification_report, roc_curve
from sklearn.model_selection import cross_validate, cross_val_score, learning_curve, GridSearchCV, train_test_split, validation_curve

# https://malishoaib.wordpress.com/2017/09/08/sample-size-estimation-for-machine-learning-models-using-hoeffdings-inequality/
# error = sqrt(-1*math.log(significance_level / 2)/n)
# significance_level: type 1 error for the confusion matrix
def Hoeffding_SampleSize(error, significance_level):
    n = (-1 // (2 * error ** 2)) * math.log(significance_level / 2)
    return n

class ClassifierAnalysis():
    def __init__(self, outer_split_size, inner_fold_obj, outer_fold_obj, output_folder, output_FileName):
        CU.createOutputFolder(output_folder)
        #self.X, self.y = X, y
        self.split_size = outer_split_size
        self.out_fold_val = outer_fold_obj
        self.inner_fold_val = inner_fold_obj
        self.output_folder = output_folder
        self.output_FileName = output_FileName

        # create table to compare classifier metrics
        self.clf_cv_table = None
        self.clf_cv_opt_table = None
        #self.clf_cv_test_size_table = None
        #self.clf_cv_train_score_table = None
        #self.clf_cv_test_score_table = None
        """
        clf_columns = ['Classifier Name', 'Classifier Parameters', 'Classifier Train Accuracy Mean',
                       'Classifier Test Accuracy Mean', 'Classifier Test Accuracy 3*STD']
        self.clf_table = pd.DataFrame(columns=clf_columns)
        """

        self.classifiers = [XGBClassifier(objective='multi:softmax')]
        """
            BaggingClassifier(),
            AdaBoostClassifier(),
            #GradientBoostingClassifier(),
            
            DecisionTreeClassifier(),
            RandomForestClassifier(),
            KNeighborsClassifier(),
            MLPClassifier(),
            GaussianProcessClassifier(),
            LogisticRegression(),
            SVC(probability=True)
        ]
        """


        """
        # create all classifiers
        self.classifiers = [
            # Gaussian Processes
            GaussianProcessClassifier(),

            # Navies Bayes
            BernoulliNB(),
            GaussianNB(),

            # GLM
            Perceptron(),
            LogisticRegression(),
            LogisticRegressionCV(),
            PassiveAggressiveClassifier(),
            RidgeClassifierCV(),
            SGDClassifier(),

            # Discriminant Analysis
            LinearDiscriminantAnalysis(),
            QuadraticDiscriminantAnalysis(),

            # SVM
            SVC(probability=True),
            NuSVC(probability=True),
            LinearSVC(),

            # Ensemble Methods
            # Linear Aggregation - Bagging
            BaggingClassifier(),
            # Weighted Aggregation - Boost
            AdaBoostClassifier(),
            GradientBoostingClassifier(),
            # XGBClassifier
            XGBClassifier(),
            # Nonlinear Aggregation - Decision Tree
            DecisionTreeClassifier(),
            ExtraTreesClassifier(),
            # Bagging + Decision Tree
            RandomForestClassifier(),

            # Nearest Neighbor
            KNeighborsClassifier(),

            # MLP Classifier
            MLPClassifier()
            #BernoulliRBM()
        ]
        """

        self.grid_param = {}
        self.clf_config()

    def clf_config(self):
        grid_n_estimator = [10, 50, 100, 300]
        grid_ratio = [.1, .25, .5, .75, 1.0]
        grid_learn = [.01, .03, .05, .1, .25]
        grid_max_depth = [2, 4, 6, 8, 10, None]
        #grid_min_samples = [5, 10, .03, .05, .10]
        grid_criterion = ['gini', 'entropy']
        grid_bool = [True, False]
        grid_seed = [0]

        # GaussianProcessClassifier
        self.grid_param["GaussianProcessClassifier"] = \
            [{
                # GaussianProcessClassifier
                'max_iter_predict': grid_n_estimator,  # default: 100
                'random_state': grid_seed
            }]

        # Navies Bayes
        self.grid_param["BernoulliNB"] = \
            [{
            # BernoulliNB - http://scikit-learn.org/stable/modules/generated/sklearn.naive_bayes.BernoulliNB.html#sklearn.naive_bayes.BernoulliNB
            'alpha': grid_ratio,  # default: 1.0
            }]
        self.grid_param["GaussianNB"] = [{}]

        # GLM
        self.grid_param["Perceptron"] = \
            [{
                # Perceptron - https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Perceptron.html
                #'alpha ': [0.0001, 0.001, 0.01, 0.1, 1, 10, 100, 1000]  # default: 0.0001
            }]
        self.grid_param["LogisticRegression"] = \
            [{
                # LogisticRegression - https://www.kaggle.com/enespolat/grid-search-with-logistic-regression
                "C": [0.001, 0.01, 0.1, 1, 10, 100, 1000],
                "penalty": ["l1","l2"]
            }]
        self.grid_param["LogisticRegressionCV"] = \
            [{
                # LogisticRegressionCV - http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegressionCV.html#sklearn.linear_model.LogisticRegressionCV
                'fit_intercept': grid_bool,  # default: True
                # 'penalty': ['l1','l2'],
                'solver': ['newton-cg', 'lbfgs', 'liblinear', 'sag', 'saga'],  # default: lbfgs
                'random_state': grid_seed
            }]
        self.grid_param["PassiveAggressiveClassifier"] = \
            [{
                # PassiveAggressiveClassifier - https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.PassiveAggressiveClassifier.html
                "C": [0.001, 0.01, 0.1, 1, 10, 100, 1000]
            }]
        self.grid_param["RidgeClassifierCV"] = \
            [{
                # RidgeClassifierCV - https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.RidgeClassifierCV.html
                #"alphas": [0.001, 0.01, 0.1, 1, 10, 100, 1000]
            }]
        self.grid_param["SGDClassifier"] = \
            [{
                # SGDClassifier -
                "alpha": [0.001, 0.01, 0.1, 1, 10, 100, 1000],
                "penalty": ["l1", "l2"]
            }]

        # Discriminant Analysis
        self.grid_param["LinearDiscriminantAnalysis"] = \
            [{
                # LinearDiscriminantAnalysis - https://scikit-learn.org/stable/modules/generated/sklearn.discriminant_analysis.LinearDiscriminantAnalysis.html
            }]
        self.grid_param["QuadraticDiscriminantAnalysis"] = \
            [{
                # QuadraticDiscriminantAnalysis - https: // scikit - learn.org / stable / modules / generated / sklearn.discriminant_analysis.QuadraticDiscriminantAnalysis.html
            }]

        # SVM
        self.grid_param["SVC"] = \
            [{
                # SVC - http://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html#sklearn.svm.SVC
                # http://blog.hackerearth.com/simple-tutorial-svm-parameter-tuning-python-r
                'kernel': ['linear'], #'kernel': ['linear', 'poly', 'rbf', 'sigmoid'],
                'C': [1, 2, 3, 4, 5],  # default=1.0
                'gamma': grid_ratio,  # edfault: auto
                #'decision_function_shape': ['ovo', 'ovr'],  # default:ovr
                'probability': [True],
                'random_state': grid_seed
            }]
        self.grid_param["NuSVC"] = \
            [{
                # NuSVC - https://scikit-learn.org/stable/modules/generated/sklearn.svm.NuSVC.html
                'kernel': ['linear', 'poly', 'rbf', 'sigmoid'], # default=poly
                'degree ': [1, 2, 3, 4, 5],  # default=3
                'gamma': grid_ratio,  # dfault: auto
                'decision_function_shape': ['ovo', 'ovr'],  # default:ovr
                'probability': [True],
                'random_state': grid_seed
            }]
        self.grid_param["LinearSVC"] = \
            [{
                # LinearSVC - https://scikit-learn.org/stable/modules/generated/sklearn.svm.LinearSVC.html
                'C': [1, 2, 3, 4, 5],  # default=1.0
                'random_state': grid_seed,
                "penalty": ["l1", "l2"],
                "loss": ["hinge", "squared_hinge"]
            }]

        # Ensemble Methods
        # Linear Aggregation - Bagging
        self.grid_param["BaggingClassifier"] = \
            [{
                # BaggingClassifier - http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.BaggingClassifier.html#sklearn.ensemble.BaggingClassifier
                'n_estimators': grid_n_estimator,  # default=10
                'max_samples': grid_ratio,  # default=1.0
                'random_state': grid_seed
            }]
        # Weighted Aggregation - Boost
        self.grid_param["AdaBoostClassifier"] = \
            [{
                # AdaBoostClassifier - http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.AdaBoostClassifier.html
                'n_estimators': grid_n_estimator,  # default=50
                'learning_rate': grid_learn,  # default=1
                # 'algorithm': ['SAMME', 'SAMME.R'], #default=’SAMME.R
                'random_state': grid_seed
            }]
        self.grid_param["GradientBoostingClassifier"] = \
            [{
                # GradientBoostingClassifier - http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.GradientBoostingClassifier.html#sklearn.ensemble.GradientBoostingClassifier
                # 'loss': ['deviance', 'exponential'], #default=’deviance’
                'learning_rate': [.05],
                # default=0.1 -- 12/31/17 set to reduce runtime -- The best parameter for GradientBoostingClassifier is {'learning_rate': 0.05, 'max_depth': 2, 'n_estimators': 300, 'random_state': 0} with a runtime of 264.45 seconds.
                'n_estimators': [300],
                # default=100 -- 12/31/17 set to reduce runtime -- The best parameter for GradientBoostingClassifier is {'learning_rate': 0.05, 'max_depth': 2, 'n_estimators': 300, 'random_state': 0} with a runtime of 264.45 seconds.
                # 'criterion': ['friedman_mse', 'mse', 'mae'], #default=”friedman_mse”
                'max_depth': grid_max_depth,  # default=3
                'random_state': grid_seed
            }]
        # XGBClassifier
        self.grid_param["XGBClassifier"] = \
            [{
                # XGBClassifier - http://xgboost.readthedocs.io/en/latest/parameter.html
                'learning_rate': grid_learn,  # default: .3
                'max_depth': [1, 2, 4, 6, 8, 10],  # default 2
                'n_estimators': grid_n_estimator,
                'seed': grid_seed
            }]
        # Nonlinear Aggregation - Decision Tree
        self.grid_param["DecisionTreeClassifier"] = \
            [{
                # DecisionTreeClassifier - https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html
                'criterion': grid_criterion,  # default=”gini”
                'max_depth': grid_max_depth,  # default=None
                'random_state': grid_seed
            }]
        self.grid_param["ExtraTreesClassifier"] = \
            [{
                # ExtraTreesClassifier - http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.ExtraTreesClassifier.html#sklearn.ensemble.ExtraTreesClassifier
                'n_estimators': grid_n_estimator,  # default=10
                'criterion': grid_criterion,  # default=”gini”
                'max_depth': grid_max_depth,  # default=None
                'random_state': grid_seed
            }]
        # Bagging + Decision Tree
        self.grid_param["RandomForestClassifier"] = \
            [{
                # RandomForestClassifier - http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html#sklearn.ensemble.RandomForestClassifier
                'n_estimators': grid_n_estimator,  # default=10
                'criterion': grid_criterion,  # default=”gini”
                'max_depth': grid_max_depth,  # default=None
                'oob_score': [True],
                # default=False -- 12/31/17 set to reduce runtime -- The best parameter for RandomForestClassifier is {'criterion': 'entropy', 'max_depth': 6, 'n_estimators': 100, 'oob_score': True, 'random_state': 0} with a runtime of 146.35 seconds.
                'random_state': grid_seed
            }]

        # Nearest Neighbor
        self.grid_param["KNeighborsClassifier"] = \
            [{
                # KNeighborsClassifier - http://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html#sklearn.neighbors.KNeighborsClassifier
                'n_neighbors': [1, 2, 3, 4, 5, 6, 7],  # default: 5
                'weights': ['uniform', 'distance'],  # default = ‘uniform’
                'algorithm': ['auto', 'ball_tree', 'kd_tree', 'brute']
            }]

        # MLP Classifier
        self.grid_param["MLPClassifier"] = \
            [{
                # MLPClassifier
                'hidden_layer_sizes': [(50, 50, 50), (50, 100, 50), (100,)],
                'activation': ['tanh', 'relu'],
                'solver': ['sgd', 'adam'],
                'alpha': [0.0001, 0.05],
                'learning_rate': ['constant', 'adaptive'],
            }]
        #print(self.grid_param)

    def model_training_stage(self, X_train, y_train, X_val, y_val, opt_num = 23):
        row_index = 0

        clf_table_dataType = np.dtype([('Classifier Name', h5py.special_dtype(vlen=str)),
                                       ('Classifier Parameters', h5py.special_dtype(vlen=str)),
                                       ('Classifier Train Accuracy Mean', np.float),
                                       ('Classifier Test Accuracy Mean', np.float),
                                       ('Classifier Test Accuracy 3*STD', np.float),
                                       ('Classifier Time', np.float)])
        clf_table = np.zeros((opt_num,), dtype = clf_table_dataType)

        for clf in self.classifiers:
            cv_results = cross_validate(clf, X_train, y_train, cv=self.out_fold_val,
                                        return_train_score=True, return_estimator=True)
            modelname = clf.__class__.__name__
            print(modelname, "training start")
            CU.createOutputFolder(os.path.join(self.output_folder, modelname + "_CV"))
            clf_table['Classifier Name'][row_index] = modelname
            clf_table['Classifier Parameters'][row_index] = str(clf.get_params())
            clf_table['Classifier Train Accuracy Mean'][row_index] = cv_results['train_score'].mean()
            clf_table['Classifier Test Accuracy Mean'][row_index] = cv_results['test_score'].mean()
            clf_table['Classifier Test Accuracy 3*STD'][row_index] = cv_results['test_score'].std()*3
            clf_table['Classifier Time'][row_index] = cv_results['fit_time'].mean()
            cv_count = 0
            for est in cv_results['estimator']:
                pickle.dump(est, open(
                    os.path.join(self.output_folder, modelname + "_CV", modelname + str(cv_count) + "_CV.sav"), 'wb'))
                pickle.dump(est, open(
                    os.path.join(self.output_folder, modelname + "_CV", modelname  + "_CV.sav"), 'wb'))
                cv_count += 1
            row_index+=1
            self.clf_cv_table = clf_table
            self.clf_cv_table = np.sort(self.clf_cv_table, order='Classifier Test Accuracy Mean')
            self.clf_cv_table = self.clf_cv_table[::-1]
            H5FM.Data2H5(self.clf_cv_table,
                         os.path.join(self.output_folder, self.output_FileName),
                         'CLF CV Table', groupName="ML Analysis", dtype=clf_table_dataType)
            df = pd.DataFrame(clf_table)
            df.to_csv(os.path.join(self.output_folder, modelname + "_CV", "clf_table.csv"))
            #np.savetxt(os.path.join(self.output_folder, modelname + "_CV", "clf_table.csv"), clf_table, , fmt='%f, %f, %f, string= %s %s, last number = %f', delimiter=",")
            self.plot_learning_curve(clf, X_train, y_train, "_CV")
            self.validation(os.path.join(self.output_folder, modelname + "_CV"), modelname  + "_CV", X_val, y_val)
            self.clf_score_bar_plot(self.clf_cv_table)

    def model_optimization(self, X_train, y_train, X_val, y_val, opt_num = 23):
        row_index = 0

        clf_table_dataType = np.dtype([('Classifier Name', h5py.special_dtype(vlen=str)),
                                       ('Classifier Parameters', h5py.special_dtype(vlen=str)),
                                       ('Classifier Train Accuracy Mean', np.float),
                                       ('Classifier Test Accuracy Mean', np.float),
                                       ('Classifier Test Accuracy 3*STD', np.float),
                                       ('Classifier Time', np.float)])
        clf_table = np.zeros((opt_num,), dtype=clf_table_dataType)

        for modelname in self.clf_cv_table['Classifier Name']:
            if row_index >= opt_num:
                break
            for clf in self.classifiers:
                clf_modelname = clf.__class__.__name__
                if modelname == clf_modelname:
                    print(modelname, "optimization training start")
                    param = self.grid_param[modelname]
                    # Non_nested parameter search and scoring
                    # grid_search = GridSearchCV(estimator=clf, cv=self.inner_fold_val, param_grid=param)
                    # fit_result = grid_search.fit(self.X, self.y)
                    # non_nested_score = clf_fit.best_score_
                    # best_param = fit_result.best_params_

                    # Nested Cross Validation
                    # Pass the gridSearch estimator to cross_val_score
                    # This will be your required 10 x 5 cvs
                    # 10 for outer cv and 5 for gridSearch's internal CV
                    best_search = GridSearchCV(estimator=clf, param_grid=param, cv=self.inner_fold_val)
                    cv_results = cross_validate(best_search, X_train, y_train, cv=self.out_fold_val,
                                                return_train_score=True,
                                                return_estimator=True)

                    CU.createOutputFolder(os.path.join(self.output_folder, modelname + "_CV_Opt"))
                    clf_table['Classifier Name'][row_index] = modelname
                    clf_table['Classifier Parameters'][row_index] = str(clf.get_params())
                    print(clf.get_params())
                    clf_table['Classifier Train Accuracy Mean'][row_index] = cv_results['train_score'].mean()
                    clf_table['Classifier Test Accuracy Mean'][row_index] = cv_results['test_score'].mean()
                    clf_table['Classifier Test Accuracy 3*STD'][row_index] = cv_results['test_score'].std() * 3
                    clf_table['Classifier Time'][row_index] = cv_results['fit_time'].mean()
                    cv_count = 0
                    for est in cv_results['estimator']:
                        pickle.dump(est, open(
                            os.path.join(self.output_folder, modelname + "_CV_Opt", modelname + str(cv_count) + "_CV_Opt.sav"),
                            'wb'))
                        pickle.dump(est, open(
                            os.path.join(self.output_folder, modelname + "_CV_Opt", modelname + "_CV_Opt.sav"), 'wb'))
                        cv_count += 1
                    row_index += 1
                    self.clf_cv_opt_table = clf_table
                    self.clf_cv_opt_table = np.sort(self.clf_cv_opt_table, order='Classifier Test Accuracy Mean')
                    self.clf_cv_opt_table = self.clf_cv_opt_table[::-1]
                    H5FM.Data2H5(self.clf_cv_opt_table,
                                 os.path.join(self.output_folder, self.output_FileName),
                                 'CLF CV Opt Table', groupName="ML Analysis", dtype=clf_table_dataType)
                    df = pd.DataFrame(clf_table)
                    df.to_csv(os.path.join(self.output_folder, modelname + "_CV_Opt", "clf_opt_table.csv"))
                    df = pd.DataFrame(cv_results)
                    df.to_csv(os.path.join(self.output_folder, modelname + "_CV_Opt", "clf_opt_results.csv"))
                    #np.savetxt(os.path.join(self.output_folder, modelname + "_CV_Opt", "clf_opt_table.csv"), clf_table, fmt='%f', delimiter=",")
                    #np.savetxt(os.path.join(self.output_folder, modelname + "_CV_Opt", "clf_opt_results.csv"), cv_results, fmt='%f', delimiter=",")
                    self.plot_learning_curve(clf, X_train, y_train, "_CV_Opt")
                    self.validation(os.path.join(self.output_folder, modelname + "_CV_Opt"), modelname + "_CV_Opt", X_val,
                                    y_val)


    def model_fine_tuning(self, model_folder, modelname, X, y, param_name = "gamma", param_range =  np.logspace(-6, -1, 5)):
        clf = pickle.load(open(os.path.join(model_folder, modelname + ".sav"), 'rb'))
        train_scores, test_scores = validation_curve(
            clf, X, y, param_name = param_name, param_range=param_range,
            cv = self.out_fold_val, scoring="accuracy", n_jobs=1)
        self.validation_curve_plot(modelname, train_scores, test_scores, param_name = param_name, param_range = param_range)
        plt.savefig(os.path.join(self.output_folder, modelname + "_CV_Opt", modelname +"_validation_curve.png"))
        plt.close()

    def validation(self, model_folder, modelname, X, y):
        loaded_model = pickle.load(open(os.path.join(model_folder, modelname + ".sav"), 'rb'))
        y_pred = loaded_model.predict(X)
        cm = confusion_matrix(y, y_pred, labels=[1, 0])
        # plt.figure(figsize=(6, 6))
        # sns.heatmap(cm, annot=True, fmt='.0f', cmap="confusion matrix", square=True)
        # plt.xlabel('prediction class')
        # plt.ylabel('actual class')
        # plt.title('confusion_matrix')
        clf_report = classification_report(y, y_pred, labels=None, sample_weight=None, digits=2,
                                           output_dict=True)
        df = pd.DataFrame(clf_report).transpose()
        df.to_csv(os.path.join(model_folder, "val_clf_report.csv"))
        df = pd.DataFrame(cm).transpose()
        df.to_csv(os.path.join(model_folder, "val_cm.csv"))

        # np.savetxt(os.path.join(self.output_folder, "clf_report.csv"), clf_report, fmt='%f', delimiter=",")
        # np.savetxt(os.path.join(self.output_folder, clf_modelname, "cm.csv"), cm, fmt='%f', delimiter=",")

    def plot_roc_curve(self, clf, X, y):
        y_pred = clf.fit(X)
        fpr, tpr, _ = roc_curve(y, y_pred)
        #roc_auc = auc(fpr, tpr)

    def plot_learning_curve(self, clf, X, y, tag):
        modelname = clf.__class__.__name__
        train_sizes, train_scores, valid_scores = \
            learning_curve(clf, X, y, cv=self.out_fold_val)
        self.learning_curve_plot(modelname, train_sizes, train_scores, valid_scores)
        # plt.show()
        plt.savefig(os.path.join(self.output_folder, modelname + tag, modelname + tag + ".png"))
        plt.close()
        #for modelname in table['Classifier Name']:
        #    print(modelname)
        #    if plot_count >= opt_num:
        #        break
        #    for clf in self.classifiers:

    def validation_curve_plot(self, modelname, train_scores, test_scores, param_name = "gamma", param_range =  np.logspace(-6, -1, 5)):
        train_scores_mean = np.mean(train_scores, axis=1)
        train_scores_std = np.std(train_scores, axis=1)
        test_scores_mean = np.mean(test_scores, axis=1)
        test_scores_std = np.std(test_scores, axis=1)

        plt.title("Validation Curve with " + modelname)
        plt.xlabel(param_name)
        plt.ylabel("Score")
        plt.ylim(0.0, 1.1)
        lw = 2
        plt.semilogx(param_range, train_scores_mean, label="Training score",
                     color="darkorange", lw=lw)
        plt.fill_between(param_range, train_scores_mean - train_scores_std,
                         train_scores_mean + train_scores_std, alpha=0.2,
                         color="darkorange", lw=lw)
        plt.semilogx(param_range, test_scores_mean, label="Cross-validation score",
                     color="navy", lw=lw)
        plt.fill_between(param_range, test_scores_mean - test_scores_std,
                         test_scores_mean + test_scores_std, alpha=0.2,
                         color="navy", lw=lw)
        plt.legend(loc="best")
        return plt

    def learning_curve_plot(self, modelname, train_sizes, train_scores, valid_scores):
        train_scores_mean = np.mean(train_scores, axis=1)
        train_scores_std = np.std(train_scores, axis=1)
        test_scores_mean = np.mean(valid_scores, axis=1)
        test_scores_std = np.std(valid_scores, axis=1)

        plt.figure()
        # box-like grid
        plt.grid()

        # plot the std deviation as a transparent range at each training set size
        plt.fill_between(train_sizes, train_scores_mean - train_scores_std, train_scores_mean + train_scores_std,
                         alpha=0.1, color="r")
        plt.fill_between(train_sizes, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std,
                         alpha=0.1, color="g")

        # plot the average training and test score lines at each training set size
        plt.plot(train_sizes, train_scores_mean, 'o-', color="r", label="Training score")
        plt.plot(train_sizes, test_scores_mean, 'o-', color="g", label="Cross-validation score")

        plt.title(modelname)
        plt.legend(loc="best", labels=['Training score', 'Cross-validation score'])
        plt.xlabel("Training examples")
        plt.ylabel("Score")
        plt.gca().invert_yaxis()

        # sizes the window for readability and displays the plot
        # shows error from 0 to 1.1
        plt.ylim(-.1, 1.1)
        return plt

    def clf_score_bar_plot(self, clf_table, file_name = 'cv_classfier.png'):
        log_cols = ["Classifier", "Accuracy"]
        log = pd.DataFrame(columns=log_cols)
        for idx in range(0, len(clf_table)):
            log_entry = pd.DataFrame([[clf_table['Classifier Name'][idx], clf_table['Classifier Test Accuracy Mean'][idx]]], columns=log_cols)
            log = log.append(log_entry)

        plt.xlabel('Accuracy')
        plt.title('Classifier Accuracy')

        sns.set_color_codes("muted")
        sns_plot = sns.barplot(x='Accuracy', y='Classifier', data=log, color="b")
        sns_plot.figure.savefig(os.path.join(self.output_folder, file_name))
        plt.close()

    def get_confusion_matrix(self, X, y, model_folder, modelname):
        #loaded_model = pickle.load(open("RandomForestClassifier9_CV.sav", 'rb'))
        loaded_model = pickle.load(open(os.path.join(model_folder, modelname + ".sav"), 'rb'))
        #X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.33, random_state = 42)
        #clf = loaded_model.fit(X_train, y_train)
        y_pred = loaded_model.predict(X)
        cm = confusion_matrix(y, y_pred, labels=[1, 0])
        #plt.figure(figsize=(6, 6))
        #sns.heatmap(cm, annot=True, fmt='.0f', cmap="confusion matrix", square=True)
        #plt.xlabel('prediction class')
        #plt.ylabel('actual class')
        #plt.title('confusion_matrix')
        clf_report = classification_report(y, y_pred, labels=None, sample_weight=None, digits=2, output_dict=True)
        df = pd.DataFrame(clf_report).transpose()
        df.to_csv(os.path.join(model_folder, "verify_clf_report.csv"))
        df = pd.DataFrame(cm).transpose()
        df.to_csv(os.path.join(model_folder, "verify_cm.csv"))
        print(cm)
        print(clf_report)
