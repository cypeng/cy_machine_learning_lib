'''
@Author: CY Peng
@Date: 2020-01-15
@LastEditTime: 2020-01-15

DataVer  - Author - Note
2020/01/15   CY    Initialization
'''
import re
import numpy as np
import pandas as pd
import pickle
import shutil
import glob
import os
import matplotlib.pyplot as plt # data visualization

import H5FileManager as H5FM
import CommonUtility as CU
import regModelCreatorHelper as MCH
import fbprophetModelCreatorHelper as fbMCH
import tsModelCreatorHelper as tsMCH
import ModelEvaluation as ME
import ExploratoryDataAnalysis as EDA

output_folder = "model_organization"
h5file_count = 1

def read_data(filename):
    data = pd.read_csv(filename+".csv")
    #data = data.fillna(value="unknown")
    return data

def read_val_data(folder_path):
    df_x_val = read_data(os.path.join(folder_path, 'x_val'))#.iloc[:, 1:]
    df_y_val = read_data(os.path.join(folder_path, 'y_val'))#.iloc[:, 1:]
    return df_x_val, df_y_val

def find_extension_name(folder_path, extension_name):
    return glob.glob(os.path.join(folder_path, extension_name))

def copy_h5_file(h5_file, output_path):
    global h5file_count

    organization_file = os.path.join(output_path, "MultiModelAnalysis.h5")

    # Copy Dataset to Organization File
    datasets_name = H5FM.H5datasets(h5_file, groupName = "ML Analysis")
    for dataset_name in datasets_name:
        Data = H5FM.H52Data(h5_file,  dataset_name, groupName = "ML Analysis")
        if ("REG Nested CV Table" == dataset_name):
            dataset_name = dataset_name + "_"+str(h5file_count)
            h5file_count += 1
        H5FM.Data2H5(Data, organization_file, dataset_name, groupName = "ML Analysis")

    # REG Dataset to Organization File
    ori_data = H5FM.H52Data(organization_file, "REG Nested CV Table_1", groupName="ML Analysis")
    for cv_table_count in range(2, h5file_count):
        concat_data = H5FM.H52Data(organization_file, "REG Nested CV Table_"+str(cv_table_count), groupName="ML Analysis")
        ori_data = np.concatenate((ori_data, concat_data), axis = 0)
    ori_data = np.sort(ori_data, order='Regressor Test r2 Score Mean')[::-1]
    H5FM.Data2H5(ori_data, organization_file, "REG Nested CV Table", groupName="ML Analysis")
    H5FM.H5ReSave(organization_file)

def copy_model_data(old_model_folder, new_model_folder):
    if (len(os.listdir(old_model_folder)) >= 1):
        try:
            shutil.copytree(old_model_folder, os.path.join(new_model_folder, old_model_folder.split("\\")[-2]))
        except:
            print("Some issues in {}".format(old_model_folder))
            pass

def get_ml_model_path_info(model_folder):
    best_reg_path_lists = find_extension_name(os.path.join(model_folder,"*"),"*_best_reg.sav")
    print(os.path.join(model_folder,"*"))
    print(best_reg_path_lists)
    tree_based_reg_path_lists, tree_based_reg_folder_lists, best_reg_folder_lists = [], [], []
    prob_based_reg_path_lists, prob_based_reg_folder_lists = [], []
    for reg_path in best_reg_path_lists:
        reg_path_folder = os.path.join(*reg_path.split("\\")[:-1])
        best_reg_folder_lists.append(reg_path_folder)
        modelObj = pickle.load(open(os.path.join(reg_path), 'rb'))
        if (ME.check_tree_based_model(modelObj)):
            tree_based_reg_path_lists.append(reg_path)
            tree_based_reg_folder_lists.append(reg_path_folder)
            #print('Tree Based Model:',reg_path)
            #print(reg_path_folder)

        df_x_val, _ = read_val_data(reg_path_folder)
        if (ME.check_probability_based_model(modelObj, df_x_val)):
            prob_based_reg_path_lists.append(reg_path)
            prob_based_reg_folder_lists.append(reg_path_folder)
        del modelObj, df_x_val
    return best_reg_path_lists, best_reg_folder_lists, \
           tree_based_reg_path_lists, tree_based_reg_folder_lists, \
           prob_based_reg_path_lists, prob_based_reg_folder_lists

def get_prophet_ts_model_path_info(model_folder):
    best_reg_path_lists = find_extension_name(os.path.join(model_folder,"*"),"*.sav")
    print(os.path.join(model_folder,"*"))
    print(best_reg_path_lists)
    ts_based_reg_path_lists, ts_based_reg_folder_lists = [], []
    for reg_path in best_reg_path_lists:
        reg_path_folder = os.path.join(*reg_path.split("\\")[:-1])
        modelObj = pickle.load(open(os.path.join(reg_path), 'rb'))
        if (ME.check_prophet_ts_based_model(modelObj)):
            ts_based_reg_path_lists.append(reg_path)
            ts_based_reg_folder_lists.append(reg_path_folder)
    return ts_based_reg_path_lists, ts_based_reg_folder_lists

def get_basic_ts_model_path_info(model_folder):
    best_reg_path_lists = find_extension_name(os.path.join(model_folder,"*"),"*.sav")
    print(os.path.join(model_folder,"*"))
    print(best_reg_path_lists)
    ts_based_reg_path_lists, ts_based_reg_folder_lists = [], []
    for reg_path in best_reg_path_lists:
        reg_path_folder = os.path.join(*reg_path.split("\\")[:-1])
        modelObj = pickle.load(open(os.path.join(reg_path), 'rb'))
        if (ME.check_basic_ts_model(modelObj)):
            ts_based_reg_path_lists.append(reg_path)
            ts_based_reg_folder_lists.append(reg_path_folder)
    return ts_based_reg_path_lists, ts_based_reg_folder_lists

def get_cv_results(reg_path_lists, reg_folder_lists, outputFolder):
    reg_table_group = MCH.cv_results_record_init(len(reg_path_lists))
    pred_val_record = []
    specs = EDA.multiplot_strategy(len(reg_path_lists))
    fig = plt.figure(figsize=(25, 20))
    plot_setting = {}
    for idx, spec in enumerate(specs):
        # model load
        reg_path = reg_path_lists[idx]
        # modelObj = pickle.load(open(os.path.join(clf_path), 'rb'))
        reg_name = reg_path.split("\\")[-1].split(".")[0]
        model_name = reg_name.split("_")[0]

        # val data load
        reg_folder = reg_folder_lists[idx]
        df_x_val, df_y_val = read_val_data(reg_folder)
        #print(df_x_val)

        cv_results_path = find_extension_name(reg_folder, "*_cv_results.sav")[0]
        cv_results = pickle.load(open(os.path.join(cv_results_path), 'rb'))
        MCH.cv_reg_table_record(model_name, reg_table_group, cv_results, idx,
                                outputFolder, outputFolder)

        # cv plot
        plot_setting['title'] = "{} Learning Curve".format(model_name)
        plt.subplot(specs[idx])
        MCH.cv_plot(cv_results['train_r2'], cv_results['test_r2'], plot_setting = plot_setting)

        # prediction error 
        val_mse, val_rmse, val_median_abs_err, val_mean_abs_err, val_exp_var_score, val_r2 = MCH.cal_reg_error(df_x_val, df_y_val, reg_folder, reg_name)
        this_pred_val_record = [val_mse, val_rmse, val_median_abs_err, val_mean_abs_err, val_exp_var_score, val_r2]
        pred_val_record.append(this_pred_val_record)		

    df_pred_val_record= pd.DataFrame(pred_val_record, columns = ['MSE', 'RMSE', 'Median ABS Error', 'Mean Abs Error', 'Explained Variance Score', 'r2 Score'])
    df_pred_val_record.sort_values('r2 Score', ascending=[1]).to_csv(os.path.join(outputFolder,'val_performance.csv'), index = False)
    EDA.multiplot_save(fig, outputFolder, "cv_curve.svg")
    plt.close("all")

def prophet_cv_plot(reg_path_lists, reg_folder_lists, outputFolder, ds_column = ['Date'], ts_model_column = ['NAV'], **cv_params):
    reg_table_group = fbMCH.cv_results_record_init(len(reg_path_lists)*len(ts_model_column))
    pred_val_record = []
    specs = EDA.multiplot_strategy(len(reg_path_lists)*len(ts_model_column))
    fig = plt.figure(figsize=(25, 20))
    plot_setting = {}
    for idx, spec in enumerate(specs):
        # model load
        reg_path = reg_path_lists[idx]
        reg_folder = reg_folder_lists[idx]
        # modelObj = pickle.load(open(os.path.join(clf_path), 'rb'))
        reg_name = reg_path.split("\\")[-1].split(".")[0]
        model_name = reg_name.split("_")[0] + "_"+str(idx)

        # val data load
        reg_folder = reg_folder_lists[idx]
		
        x_train = read_data(os.path.join(reg_folder, 'x_train'))
        x_val = read_data(os.path.join(reg_folder, 'x_val'))

        for y_column in ts_model_column:
            #print(x_val)
            cv_table_path = find_extension_name(reg_folder, '*_cv_table.csv')
            df_train, df_val = fbMCH.df_data2prophet_data(x_train, x_val, ds_column, y_column)
            df_cv_table = pd.read_csv(cv_table_path[0])
            df_cv_table['ds'] = pd.to_datetime(df_cv_table.loc[:,'ds'])
            df_cv_table['cutoff'] = pd.to_datetime(df_cv_table.loc[:,'cutoff'])
            #df_performance = performance_metrics(df_cv_table)

            fig.tight_layout()
            ax = plt.subplot(spec)
            #print(y_column)
            fbMCH.prophet_cv_plot(df_cv_table, reg_folder, model_name, figname = 'Prophet_'+model_name, ylabel_tag = y_column, ax = ax)
    EDA.multiplot_save(fig, outputFolder, "cv_pred_plot.svg")
    plt.close("all")

def prophet_err_plot(reg_path_lists, reg_folder_lists, outputFolder, ds_column = ['Date'], ts_model_column = ['NAV']):
    reg_table_group = fbMCH.cv_results_record_init(len(reg_path_lists)*len(ts_model_column))
    pred_val_record = []
    specs = EDA.multiplot_strategy(len(reg_path_lists)*len(ts_model_column))
    fig = plt.figure(figsize=(25, 20))
    plot_setting = {}
    for idx, spec in enumerate(specs):
        # model load
        reg_path = reg_path_lists[idx]
        reg_folder = reg_folder_lists[idx]
        # modelObj = pickle.load(open(os.path.join(clf_path), 'rb'))
        reg_name = reg_path.split("\\")[-1].split(".")[0]
        model_name = reg_name.split("_")[0] + "_"+str(idx)

        # val data load
        reg_folder = reg_folder_lists[idx]
		
        x_train = read_data(os.path.join(reg_folder, 'x_train'))
        x_val = read_data(os.path.join(reg_folder, 'x_val'))

        for y_column in ts_model_column:
            #print(x_val)
            cv_table_path = find_extension_name(reg_folder, '*_cv_table.csv')
            df_cv_table = pd.read_csv(cv_table_path[0])
            df_cv_table['ds'] = pd.to_datetime(df_cv_table.loc[:,'ds'])
            df_cv_table['cutoff'] = pd.to_datetime(df_cv_table.loc[:,'cutoff'])
            #df_performance = performance_metrics(df_cv_table)

            fig.tight_layout()
            ax = plt.subplot(spec)
            #print(y_column)
            cvfig = fbMCH.prophet_err_plot('Prophet_'+model_name, df_cv_table, title_name = ' CV Test:')
            EDA.multiplot_save(cvfig, reg_folder, model_name + "_cv_err_plot.svg")

            val_table_path = find_extension_name(reg_folder, '*_val_table.csv')
            df_val_table = pd.read_csv(val_table_path[0])
            df_val_table['ds'] = pd.to_datetime(df_val_table.loc[:,'ds'])
            df_val_table['cutoff'] = pd.to_datetime(df_val_table.loc[:,'cutoff'])
			
            valfig = fbMCH.prophet_err_plot('Prophet_'+model_name, df_val_table, title_name = ' Validation:')
            EDA.multiplot_save(valfig, reg_folder, model_name + "_val_err_plot.svg")
    plt.close("all")

def df_data_features_err_multi_model_type_plot(model_folder, type_name, target_name, outputFolder, filename, plot_type = None, ylabel_list = ["No of Instances"], fig_size = (10, 10), missing_data_handle = None, FeaturesEngineeringObj = None):
	# File Path List
    err_file_list = glob.glob(os.path.join(model_folder,'*','val_err.csv'))
    
    print(os.path.join(model_folder,'*','*_err.csv'))
    specs = EDA.multiplot_strategy(len(err_file_list))
    fig = plt.figure(figsize=fig_size)
	
    # Plot Setting Initialization
    plot_setting = {}
    kwargs = {}
    for idx, spec in enumerate(specs):
        df_data = pd.read_csv(err_file_list[idx])
        df_data = FeaturesEngineeringObj(df_data)
        if (missing_data_handle['type'] == 'fillna'):
            df_data = df_data.fillna(value=missing_data_handle['value'])
        if (missing_data_handle['type'] == 'dropna'):
            df_data = df_data.dropna()
   
        folder_name = err_file_list[idx].split("\\")[-2]#.split(".")[0].split("_")[0]
        r = re.compile("([a-zA-Z]+)([0-9]+)")
        m = r.match(folder_name)
        model_name =  m.group(1)
        #print(model_name)
        fig.tight_layout()
        
        # Plot Setting
        if (target_name == None):   
           plot_setting['xlabel'] = type_name  
        else:
           plot_setting['xlabel'] = "{} versus {}".format(type_name, target_name) 
        
        if plot_type == 'Distribution':
           plot_setting['x'] = type_name
           plot_setting['y'] = None
        elif (plot_type == 'Count'):
           kwargs['x'] = type_name
           kwargs['hue'] = target_name
        elif (plot_type == 'Box'):
           kwargs['y'] = type_name
           kwargs['x'] = target_name

        plot_setting['title'] = "{} Model {} Error {} Plot".format(model_name, type_name, plot_type)
        plot_setting['ylabel'] = None
        if (ylabel_list):
            if (len(ylabel_list) > 1):
                plot_setting['ylabel'] = ylabel_list[idx]
            else:
                plot_setting['ylabel'] = ylabel_list[0]

        # Figure Plot
        plt.subplot(spec)
        #print('test', plot_type, kwargs, plot_setting)
        EDA.df_data_fig_plot(df_data, plot_type, plot_setting, **kwargs)
    if target_name == None:
       target = 'None'
    EDA.multiplot_save(fig, outputFolder, filename+"_multi_models_{}_versus_{}_err_{}.svg".format(type_name, target_name, plot_type))
    plt.close("all")

def df_data_multi_tree_based_reg_feature_importance_plot(reg_path_lists, reg_folder_lists, outputFolder):
    specs = EDA.multiplot_strategy(len(reg_path_lists))
    fig = plt.figure(figsize=(25, 10))
    plot_setting = {}
    for idx, spec in enumerate(specs):
        # model load
        reg_path = reg_path_lists[idx]
        # modelObj = pickle.load(open(os.path.join(clf_path), 'rb'))
        reg_name = reg_path.split("\\")[-1].split(".")[0]
        model_name = reg_name.split("_")[0]

        # val data load
        reg_folder = reg_folder_lists[idx]
        df_x_val, df_y_val = read_val_data(reg_folder)

        fig.tight_layout()
        plot_setting['title'] = "{} Based Feature Importance".format(model_name)
        plt.subplot(spec)
        modelObj, importances, indices = MCH.get_tree_based_feature_importance(df_x_val, reg_folder, reg_name)
        modelObjGroup = (modelObj, importances, indices)
        ME.df_data_tree_based_feature_importance_plot(modelObjGroup, df_x_val, plot_setting=plot_setting)
    EDA.multiplot_save(fig, outputFolder, "feature_importance.svg")
    plt.close("all")

def prophet_val_plot(ts_based_reg_path_lists, ts_based_reg_folder_lists, outputFolder, ds_column = ['Date'], ts_model_column = ['NAV']):
    specs = EDA.multiplot_strategy(len(ts_based_reg_path_lists)*len(ts_model_column))
    fig = plt.figure(figsize=(25, 10))
    plot_setting = {}
    for idx, spec in enumerate(specs):
        # model load
        reg_path = ts_based_reg_path_lists[idx]
        reg_folder = ts_based_reg_folder_lists[idx]
        # modelObj = pickle.load(open(os.path.join(clf_path), 'rb'))
        reg_name = reg_path.split("\\")[-1].split(".")[0]
        model_name = reg_name.split("_")[0] + "_"+str(idx)
        #ds_column, ts_model_column = ['Date'], ['NAV']
		
        x_train = read_data(os.path.join(reg_folder, 'x_train'))
        x_val = read_data(os.path.join(reg_folder, 'x_val'))
        #df_train = fbMCH.df_data2prophet_data(x_train, ds_column, y_column)

        for y_column in ts_model_column:
            df_train, df_val = fbMCH.df_data2prophet_data(x_train, x_val, ds_column, y_column)
            #df_train = fbMCH.df_data2prophet_data(x_train, ds_column, y_column)
            #df_val = fbMCH.df_data2prophet_data(x_val, ds_column, y_column)
            #print(df_train, df_val)
            fig.tight_layout()
            ax = plt.subplot(spec)
            #print(y_column)
            fbMCH.prophet_validation_plot(df_train, df_val, reg_folder, model_name, figname = 'Prophet_'+model_name, ylabel_tag = y_column, ax = ax)
    EDA.multiplot_save(fig, outputFolder, "val_pred_plot.svg")
    plt.close("all")

def basic_ts_val_plot(basic_ts_based_reg_path_lists, basic_ts_based_reg_folder_lists, outputFolder, ds_column = ['Date'], ts_model_column = ['NAV'], freq = 'B'):
    specs = EDA.multiplot_strategy(len(basic_ts_based_reg_path_lists)*len(ts_model_column))
    fig = plt.figure(figsize=(25, 10))
    plot_setting = {}
    for idx, spec in enumerate(specs):
        # model load
        reg_path = basic_ts_based_reg_path_lists[idx]
        reg_folder = basic_ts_based_reg_folder_lists[idx]
        # modelObj = pickle.load(open(os.path.join(clf_path), 'rb'))
        reg_name = reg_path.split("\\")[-1].split(".")[0]
        model_name = reg_name #.split("_")[0] + "_"+str(idx)
        print(reg_name, model_name)
        #ds_column, ts_model_column = ['Date'], ['NAV']
		
        x_train = read_data(os.path.join(reg_folder, 'x_train'))
        x_val = read_data(os.path.join(reg_folder, 'x_val'))
        #df_train = fbMCH.df_data2prophet_data(x_train, ds_column, y_column)

        for y_column in ts_model_column:
            df_train, df_val = tsMCH.df_data2ts_data(x_train, x_val, ds_column, y_column, freq = freq)
            #df_train = fbMCH.df_data2prophet_data(x_train, ds_column, y_column)
            #df_val = fbMCH.df_data2prophet_data(x_val, ds_column, y_column)
            #print(df_train, df_val)
            fig.tight_layout()
            ax = plt.subplot(spec)
            #print(y_column)
            tsMCH.ts_validation_plot(df_train, df_val, reg_folder, model_name, figname = model_name, ylabel_tag = y_column, ax = ax)
    EDA.multiplot_save(fig, outputFolder, "val_pred_plot.svg")
    plt.close("all")

def all_models_prediction(reg_path_lists, outputFolder, pred_id_column='id', pred_column='sub', data_drop_column='id', pred_filename='test', databasefolder='database'):
    pred_file_data = read_data(os.path.join(databasefolder, pred_filename))
    for reg_path in reg_path_lists:
        # model load
        #clf_path = clf_path_lists[idx]
        modelObj = pickle.load(open(os.path.join(reg_path), 'rb'))
        reg_name = reg_path.split("\\")[-1].split(".")[0]
        #model_name = clf_name.split("_")[0]

        submission = pd.DataFrame({
        pred_id_column: pred_file_data[pred_id_column],
		pred_column: modelObj.predict(pred_file_data.drop(columns=data_drop_column, axis=1))
        })
        submission.to_csv(os.path.join(outputFolder, reg_name + '_submission.csv'), index=False)

def model_organization(model_folder, ds_column, ts_model_column, **kwargs):
    output_folder_path = os.path.join(model_folder, output_folder)
    CU.createOutputFolder(output_folder_path)

    # model data
    print("model data check")
    model_folder_lists = find_extension_name(model_folder, "*/*/")
    for old_model_folder in model_folder_lists:
        copy_model_data(old_model_folder, output_folder_path)

    # create h5file
    print("Model Information Reorganization")
    best_reg_path_lists, best_reg_folder_lists, \
    tree_based_reg_path_lists, tree_based_reg_folder_lists, \
    prob_based_reg_path_lists, prob_based_reg_folder_lists = get_ml_model_path_info(output_folder_path)

    prophet_ts_based_reg_path_lists, prophet_ts_based_reg_folder_lists = get_prophet_ts_model_path_info(output_folder_path)
    basic_ts_based_reg_path_lists, basic_ts_based_reg_folder_lists = get_basic_ts_model_path_info(output_folder_path)

    print(best_reg_path_lists, best_reg_folder_lists)
    print(tree_based_reg_path_lists, tree_based_reg_folder_lists)
    print(prob_based_reg_path_lists, prob_based_reg_folder_lists)
    print(prophet_ts_based_reg_path_lists, basic_ts_based_reg_folder_lists)
    print(basic_ts_based_reg_path_lists, basic_ts_based_reg_folder_lists)
    if (len(best_reg_path_lists) > 0):
       get_cv_results(best_reg_path_lists, best_reg_folder_lists, output_folder_path)
    if (len(tree_based_reg_path_lists) > 0):
       df_data_multi_tree_based_reg_feature_importance_plot(tree_based_reg_path_lists, tree_based_reg_folder_lists, output_folder_path)
    if (len(prophet_ts_based_reg_path_lists) > 0):
       prophet_cv_plot(prophet_ts_based_reg_path_lists, basic_ts_based_reg_folder_lists, output_folder_path, ds_column = ds_column, ts_model_column = ts_model_column, **kwargs)
       prophet_val_plot(prophet_ts_based_reg_path_lists, basic_ts_based_reg_folder_lists, output_folder_path, ds_column = ds_column, ts_model_column = ts_model_column)
       prophet_err_plot(prophet_ts_based_reg_path_lists, basic_ts_based_reg_folder_lists, output_folder_path, ds_column = ds_column, ts_model_column = ts_model_column)
    if (len(basic_ts_based_reg_path_lists) > 0):
       basic_ts_val_plot(basic_ts_based_reg_path_lists, basic_ts_based_reg_folder_lists, output_folder_path, ds_column = ds_column, ts_model_column = ts_model_column)
    # all data prediction
    try:
        all_models_prediction(best_reg_path_lists, output_folder_path, **kwargs)
    except:
        print("Issues in Prediction File")
        pass
    #cv_results_lists = find_extension_name(output_folder_path, "*/*_cv_results.sav")
    #get_cv_results(cv_results_lists, output_folder_path, conf_matrix_params)

    # h5file data
    """
    print("h5 data check")
    h5file_lists = find_extension_name(model_folder, "*\*.h5")
    for h5file in h5file_lists:
        copy_h5_file(h5file, output_folder_path)
    """

    # csv table
    """
    print("csv data check")
    csv_lists = find_extension_name(model_folder, "*\*.csv")
    for csv_file in csv_lists:
        shutil.copy(csv_file, output_folder_path)
    """


